import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
//import 'package:flutter/rendering.dart';
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/pages/home.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

//import 'package:flutter/scheduler.dart';

void main() {
  // debugPaintSizeEnabled = true;
  // timeDilation = 10;
  runApp(MyApp());
}

///Main-Datei. Erstellt die Material-App
///
///Die Hauptseite ist allerdings unter [HomePage] zu finden
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final DataModel model = DataModel();
    return ScopedModel<DataModel>(
      model: model,
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        supportedLocales: [const Locale("de", "DE")],
        title: 'eMission ',
        theme: ThemeData(
            primarySwatch: Colors.lightGreen,
            cardColor: Colors.white.withAlpha(225)),
        home: HomePage(model),
      ),
    );
  }
}

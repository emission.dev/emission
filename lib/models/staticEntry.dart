import 'dart:convert';

import 'package:eMission/models/entry.dart';
import 'package:eMission/utils/storage.dart';

///Regelmäßiger CO²-Verbrauch über einen langen Zeitraum, z.B. Strom, Heizung, etc
///Beim Erstellen werden die CO²-Emissionen pro Tag festgelegt und ein Datum, ab dem
///der Verbrauch berücksichtigt werden soll.
///Wenn kein Eintrag manuell hinzugefügt wurde, wird StaticEntry.average() benutzt
class StaticEntry {
  ///Datum, ab dem der StaticEntry gelten soll
  DateTime start;
  //Setzte diesen Wert nicht direkt im Konstruktor, außer über [fromJson]
  final double emissionsPerDay;

  ///Ist nur dann wahr, falls der Benutzer diesen Eintrag nicht selbst konfiguriert hat
  final bool isDefault;
  StaticEntry(DateTime start, this.emissionsPerDay, [this.isDefault = false
      //Sorgt dafür, dass der Anfang des [StaticEntry] Mitternacht ist
      ])
      : this.start = DateTime(start.year, start.month, start.day);

  bool operator ==(o) => o.start == start && o.runtimeType == runtimeType;
  int get hashCode => start.hashCode * runtimeType.hashCode;

  factory StaticEntry.fromJson(Map<String, dynamic> entryMap) {
    switch (entryMap["staticEntryType"]) {
      case "ConsumptionStaticEntry":
        return ConsumptionStaticEntry.fromJson(entryMap);
      case "PublicStaticEntry":
        return PublicStaticEntry.fromJson(entryMap);
      case "FoodStaticEntry":
        return FoodStaticEntry.fromJson(entryMap);
      case "PowerStaticEntry":
        return PowerStaticEntry.fromJson(entryMap);
      case "HeatingStaticEntry":
        return HeatingStaticEntry.fromJson(entryMap);
      default:
        print("Fehler beim Laden eines objekts mit typ:" +
            entryMap["staticEntryType"]);
        return null;
    }
  }
  factory StaticEntry.average(String type) {
    switch (type) {
      case "PowerStaticEntry":
        return PowerStaticEntry.average();
      case "HeatingStaticEntry":
        return HeatingStaticEntry.average();
      case "PublicStaticEntry":
        return PublicStaticEntry.average();
      case "FoodStaticEntry":
        return FoodStaticEntry.average();
      case "ConsumptionStaticEntry":
        return ConsumptionStaticEntry.average();
      case "StaticEntry":
        return StaticEntry(DateTime.now(), 0);
      default:
        print("Fehler! Kein StaticEntry.average für typ " + type + " gefunden");
        return null;
    }
  }
  withStartDate(DateTime start) {
    this.start = start;
    return this;
  }
}

const Map<Type, String> staticEntryString = {
  PowerStaticEntry: "Strom",
  HeatingStaticEntry: "Heizung",
  FoodStaticEntry: "Nahrung",
  ConsumptionStaticEntry: "Konsum",
  PublicStaticEntry: "Öffentlich",
  Entry: "Transport"
};

///Staatliche Emissionen (Schulen, Müllabfuhr, Verwaltung,...)
class PublicStaticEntry extends StaticEntry {
  PublicStaticEntry({start, emissionsPerDay})
      : super(start, emissionsPerDay, false);

  PublicStaticEntry.average() : super(DateTime(-20000), 2, true);
  factory PublicStaticEntry.fromJson(Map<String, dynamic> jsonData) {
    assert(jsonData["staticEntryType"] == "PublicStaticEntry");
    return PublicStaticEntry(
      emissionsPerDay: jsonData['emissionsPerDay'].toDouble(),
      start: jsonData['start'] == null
          ? null
          : DateTime.parse(jsonData['start'] as String),
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'staticEntryType': "ConsumptionStaticEntry",
      'emissionsPerDay': emissionsPerDay,
      'start': start.toString(),
    };
  }
}

///Enthält Daten zum generellen Konsum eines Nutzers (Kaufverhalten, etc)
class ConsumptionStaticEntry extends StaticEntry {
  ConsumptionStaticEntry({start, emissionsPerDay})
      : super(start, emissionsPerDay, false);

  ConsumptionStaticEntry.average() : super(DateTime(-20000), 12.5, true);
  factory ConsumptionStaticEntry.fromJson(Map<String, dynamic> jsonData) {
    assert(jsonData["staticEntryType"] == "ConsumptionStaticEntry");
    return ConsumptionStaticEntry(
      emissionsPerDay: jsonData['emissionsPerDay'].toDouble(),
      start: jsonData['start'] == null
          ? null
          : DateTime.parse(jsonData['start'] as String),
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'staticEntryType': "ConsumptionStaticEntry",
      'emissionsPerDay': emissionsPerDay,
      'start': start?.toIso8601String(),
    };
  }
}

Map<HeatingType, String> heatingTypeEnumMap = {
  HeatingType.gas: 'Erdgas',
  HeatingType.oil: 'Heizöl',
  HeatingType.charcoal: 'Brankohle',
  HeatingType.coal: 'Steinkohle',
  HeatingType.district: 'Fernwärme',
  HeatingType.wood: 'Holz',
  HeatingType.power: 'Strom',
  HeatingType.liquidGas: 'Flüssiggas'
};
enum HeatingType { coal, charcoal, oil, gas, district, wood, power, liquidGas }
//Rechnet die Menge des jeweiligen Stoffes in kWh um
const Map<HeatingType, Map<HeatingUnit, double>> unitConversion = {
  HeatingType.gas: {HeatingUnit.m3: 8.816},
  HeatingType.oil: {HeatingUnit.kg: 11.87, HeatingUnit.liter: 10.03},
  HeatingType.charcoal: {HeatingUnit.kg: 5.448},
  HeatingType.liquidGas: {HeatingUnit.liter: 6.627, HeatingUnit.kg: 12.944},
  HeatingType.coal: {HeatingUnit.m3: 8.723},
  HeatingType.wood: {HeatingUnit.kg: 4.117, HeatingUnit.ster: 1852.65},
  HeatingType.district: {}
};

enum HeatingUnit { m3, kg, liter, ster, kWh }
Map<HeatingUnit, String> heatingUnitEnumMap = {
  HeatingUnit.m3: 'm³',
  HeatingUnit.kg: 'kg',
  HeatingUnit.liter: 'Liter',
  HeatingUnit.ster: 'Ster',
  HeatingUnit.kWh: 'kWh'
};

///Enthält Daten zur Heizanlage im Haus des Nutzers
class HeatingStaticEntry extends StaticEntry {
  final HeatingType heatingType;
  final double kWh;
  final HeatingUnit unit;
  final double unitAmount;
  HeatingStaticEntry.power(DateTime date)
      : heatingType = HeatingType.power,
        unit = HeatingUnit.kWh,
        unitAmount = 0,
        kWh = 0,
        super(date, 0);
  HeatingStaticEntry(
      {DateTime start, this.heatingType, this.unit, this.unitAmount})
      : kWh = unit == HeatingUnit.kWh
            ? unitAmount
            : unitConversion[heatingType][unit],
        super(
            start,
            calcEmissions(
                heatingType,
                unit == HeatingUnit.kWh
                    ? unitAmount
                    : unitConversion[heatingType][unit]),
            false);

  HeatingStaticEntry.average()
      : heatingType = null,
        unit = null,
        unitAmount = 0,
        kWh = 0,
        super(DateTime(-20000), 3.3, true);

  factory HeatingStaticEntry.fromJson(Map<String, dynamic> jsonData) {
    assert(jsonData["staticEntryType"] == "HeatingStaticEntry");
    return HeatingStaticEntry(
        start: jsonData['start'] == null
            ? null
            : DateTime.parse(jsonData['start'] as String),
        heatingType: StorageUtils.enumDecode(
            heatingTypeEnumMap, jsonData['heatingType']),
        unit: StorageUtils.enumDecode(heatingUnitEnumMap, jsonData['unit']),
        unitAmount: jsonData['unitAmount']);
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'staticEntryType': "HeatingStaticEntry",
      'emissionsPerDay': emissionsPerDay,
      'start': start?.toIso8601String(),
      'heatingType': heatingTypeEnumMap[heatingType],
      'unit': heatingUnitEnumMap[unit],
      'unitAmount': unitAmount
    };
  }

  static double calcEmissions(HeatingType ht, double kWh) {
    switch (ht) {
      case HeatingType.charcoal:
        return kWh * 0.5;
      case HeatingType.coal:
        return kWh * 0.4;
      case HeatingType.district:
        return kWh * 0.13;
      case HeatingType.gas:
        return kWh * 0.24;
      case HeatingType.liquidGas:
        return kWh * 0.3;
      case HeatingType.oil:
        return kWh * 0.3;
      case HeatingType.wood:
        return kWh * 0.2;
      case HeatingType.power:
        return kWh * 0;
    }
    return null;
  }
}

///Durch Nahrung erzeugte Emissionen. Soll später von StaticEntry zu Entry transformiert werden
class FoodStaticEntry extends StaticEntry {
  FoodStaticEntry({DateTime start, double emissionsPerDay})
      : super(start, emissionsPerDay, false);
  FoodStaticEntry.average() : super(DateTime(-20000), 4.75, true);

  factory FoodStaticEntry.fromJson(Map<String, dynamic> jsonData) {
    assert(jsonData["staticEntryType"] == "FoodStaticEntry");
    return FoodStaticEntry(
      emissionsPerDay: jsonData['emissionsPerDay'].toDouble(),
      start: jsonData['start'] == null
          ? null
          : DateTime.parse(jsonData['start'] as String),
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'staticEntryType': "FoodStaticEntry",
      'emissionsPerDay': emissionsPerDay,
      'start': start?.toIso8601String(),
    };
  }
}

///Durch Stromverbrauch erzeugte CO² Emissionen
class PowerStaticEntry extends StaticEntry {
  final PowerMix powerMix;
  final double consumptionPerDay;
  PowerStaticEntry({DateTime start, this.powerMix, this.consumptionPerDay})
      : super(start, calcEmissions(powerMix, consumptionPerDay), false);
  PowerStaticEntry.average()
      : powerMix = PowerMix.average(),
        consumptionPerDay = 4,
        super(DateTime(-20000), 3.3, true);

  factory PowerStaticEntry.fromJson(Map<String, dynamic> jsonData) {
    assert(jsonData["staticEntryType"] == "PowerStaticEntry");
    return PowerStaticEntry(
        start: DateTime.parse(jsonData['start'] as String),
        powerMix: PowerMix.fromJson(json.decode(jsonData["powerMix"])),
        consumptionPerDay: jsonData["consumptionPerDay"]);
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'staticEntryType': "PowerStaticEntry",
      "powerMix": json.encode(powerMix),
      "consumptionPerDay": consumptionPerDay,
      'start': start?.toIso8601String(),
    };
  }

  ///Berechnet aus Stromverbrauch und Strommix die Emissionen
  static calcEmissions(PowerMix powerMix, double consumptionPerDay) {
    double emissions = powerMix.atomic * 0.03;
    emissions += powerMix.renewable * 0.03;
    emissions += powerMix.coal * 1.022;
    emissions += powerMix.gas * 0.53;
    emissions += powerMix.other * 0.45;
    emissions *= consumptionPerDay;
    return emissions;
  }
}

enum PowerMixType { custom, renewable, average }
Map<PowerMixType, String> _powerMixTypeEnumMap = {
  PowerMixType.average: "avg",
  PowerMixType.custom: "cust",
  PowerMixType.renewable: "renew"
};

///Dient zur Festlegung des Strommixes des Nutzers
class PowerMix {
  ///Anteil des jeweiligen Energieträgers
  final double coal, atomic, renewable, gas, other;

  ///Festlegung des Stromanteils
  final PowerMixType powerMixType;

  ///Eigenen Anteil definieren
  PowerMix({this.coal, this.atomic, this.renewable, this.gas})
      : other = 1 - (coal - atomic - renewable - gas),
        powerMixType = PowerMixType.custom;

  ///Rein erneuerbare Energien
  PowerMix.renewable()
      : coal = 0,
        atomic = 0,
        renewable = 1,
        gas = 0,
        other = 0,
        powerMixType = PowerMixType.renewable;

  ///Deutscher Strommix
  PowerMix.average()
      : coal = 0.366,
        atomic = 0.117,
        gas = 0.132,
        renewable = 0.323,
        other = 0.062,
        powerMixType = PowerMixType.average;

  factory PowerMix.fromJson(Map<String, dynamic> jsonData) {
    switch (StorageUtils.enumDecode(
        _powerMixTypeEnumMap, jsonData["powerMixType"])) {
      case PowerMixType.average:
        return PowerMix.average();
      case PowerMixType.renewable:
        return PowerMix.renewable();
      case PowerMixType.custom:
        return PowerMix(
            coal: jsonData["coal"],
            atomic: jsonData["atomic"],
            gas: jsonData["gas"],
            renewable: jsonData["renewable"]);
    }
    return null;
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> returnMap = {
      "powerMixType": _powerMixTypeEnumMap[powerMixType]
    };
    if (powerMixType == PowerMixType.custom) {
      returnMap["coal"] = coal;
      returnMap["renewable"] = renewable;
      returnMap["atomic"] = atomic;
      returnMap["gas"] = gas;
    }
    return returnMap;
  }
}

import 'package:eMission/utils/dateTime.dart';

class Settings {
  bool enableDriveBackup, showIntro;
  Duration driveBackupFrequency;
  String backupFileUrl;
  DateTime firstUsage;

  Settings(
      {this.driveBackupFrequency = const Duration(),
      this.enableDriveBackup = false,
      this.showIntro = true,
      firstUsage})
      : this.firstUsage = firstUsage ?? DateTime.now();
  factory Settings.fromJson(Map<String, dynamic> entryMap) {
    return Settings(
      driveBackupFrequency:
          DateTimeUtils.parseDuration(entryMap["driveBackupFrequency"]) ??
              Duration(),
      firstUsage: DateTime.parse(entryMap['firstUsage']),
      enableDriveBackup: entryMap["enableDriveBackup"] ?? false,
      showIntro: entryMap["showIntro"] ?? true,
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> returnMap = {};
    returnMap["enableDriveBackup"] = enableDriveBackup;
    returnMap["showIntro"] = showIntro;
    returnMap["driveBackupFrequency"] = driveBackupFrequency.toString();
    returnMap["firstUsage"] = firstUsage.toString();
    print(returnMap);
    return returnMap;
  }
}

import 'dart:convert';
import 'package:eMission/models/entry.dart';

///Dient zum schnellen Hinzufügen von Einträgen. Alle Daten eines Eintrags außer des Datums werden aus dem gespeicherten Eintrag wiederverwertet
///Das Shortcut-Object enthält den Eintrag als json.
///dateId ist in diesem Fall das Erstelldatum des #
class Shortcut {
  ///Enthält die Daten des Eintrags
  final Entry _entry;
  Shortcut(this._entry);

  Entry get entry {
    return _entry;
  }

  ///Erstellt einen Eintrag aus dem Shortcut. Dabei wird [dateId] von [_entryJson] durch das Argument überschrieben
  Entry createEntry(DateTime dateId) {
    Map<String, dynamic> jsonData = json.decode(json.encode(_entry));
    jsonData["dateId"] = dateId.toIso8601String();
    return Entry.fromJson(jsonData);
  }

  factory Shortcut.fromJson(Map<String, dynamic> jsonData) {
    return Shortcut(Entry.fromJson(jsonData));
  }
  Map<String, dynamic> toJson() {
    return json.decode(json.encode(_entry));
  }

  @override
  String toString() {
    return entry.toString();
  }
  bool operator ==(o)=> o.entry == entry;
  int get hashCode => entry.hashCode;
}

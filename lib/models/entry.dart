import 'dart:convert';
import 'package:eMission/utils/units.dart';
import 'package:flutter/material.dart';
import 'package:eMission/models/vehicles.dart';
import 'package:eMission/utils/storage.dart';
import 'package:eMission/utils/dateTime.dart';

///Ein minimaler Eintrag. Wird so nicht genutzt, sonern an Unterklassen vererbt
abstract class Entry {
  ///In diesem Eintrag erzeugte Emissionen
  final double totalEntryEmission;

  ///optionale Beschreibung eines Eintrags
  String description;

  ///Datum, an dem die Emissionen emitiert wurden
  ///
  ///Die genaue Uhrzeit wird intern als ID verwendet, da sie auf
  ///Mikrosekunden genau ist sollte sie immer einzigartig sein.
  final DateTime dateId;

  Entry(this.totalEntryEmission, this.dateId, [this.description]);
  factory Entry.fromJson(Map<String, dynamic> entryMap) {
    switch (entryMap["entryType"]) {
      case "OtherEntry":
        return OtherEntry.fromJson(entryMap);
      case "TransportEntry":
        return TransportEntry.fromJson(entryMap);
      case "CompoundEntry":
        return CompoundEntry.fromJson(entryMap);
      default:
        print(
            "Fehler! beim Laden eines objekts mit typ:" + entryMap["entryType"]);
        return null;
    }
  }
  bool operator ==(o) => dateId == o.dateId;
  int get hashCode => dateId.hashCode;
}

///Legt das Treibhausgas für [OtherEmtry] fest. Standardmäßig co2
enum GhgType { co2, methane }
const _GhgTypeEnumMap = <GhgType, String>{
  GhgType.co2: "CO\u{2082}",
  GhgType.methane: "Methan"
};

///Minimaler erstellbarer Eintrag
class OtherEntry extends Entry {
  final GhgType ghgType;

  ///Minimaler erstellbarer Eintrag
  OtherEntry(totalEntryEmission, dateId,
      [description, this.ghgType = GhgType.co2])
      : super(totalEntryEmission, dateId, description);

  factory OtherEntry.fromJson(Map<String, dynamic> jsonData) {
    assert(jsonData["entryType"] == "OtherEntry");
    return OtherEntry(
        (jsonData['totalEntryEmission'] as num)?.toDouble(),
        jsonData['dateId'] == null
            ? null
            : DateTime.parse(jsonData['dateId'] as String),
        jsonData['description'] as String,
        StorageUtils.enumDecode(_GhgTypeEnumMap, jsonData['ghgType']));
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'entryType': "OtherEntry",
      'totalEntryEmission': totalEntryEmission,
      'description': description,
      'dateId': dateId?.toIso8601String(),
      'ghgType': _GhgTypeEnumMap[ghgType]
    };
  }
}

class TransportEntry extends Entry {
  double distance;

  ///Enthält fahrzeugspezifische Daten (Flugklasse, Motortyp etc). Enthält außerdem  Funktion zur Berechnung der CO2 Emissionen
  final Vehicle vehicle;

  ///Startpunkt der Reise. Zur Ausgabe im UI
  final String start;

  ///Endpunkt der Reise. Zur Ausgabe im UI
  String end;

  TransportEntry({
    @required this.distance,
    @required dateId,
    @required this.vehicle,
    this.start = "",
    this.end = "",
    description,
  }) : super(vehicle.calculateEmissions(distance), dateId, description);

  TransportEntry.withTotalEntryEmission({
    @required this.distance,
    @required totalEntryEmission,
    @required dateId,
    @required this.vehicle,
    this.start = "",
    this.end = "",
    description,
  }) : super(totalEntryEmission, dateId, description);

  factory TransportEntry.fromJson(Map<String, dynamic> jsonData) {
    assert(jsonData["entryType"] == "TransportEntry");
    Map<String, dynamic> vehicleMap = json.decode(jsonData["vehicle"]);
    return TransportEntry.withTotalEntryEmission(
        dateId: DateTime.parse(jsonData["dateId"]),
        totalEntryEmission: jsonData["totalEntryEmission"],
        distance: jsonData["distance"],
        description: jsonData["description"],
        vehicle: Vehicle.fromJson(vehicleMap),
        start: jsonData["start"],
        end: jsonData["end"]);
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'entryType': "TransportEntry",
      'totalEntryEmission': totalEntryEmission,
      'description': description,
      "distance": distance,
      'dateId': dateId?.toIso8601String(),
      "vehicle": json.encode(vehicle),
      "start": start,
      "end": end
    };
  }

  @override
  String toString() {
    if (description != null && description != "")
      return description;
    else if (start.isNotEmpty && end.isNotEmpty)
      return "$vehicle von $start nach $end";
    else
      return "$vehicle über ${UnitUtils.printDistance(distance)}";
  }
}

///Mehrere Einträge zusammengefasst zu einem. Nützliche bei Reisen
///mit mehreren Schritten (z.B. Zwischenlandung).
///Später eventuell auch für Lebensmittel uvm
///addiert [totalEmtryEmissiosn]
///Einträge müssen vom gleichen Tag sein
class CompoundEntry extends Entry {
  final List<Entry> entries;
  CompoundEntry(
    this.entries,
  )   : assert(_assertCompoundEntries(entries)),
        super(
          _calculateTotalEntryEmissions(entries),
          entries[0].dateId,
        );

  static bool _assertCompoundEntries(List<Entry> entries) {
    DateTime date = entries[0].dateId;
    entries.forEach((Entry entry) {
      if (!DateTimeUtils.isSameDay(date, entry.dateId)) {
        print("Nicht alle Entries von CompoundEntry sind vom selben Tag");
        return false;
      }
      if (entry is CompoundEntry) {
        print("Fehler: CompoundEntry enthält anderen CompoundEntry");
      }
    });
    return true;
  }

  static double _calculateTotalEntryEmissions(List<Entry> entries) {
    double returnDouble = 0;
    entries.forEach((Entry entry) {
      returnDouble += entry.totalEntryEmission;
    });
    return returnDouble;
  }

  factory CompoundEntry.fromJson(Map<String, dynamic> jsonData) {
    assert(jsonData["entryType"] == "CompoundEntry");
    List<Entry> entries = [];
    jsonData["entries"].forEach((dynamic map) {
      entries.add(Entry.fromJson(json.decode(map)));
    });
    return CompoundEntry(entries);
  }

  Map<String, dynamic> toJson() {
    List<String> entryString = [];

    entries.forEach((Entry entry) {
      entryString.add(json.encode(entry));
    });
    Map<String, dynamic> returnMap = {
      "entryType": "CompoundEntry",
      "entries": entryString
    };
    return returnMap;
  }

  @override
  String toString() {
    //Herausfinden, ob es sich um ein TransportEntry handelt. Dann können mehr Informationen angezeigt werden
    if (entries.every((Entry entry) {
      return entry is TransportEntry;
    })) {
      Vehicle vehicle;

      ///Das erste Wort ist das Fahrzeug. Im Folgenden wird dieses ermittelt
      String firstWord;
      //Findet heraus , ob alle Fahrzeuge vom gleichen Typ sind
      if (entries.every((Entry entry) {
        if (vehicle == null) vehicle = (entry as TransportEntry).vehicle;
        return (vehicle.toString() ==
            (entry as TransportEntry).vehicle.toString());
      }))
        firstWord = vehicle.toString();
      else
        //Wenn sich die Fahrzeuge unterscheiden, ist das erste Wort einfach "Reise"
        firstWord = "Reise";
      //Falls Start und Ziel vorhanden sind, diese benutzen
      if ((entries[0] as TransportEntry).start.isNotEmpty &&
          (entries.last as TransportEntry).end.isNotEmpty)
        return "$firstWord von ${(entries[0] as TransportEntry).start} bis ${(entries.last as TransportEntry).end}";
      else
      //Andernfalls Strecke aufsummieren und zurückgeben
      {
        double combinedDistance = 0;
        entries.forEach((Entry entry) {
          combinedDistance += (entry as TransportEntry).distance;
        });
        return "$firstWord über ${UnitUtils.printDistance(combinedDistance)}";
      }
    } else
      //Generische Antwort, wird angewandt, falls es sich nicht um einen TransportEntry handelt
      return "Mehrere Einträge mit ${UnitUtils.printEmissions(totalEntryEmission)}";
  }
}

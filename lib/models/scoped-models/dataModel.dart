import 'dart:convert';
import 'package:eMission/models/day.dart';
import 'package:eMission/models/settings.dart';
import 'package:eMission/utils/storage.dart';
import 'package:scoped_model/scoped_model.dart';

import 'package:eMission/models/scoped-models/entryModel.dart';
import 'package:eMission/models/scoped-models/shortcutModel.dart';
import 'package:eMission/models/scoped-models/friendsModel.dart';

///Das übergreifende Daten-Modell
class DataModel extends Model with EntryModel, ShortcutModel, FriendsModel {
  ///Enthält die Anwendungseinstellungen
  Settings settings;

  ///Zeigt an, ob alle Daten geladen wurden und benutzbar sind
  bool hasLoaded = false;

  saveSettings() {
    StorageUtils.writeToFile(json.encode(settings), "settings.json");
  }

  loadSettings() async {
    var loadFromFile = await StorageUtils.loadFromFile("settings.json");
    if (loadFromFile == null ||
        loadFromFile.isEmpty ||
        loadFromFile == "null") {
      settings = Settings();
      print('keine Settings zum Laden');
      return;
    }
    settings = Settings.fromJson(json.decode(loadFromFile));
    notifyListeners();
    print('loadSettings complete');
  }

  ///Lädt alle Nutzerdaten aus den jeweiligen Dateien
  loadData() async {
    DateTime start = DateTime.now();
    hasLoaded = false;
    try {
      await loadSettings();
      //Wichtig: StaticEntries werden vor den Entries geladen, weil die Tage erst in loadEntries erstellt werden, und somit
      //die Tage dann direkt bei Erstellung die richtigen StaticEntries zugewiesen bekommen können
      await loadStaticEntries();
      await loadEntries();
      await loadShortcuts();
      await loadFriendData();

      print("loadData abgeschlossen nach:" +
          DateTime.now().difference(start).toString());
    } catch (e) {
      if (e.toString().contains("Nicht"))
        print("(nicht angemeldet) loadData abgeschlossen nach:" +
            DateTime.now().difference(start).toString());
      else
        print("Fehler beim Laden der Daten:" + e.toString());
    }
    hasLoaded = true;
    notifyListeners();
  }

  ///Speichert alle Nutzerdaten in die jeweiligen Dateien
  Future<bool> saveData() async {
    await saveEntries();
    await saveSettings();
    await saveShortcuts();
    await saveStaticEntries();
    await saveFriendData();
    print("Speichern abgeschlossen");
    await updatePosts().catchError((error) {
      print(error.toString());
    });
    return true;
  }

  setStaticEntries([Day day]) {}
}

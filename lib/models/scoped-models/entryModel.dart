import 'dart:convert';

import 'package:eMission/models/day.dart';
import 'package:eMission/models/entry.dart';
import 'package:eMission/models/staticEntry.dart';
import 'package:eMission/utils/dateTime.dart';
import 'package:eMission/utils/storage.dart';
import 'package:scoped_model/scoped_model.dart';

///Verwaltung von Einträgen im Datenmodell
mixin EntryModel on Model {
  ///Das aktuell ausgewählte Datum
  DateTime _displaytDate = DateTime.now();

  ///List aller erstellter Tage
  Set<Day> _days = Set<Day>();

  ///Enthält alle Static Entries. Es kann immer nur pro Zeit ein Static Entry aktiv sein
  ///Enthält keine .average-Static-Entries, also nur die vom Nutzer erstellten
  Map<Type, List<StaticEntry>> _staticEntries = {
    ConsumptionStaticEntry: <ConsumptionStaticEntry>[],
    PublicStaticEntry: <PublicStaticEntry>[],
    PowerStaticEntry: <PowerStaticEntry>[],
    HeatingStaticEntry: <HeatingStaticEntry>[],
    FoodStaticEntry: <FoodStaticEntry>[],
  };
  Map<Type, List<StaticEntry>> get staticEntries => Map.from(_staticEntries);

  /// Das aktuell angezeigte Datum
  DateTime get displayDate {
    if (_displaytDate == null) _displaytDate = DateTime.now();
    return _displaytDate;
  }

  Set<Day> get days => _days;

  ///Eine Liste der Einträge am Tag [DateTime]
  List<Entry> getEntriesFromDay(DateTime date) =>
      List.from(_getDay(date).entries);

  Map<Type, StaticEntry> getStaticEntriesForDay(DateTime date) {
    return _getDay(date).staticEntries;
  }

  ///Gibt die addierten Co2-Emissionen des Tages [day] zurück
  ///Enthält sowohl die Emissionen aller [Entry]s als auch aller [StaticEntry]s
  double getTotalDailyEmissions(DateTime day) => _getDay(day).dailyEmissions;
  double getDailyEntryEmissions(DateTime day) =>
      _getDay(day).dailyEntryEmissions;

  ///Setzt das aktuell ausgewählte Datum auf [newDisplayDate]
  setDisplayDate(DateTime newDisplayDate) {
    _displaytDate = newDisplayDate;
    notifyListeners();
  }

  ///Fügt den neuen Eintrag hinzu.
  ///Falls [updateFile] false ist, wird keine Änderung an den Server gesendet
  addEntry(Entry newEntry,
      [bool updateFile = true, Map<String, Map<DateTime, int>> toBeUploaded]) {
    Day day = _getDay(newEntry.dateId);
    day.entries.add(newEntry);
    print("Added ${newEntry.runtimeType.toString()} to " +
        DateTimeUtils.toDateString(newEntry.dateId));
    updateTotalDailyEmissions(day, newEntry.totalEntryEmission, true,
        toBeUploaded);
    if (updateFile) saveEntries();
  }

  ///Löscht den Eintrag, der zu diesem [DateTime] erstellt wurde
  dynamic deleteEntry(
      DateTime dateId, Map<String, Map<DateTime, int>> toBeUploaded) {
    Day day = _getDay(dateId);
    Entry removed = day.entries.firstWhere((Entry entry) {
      return entry.dateId.isAtSameMomentAs(dateId);
    }, orElse: () {
      print(
          "Fehler! Es wurde versucht, ein Eintrag mit dateId $dateId zu löschen, doch für diese dateId konnte kein Eintrag gefunden werden");
      return null;
    });
    day.entries.remove(removed);

    updateTotalDailyEmissions(
        day, -removed.totalEntryEmission, true, toBeUploaded);
    print(
        "Eintrag mit dateId ${removed.dateId.toIso8601String()} erfolgreich gelöscht.");
    saveEntries();
    return removed;
  }

  ///Gibt [day] für das jeweilige [DateTime] zurück. Wenn
  ///für das [DateTime] noch kein [day] existiert, wird dieser erstellt.
  Day _getDay(DateTime date) {
    //Teste, ob der Tag schon existiert
    Day foundDay = _days.firstWhere(
        (Day searchDay) => DateTimeUtils.isSameDay(date, searchDay.date),
        orElse: () {
      Day day = Day(date);
      _setStaticEntries(day);
      _days.add(day);
      return day;
    });
    return foundDay;
  }

  ///Ändert die [dailyEmissions] für Tag [day] um [change].
  ///Wird beim Löschen und hinzufügen ausgeführt.
  void updateTotalDailyEmissions(
    Day day,
    double change, [
    bool updateEntryEmissions = true,
    Map<String, Map<DateTime, int>> toBeUploaded,
  ]) {
    day.dailyEmissions += change;
    if (updateEntryEmissions) {
      day.dailyEntryEmissions += change;
      //Weise das Friends Model an, die neuen Daten hochzuladen
      if(toBeUploaded !=null)
      toBeUploaded['Entry'][day.date] =
          (day.dailyEntryEmissions * 1000.0).round();
    }
    notifyListeners();
  }

  ///Lädt [Day]s und [Entry]s von entries.json. Gibt bei Erfolg [true] zurück
  loadEntries() async {
    //Daten lesen
    _days = Set<Day>();
    //Datei öffnen und decodieren
    var loadFromFile = await StorageUtils.loadFromFile("entries.json");
    if (loadFromFile.isEmpty || loadFromFile == null) {
      print('keine Entries zum Laden');
      return;
    }
    List<dynamic> resultsAsList = json.decode(json.decode(loadFromFile));
    resultsAsList.forEach((dynamic input) {
      addEntry(Entry.fromJson(input), false);
    });
    print('loadEntries complete');
  }

  ///Speichert [day]s und zugehörige [Entry]s in entries.json. Gibt bei Erfolg [true] zurück.
  Future<bool> saveEntries() async {
    try {
      List<String> writeString = [];
      //Alle Einträge aus allen Tagen als json zu [writeString] hinzufügen
      _days.forEach((Day day) {
        day.entries.forEach((Entry entry) {
          writeString.add(json.encode(entry));
        });
      });
      StorageUtils.writeToFile(
          json.encode(writeString.toString()), "entries.json");
      return true;
    } catch (e) {
      print("Fehler in saveEntries: " + e.toString());
      return false;
    }
  }

  saveStaticEntries() async {
    try {
      Map<String, String> writeMap = _staticEntries.map((type, staticEntry) {
        //Erstelle für jeden StaticEntry-Typ einen Map-Eintrag mit folgendem Format:
        //StaticEntry-Typ: Liste aller StaticEntries dieses Typen als json
        return MapEntry(
            type.toString(),
            _staticEntries[type]
                .map((StaticEntry staticEntry) {
                  return json.encode(staticEntry);
                })
                .toList()
                .toString());
      });
      print(writeMap);
      StorageUtils.writeToFile(json.encode(writeMap), "staticEntries.json");
    } catch (e) {
      print("Fehler in saveStaticEntries: " + e.toString());
    }
  }

  loadStaticEntries() async {
    _staticEntries = {
      ConsumptionStaticEntry: <ConsumptionStaticEntry>[],
      PublicStaticEntry: <PublicStaticEntry>[],
      PowerStaticEntry: <PowerStaticEntry>[],
      HeatingStaticEntry: <HeatingStaticEntry>[],
      FoodStaticEntry: <FoodStaticEntry>[],
    };

    //Daten lesen
    try {
      //Datei öffnen und decodieren
      var loadFromFile = await StorageUtils.loadFromFile("staticEntries.json");
      if (loadFromFile == null || loadFromFile == "") {
        print('keine staticEntries zum Laden');
        return;
      }
      Map<String, dynamic> resultsAsList = json.decode(loadFromFile);
      resultsAsList.forEach((String type, dynamic entries) {
        List<dynamic> entriesList = json.decode(entries);
        print(entriesList);
        entriesList.forEach((dynamic staticEntryString) {
          addStaticEntry(
              StaticEntry.fromJson(
                staticEntryString,
              ),
              null);
        });
      });
    } catch (e) {
      print("Fehler in loadStaticEntries: " + e.toString());
    }
    print("loadStaticEntries complete");
  }

  ///Fügt einen StaticEntry hinzu und überprüft für alle Tage, ob sie von der Änderung betroffen sind
  ///Wenn toBeUploaded nicht null ist, wird der neue Eintrag an die Friends API weitergegeben
  addStaticEntry(
      StaticEntry staticEntry, Map<String, Map<DateTime, int>> toBeUploaded) {
    //Finde heraus, ob schon ein Eintrag für diesen Tag existiert
    if (_staticEntries[staticEntry.runtimeType].contains(staticEntry)) {
      print(
          "Es wurde versucht, zwei StaticEntries dem gleichen Tag hinzuzufügen:" +
              staticEntry.start.toIso8601String());
      return;
    }
    _staticEntries[staticEntry.runtimeType].add(staticEntry);
    //Sortiere die Liste, sodass der letzte Eintrag das früheste Startdatum hat
    _staticEntries[staticEntry.runtimeType]
        .sort((StaticEntry se2, StaticEntry se1) {
      return se1.start.difference(se2.start).inDays;
    });
    //Update die Zuordnungen der StaticEntries zu den Tagen
    _days.forEach((Day day) {
      _setStaticEntries(day);
    });
    print(staticEntry.runtimeType.toString() +
        " für " +
        DateTimeUtils.toDateString(staticEntry.start));

    ///Neue Daten and Friends API weitergeben
    if (toBeUploaded != null) {
      toBeUploaded[staticEntry.runtimeType.toString()][staticEntry.start] =
          (staticEntry.emissionsPerDay * 1000.0).round();
      saveStaticEntries();
    }
    notifyListeners();
  }

  ///Löscht den StaticEntry vom Typ type, der am Tag start startet, und updated alle Tage
  ///Wenn toBeUploaded nicht null ist, wird 0 als emissions für diesen Eintrag an die Rest API weitergegeben
  deleteStaticEntry(
      DateTime start, Type type, Map<String, Map<DateTime, int>> toBeUploaded) {
    _staticEntries[type].removeWhere((StaticEntry staticEntry) {
      return DateTimeUtils.isSameDay(staticEntry.start, start);
    });
    //TODO: test
    _days = {};

    ///Gebe die Daten an die Friends API weiter
    if (toBeUploaded != null) {
      toBeUploaded[type.toString()][start] = 0;
      saveStaticEntries();
    }
    notifyListeners();
  }

  void _setStaticEntries(Day day) {
    day.dailyEmissions = day.dailyEntryEmissions;
    day.staticEntries.forEach((Type type, StaticEntry staticEntry) {
      //Finde den ersten Eintrag, dessen Startdatum vor oder an dem Tag liegt, und lege ihn fest.
      //Dies findet den richtigen Eitnrag, da die Liste der statischen Einträge absteigend sortiert ist,
      //und die Suche beim letzten Eintrag anfängt. Vielleicht ist das zu unsicher
      day.staticEntries[type] =
          _staticEntries[type].firstWhere((StaticEntry searchEntry) {
        return searchEntry.start.isBefore(day.date) ||
            searchEntry.start.isAtSameMomentAs(day.date);
      }, orElse: () => null);
      if (day.staticEntries[type] == null) {
        day.staticEntries[type] = staticEntry;
      }
      updateTotalDailyEmissions(
          day, day.staticEntries[type].emissionsPerDay, false);
    });
  }
}

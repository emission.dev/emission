import 'dart:convert';

import 'package:eMission/models/shortcut.dart';
import 'package:eMission/utils/storage.dart';
import 'package:scoped_model/scoped_model.dart';

///Verwaltung von Shortcuts im Datenmodell
mixin ShortcutModel on Model {
  ///Liste aller erstellter Shortcuts
  Set<Shortcut> _shortcuts = Set<Shortcut>();

  List<Shortcut> get shortcuts => List.from(_shortcuts);

  ///Fügt den neuen Shortcut hinzu.
  /// ///Falls [updateFile] false ist, wird danach nicht gespeichert
  addShortcut(Shortcut newShortcut, [bool updateFile = true]) {
    if (!_shortcuts.add(newShortcut))
      throw ArgumentError(
          "Achtung! Es wurde versucht, mehrere identische Shortcuts hinzuzufügen");
    print("Added Shortcut");
    if (updateFile) saveShortcuts();
    notifyListeners();
  }

  ///Löscht den Shortcut, der zu diesem [DateTime] erstellt wurde
  ///Diese lässt sich über Shortcut.entry.dateId erreichen
  dynamic deleteShortcut(DateTime dateId) {
    Shortcut removed = _shortcuts.singleWhere((Shortcut shortcut) {
      return shortcut.entry.dateId.isAtSameMomentAs(dateId);
    }, orElse: () => null);
    _shortcuts.removeWhere(
      (Shortcut shortcut) {
        return shortcut.entry.dateId.isAtSameMomentAs(dateId);
      },
    );

    if (removed.entry.dateId == dateId)
      print(
          "Shortcut mit dateId ${dateId.toIso8601String()} erfolgreich gelöscht.");
    else
      print("Fehler beim Löschen eines Shortcuts");
    saveShortcuts();
    notifyListeners();
    return removed;
  }

  ///Lädt [Shortcut]s von shortcuts.json. Gibt bei Erfolg [true] zurück
  loadShortcuts() async {
    _shortcuts = Set<Shortcut>();
    //Daten lesen

    //Datei öffnen und decodieren
    var loadFromFile = await StorageUtils.loadFromFile("shortcuts.json");
    if (loadFromFile == null|| loadFromFile == "") {
        print('keine Shortcuts zum Laden');
        return;
      }
        List<dynamic> resultsAsList =
            json.decode(loadFromFile);

    resultsAsList.forEach((dynamic input) {
      addShortcut(Shortcut.fromJson(json.decode(input)), false);
    });
    notifyListeners();
    print("loadShortcuts complete");
  }

  ///Speichert [Shortcuts] in shortcuts.json. Gibt bei Erfolg [true] zurück.
  Future<bool> saveShortcuts() async {
    try {
      List<String> writeList = _shortcuts.map((Shortcut shortcut) {
        return (json.encode(shortcut));
      }).toList();
      StorageUtils.writeToFile(json.encode(writeList), "shortcuts.json");
      return true;
    } catch (e) {
      print("Fehler in saveShortcuts: " + e.toString());
      return false;
    }
  }
}

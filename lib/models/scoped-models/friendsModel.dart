import 'dart:convert';
import 'dart:math';
import 'package:eMission/utils/dateTime.dart';
import 'package:http/http.dart' as http;

import 'package:crypto/crypto.dart';
import 'package:eMission/models/friendData.dart';
import 'package:eMission/utils/storage.dart';
import 'package:scoped_model/scoped_model.dart';

///URL zur WordPress-Api
const String _url = "https://emission.paul7.de/api/friends/v1/query";

///Verwaltet den Zugriff auf die Daten von Freunden, sowie die Freigabe eigener Daten an Freunde
mixin FriendsModel on Model {
  ///Für jeden Freund wird ein [FriendData] Objekt erstellt, dass die Daten des Freundes enthält. Der Schlüssel ist der Username
  Set<FriendData> friendData = {};

  ///Hochzuladende (veränderte) Einträge im Format {"Typ": {"yyyy-mm-dd": emissionen,...},...}
  Map<String, Map<DateTime, int>> toBeUploaded;

  ///Anmeldedaten des Benutzers
  LoginData loginData;

  Map<String, Map<DateTime, int>> standardToBeUploaded = {
    'Entry': {},
    'OtherEntry': {},
    'TransportEntry': {},
    'CompoundEntry': {},
    'StaticEntry': {},
    'ConsumptionStaticEntry': {},
    'PublicStaticEntry': {},
    'FoodStaticEntry': {},
    'PowerStaticEntry': {},
    'HeatingStaticEntry': {}
  };

  ///Lädt [Shortcut]s von shortcuts.json. Gibt bei Erfolg [true] zurück
  loadFriendData() async {
    ///Variablen leeren/initialisieren
    friendData = <FriendData>{};
    loginData = null;
    //Daten lesen

    //Datei öffnen und decodieren
    var loadFromFile = await StorageUtils.loadFromFile("friendData.json");
    if (loadFromFile == null || loadFromFile == "") {
      print('keine FriendData-Daten zum Laden');
      toBeUploaded = standardToBeUploaded;
      return;
    }
    Map<String, dynamic> resultsAsMap = json.decode(loadFromFile);

    if (resultsAsMap['username'] != null)
      //Da die Daten gespeichert wurden, dürfen sie auch wieder gespeichert werden, also [saveLogin] = true
      loginData =
          LoginData(resultsAsMap['username'], resultsAsMap['password'], true);
    resultsAsMap['friendData'].forEach((dynamic input) {
      Map<String, dynamic> resultsMap = json.decode(input);
      FriendData data = FriendData.fromJson(resultsMap);
      friendData.add(data);
    });
    //Map der hochzuladenden Einträge ins richtige Format bringen
    if (resultsAsMap['toBeUploaded'] != null)
      toBeUploaded = resultsAsMap['toBeUploaded']
          .map<String, Map<DateTime, int>>((String type, dynamic submap) {
        return MapEntry<String, Map<DateTime, int>>(type,
            submap.map<DateTime, int>((dynamic date, dynamic emissions) {
          return MapEntry<DateTime, int>(
              DateTime.parse(date), emissions as int);
        }));
      });
    if (toBeUploaded == null) toBeUploaded = standardToBeUploaded;
    await updateAllUserData();
    await updatePosts();
    notifyListeners();
    print("loadFriendData complete");
  }

  ///Speichert [Shortcuts] in shortcuts.json. Gibt bei Erfolg [true] zurück.
  Future<bool> saveFriendData() async {
    print("saveFriendData called");
    try {
      List<String> friendsList = [];
      friendData.forEach((FriendData friendData) {
        friendsList.add(json.encode(friendData).toString());
      });
      Map<String, dynamic> writeMap = {"friendData": friendsList};
      if (loginData != null) {
        if (loginData.saveLogin) {
          print("LoginData speichern");
          writeMap['password'] = loginData.hashedPassword;
          writeMap['username'] = loginData.username;
          //TODO: Find a way to keep toBeUploaded even if the user does not want to saveLgoin. This is important since upload could fail on app close
          //Map der hochzuladenden Einträge ins richtige Format bringen
          writeMap["toBeUploaded"] =
              toBeUploaded.map((String type, Map<DateTime, int> submap) {
            return MapEntry(type, submap.map((DateTime date, int emissions) {
              return MapEntry(date.toIso8601String(), emissions);
            }));
          });
        }
      }
      StorageUtils.writeToFile(json.encode(writeMap), "friendData.json");
      return true;
    } catch (e) {
      print("Fehler in saveFriendData: " + e.toString());
      return false;
    }
  }

  ///Erstellt einen neuen User und speichert diesen in den Einstellungen, sofern
  ///ein User ersetllt werden konnte
  Future<bool> createUser(LoginData newLoginData) async {
    //Erstelle einen Salt
    Random rng = Random.secure();
    double saltDouble = rng.nextDouble();
    List<int> bytesSalt = utf8.encode(saltDouble.toString());
    Digest digestSalt = sha256.convert(bytesSalt);
    Map<String, String> parameters = {
      "salt": digestSalt.toString(),
      "username": newLoginData.username,
      "password": newLoginData.hashedPassword
    };
    //Stelle eine Anfrage an die API
    http.Response response = await http
        .post(
          "$_url${convertToQueryParams(parameters)}",
        )
        .timeout((Duration(seconds: 20)));
    if (response.statusCode == 200) {
      print("User added");
      loginData = newLoginData;
      notifyListeners();
      return true;
    }
    saveFriendData();
    return Future.error(response);
  }

  ///Updated alle Freundesdaten
  updateAllUserData() {
    print("Alle Userdaten holen...");
    if (loginData == null) Future.error("Nicht angemeldet!");
    friendData.forEach((FriendData data) async {
      print("Benutzer: ${data.username}");
      updateUserData(data.username).catchError((onError) {
        print("Fehler beim Erhalten der Daten von User ${data.username}: " +
            onError.toString());
      });
    });
  }

  ///Lädt Datem für den User [username] herunter, die seit [lastUpdateDate] verändert wurden.
  Future<bool> updateUserData(String username,
      [LoginData customLoginData]) async {
    //Initialisieren, alte Daten suchen
    print("Suche nach neuen Daten für User $username");
    if (customLoginData == null) customLoginData = loginData;
    if (customLoginData == null) return Future.error("Nicht angemeldet");
    Map<String, String> parameters = {
      "posts-user": username,
      "username": customLoginData.username,
      "password": customLoginData.hashedPassword
    };
    FriendData oldData = friendData.singleWhere((FriendData data) {
      return username == data.username;
    }, orElse: () => null);
    if (oldData != null)
      parameters["last-update"] =
          DateTimeUtils.toMySqlDate(oldData.lastUpdateDate);

    print("$_url${convertToQueryParams(parameters)}");
    http.Response response = await http
        .get("$_url${convertToQueryParams(parameters)}")
        .timeout(Duration(seconds: 20));

    //Anmeldetest
    if (username == "example" &&
        (response.statusCode == 405 || response.statusCode == 200)) {
      print("Anmeldetest erfolgreich");
      return true;
      //Daten empfangen
    } else if (response.statusCode == 200) {
      print("Daten für User $username erhalten");
      print(response.body);

      ///Falls für diesen Benutzer noch kein FriendData-Objekt existiert, dieses erstellen
      friendData.add(FriendData(username));

      ///neue Daten hinzufügen
      friendData.singleWhere((FriendData data) {
        return username == data.username;
      }).rawData = json.decode(response.body);
      saveFriendData();
      return true;

      ///Wenn der Beispielnutzer aufgerufen wird, keine neuen Daten  setzen
    } else
      return Future.error(response);
  }

  ///Teste, ob die Anmeldedaten korrekt sind, und falls ja, lade alle Freundesdaten herunter
  Future<bool> login(LoginData newLoginData) async {
    if (loginData != null) return Future.error("Bereits angemeldet");
    var response = await updateUserData("example", newLoginData);
    if (!(response is bool)) {
      print("RuntimeType of answer: " + response.runtimeType.toString());
      loginData = null;
      return Future.error(response);
    }
    // updateAllUserData();
    loginData = newLoginData;
    updateAllUserData();
    print("Erfolgreich angemeldet");
    notifyListeners();
    return true;
  }

  ///Lädt Daten für den angemeldeten User zur Friends API hoch, und leert bei Erfolg den Zwischenspeicher für
  ///hochzuladende Daten
  Future<bool> updatePosts() async {
    if (loginData == null) return Future.error("nicht angemeldet");
    StringBuffer uploadString = StringBuffer();
    toBeUploaded.forEach((String type, Map<DateTime, int> category) {
      category.forEach((DateTime date, int emissions) {
        uploadString
            .write("${DateTimeUtils.toMySqlDate(date)}:$emissions:$type,");
      });
    });
    if (uploadString.isEmpty) {
      print(
          "Keine Daten vorhanden, die an die Friends API hochgeladen werden müssten");
      return true;
    }
    Map<String, String> parameters = {
      "username": loginData.username,
      "password": loginData.hashedPassword,
      "posts": uploadString.toString()
    };
    print(convertToQueryParams(parameters));
    http.Response response = await http
        .put("$_url${convertToQueryParams(parameters)}")
        .timeout(Duration(seconds: 20));
    if (response.statusCode == 200) {
      print("${toBeUploaded.length} Einträge an Friends API gesendet");
      toBeUploaded = standardToBeUploaded;
      return true;
    } else
      return Future.error(response.body);
  }

  deleteUser() async {
    if (loginData == null) return null;
    final parameters = {
      "username": loginData.username,
      "password": loginData.hashedPassword
    };
    http.Response response = await http
        .delete("$_url${convertToQueryParams(parameters)}")
        .timeout(Duration(seconds: 20));

    if (response.statusCode == 200) {
      print("Nutzer und seine Daten erfolgreich gelöscht");
      return true;
    } else
      return Future.error(response);
  }

  //Ändert Benutzername und/oder Passwort und Salt. Falls einer der beiden Werte nicht geändert werden soll, null einsetzen
  Future<bool> updateUser(LoginData newLoginData) async {
    if (loginData == null) return Future.error("Nicht angemeldet");
    final parameters = {
      "username": loginData.username,
      "password": loginData.hashedPassword
    };
    if (newLoginData.hashedPassword != null) {
      //Erstelle einen Salt
      Random rng = new Random.secure();
      double saltDouble = rng.nextDouble();
      List<int> bytesSalt = utf8.encode(saltDouble.toString());
      Digest digestSalt = sha256.convert(bytesSalt);

      parameters["new-salt="] = digestSalt.toString();
      parameters["&new-password="] = newLoginData.hashedPassword;
    }
    if (newLoginData.username != null)
      parameters["new-username="] = newLoginData.username;

    //Stelle eine Anfrage an die API
    http.Response response = await http
        .patch(
          "$_url$parameters",
        )
        .timeout(Duration(seconds: 20));
    if (response.statusCode == 200) {
      if (newLoginData.username != null)
        loginData.username = newLoginData.username;
      if (newLoginData.hashedPassword != null)
        loginData.hashedPassword = newLoginData.hashedPassword;
      return true;
    } else
      return Future.error(response);
  }

  ///Verwandelt eine Map aus Parametern in ein Format, dass in eine URL eingefügt werden kann
  ///(?key1=value1&key2=value2...)
  String convertToQueryParams(Map<String, String> map) {
    StringBuffer sb = StringBuffer("?");
    map.forEach((String key, String value) {
      sb.write("$key=$value&");
    });
    return sb.toString();
  }
}

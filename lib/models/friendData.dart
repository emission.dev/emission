import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:eMission/models/staticEntry.dart';
import 'package:eMission/utils/dateTime.dart';

///Enthält alle Daten eines Freundes (Name, Einträge)
///Mit dem Gleichheitszeichen kann herausgefunden werden, ob zwei Einträge
///demselben Benutzer angehören
class FriendData {
  ///Benutzername des Freundes
  final String username;

  ///Enthält die Daten, die von der Friends API empfangen wurden
  Map<String, Map<DateTime, int>> _rawData = <String, Map<DateTime, int>>{
    'Entry': {},
    'OtherEntry': {},
    'TransportEntry': {},
    'CompoundEntry': {},
    'StaticEntry': {},
    'ConsumptionStaticEntry': {},
    'PublicStaticEntry': {},
    'FoodStaticEntry': {},
    'PowerStaticEntry': {},
    'HeatingStaticEntry': {}
  };

  ///Erstes Datum, an dem Einträge gefunden wurden. Davor wird angenommen, dass der Freund die App nicht benutzt hat
  DateTime firstEntryDate;

  //Das Datum, an dem zuletzt Daten dieses Freundes angefragt wurden
  DateTime lastUpdateDate;
  FriendData(this.username);

  Map<String, Map<DateTime, int>> get rawData => _rawData;

  ///Überschreibe die neuen Map-Einträge, aber behalte die alten
  ///Nimmt als Argument eine einmal decodierte HTTP-Response
  set rawData(dynamic input) {
    if (firstEntryDate == null) firstEntryDate = DateTime.now();
    print(input);
    lastUpdateDate = DateTime.now();
    if (input != null && !(input is List)) {
      Map<String, dynamic> map = input;
      //RawData überschreiben
      map.forEach((String type, dynamic entry) {
        Map<DateTime, int> map = {};
        entry.forEach((String string, dynamic emissions) {
          DateTime date = DateTime.parse(string);
          // wenn dieser Eintrag vor dem ersten bisher gefundenen ist, das datum updaten
          if (date.isBefore(firstEntryDate)) firstEntryDate = date;
          //Wenn die Emissionen 0 sind, lösche den existierenden Eintrag
          //emissions=0 ist eine Nachricht vom Server, dass ein bestehender Eintrag gelöscht werden soll
          if (emissions as int == 0) {
            map.remove(date);
          }
          //Wenn das Datum vor 1900 ist, soll nichts passieren. Dies ist notwendigm, weil fehlerhafte
          //Eingaben als 00-00-0000 von MySQL angenommen werden. Diese sollen entfernt werden.
          else if (date.isBefore(DateTime(1900))) {
            print("falsches Format eines Eintrages. Datum: " +
                date.toIso8601String());
          } else
            map[date] = (emissions as int);
        });
        if (type.isNotEmpty) {
          if (_rawData[type] == null) _rawData[type] = {};
          _rawData[type].addAll(map);
        }
      });
    }
    print("new RawData: " + _rawData.toString());
  }

  factory FriendData.fromJson(Map<String, dynamic> entryMap) {
    return FriendData(entryMap['username'])
      ..lastUpdateDate = DateTime.parse(entryMap['lastUpdate'])
      ..firstEntryDate = DateTime.parse(entryMap["firstEntry"])
      .._rawData = entryMap['rawData'].map<String, Map<DateTime, int>>(
        (String type, dynamic submap) {
          return MapEntry<String, Map<DateTime, int>>(
            type,
            submap.map<DateTime, int>(
              (dynamic date, dynamic emissions) {
                return MapEntry<DateTime, int>(
                    DateTime.parse(date), emissions as int);
              },
            ),
          );
        },
      );
  }

  Map<String, dynamic> toJson() {
    return {
      "username": username,
      "lastUpdate": lastUpdateDate.toString(),
      "firstEntry": firstEntryDate.toString(),
      "rawData": rawData.map((String type, Map<DateTime, int> map) {
        return MapEntry(type, map.map((DateTime date, int emissions) {
          return MapEntry(date.toString(), emissions);
        }));
      })
    };
  }

  ///Gibt alle Emissionen des angegebenen Tages dieses Freundes in KG zurück
  ///Gibt 0 zurück, falls für diesen Tag noch keine Daten vorhanden sind
  ///d.h. der User benutzt eMission wahrscheinlich erst nach dem angefragten Datum
  ///Mit allowedTypes können einzelne Eintragstypen ausgeschlossen werden
  double getEmissions(DateTime date,
      [Map<String, bool> allowedTypes = const {
        'Entry': true,
        'OtherEntry': true,
        'TransportEntry': true,
        'CompoundEntry': true,
        'StaticEntry': true,
        'ConsumptionStaticEntry': true,
        'PublicStaticEntry': true,
        'FoodStaticEntry': true,
        'PowerStaticEntry': true,
        'HeatingStaticEntry': true
      }]) {
    if (date.isBefore(firstEntryDate)) return 0;
    double emissions = 0;
    _rawData.forEach((String category, Map<DateTime, int> map) {
      if (allowedTypes[category] && !category.contains("StaticEntry"))
        map.forEach((DateTime entryDate, int entryEmissions) {
          if (DateTimeUtils.isSameDay(date, entryDate))
            emissions += entryEmissions / 1000;
        });
      //StaticEntries behandeln
      else if (allowedTypes[category]) {
        double staticEmissions = 0;
        DateTime staticEntryDate = DateTime(0);
        map.forEach((DateTime entryDate, int entryEmissions) {
          //Finde den passenden staticEntry und wähle ihn aus
          if (date.isAfter(entryDate) && date.isAfter(staticEntryDate)) {
            staticEmissions = entryEmissions / 1000;
            staticEntryDate = entryDate;
          }
        });
        //Falls keiner gefunden wurde, Default-Werte benutzen
        if (staticEmissions == 0) {
          final StaticEntry averageStaticEntry = StaticEntry.average(category);
          staticEmissions += averageStaticEntry.emissionsPerDay;
        }
        emissions += staticEmissions;
      }
    });
    return emissions;
  }

  bool operator ==(o) => o is FriendData && o.username == username;
  int get hashCode => username.hashCode;
}

class LoginData {
  //Der Nutzername
  String username;
  //Das Passwort des Nutzers als SHA-256 gehashed
  String hashedPassword;
  bool saveLogin;
  LoginData(this.username, this.hashedPassword, this.saveLogin);
  //Falls das Passwort noch nicht gehashed ist, kann dieser Konstruktor das übernehmen
  LoginData.hashPassword(String name, String unhashedPassword, this.saveLogin)
      : hashedPassword =
            sha256.convert(utf8.encode(unhashedPassword.trim())).toString(),
        username = name.trim();
}

import 'dart:math';
import 'package:eMission/utils/storage.dart';
import 'package:eMission/models/directions.dart';
import 'package:flutter/material.dart';

///Die Art des Fahrzeugs (Motorfahrzeug, Öffentlich, Flugzeug)
enum VehicleType { plane, motorizedVehicle, public }
const Map<VehicleType, String> _vehicleTypeEnumMap = {
  VehicleType.motorizedVehicle: "MotorizedVehicle",
  VehicleType.plane: "Plane",
  VehicleType.public: "Public",
};

class Vehicle {
  Vehicle();
  double calculateEmissions(double distance) {
    return null;
  }

  factory Vehicle.fromJson(Map<String, dynamic> json) {
    switch (StorageUtils.enumDecode(_vehicleTypeEnumMap, json["vehicleType"])) {
      case VehicleType.plane:
        return Plane.fromJson(json);
      case VehicleType.motorizedVehicle:
        return MotorizedVehicle.fromJson(json);
      case VehicleType.public:
        return PublicTransport.fromJson(json);
    }
    return null;
  }
}

// Maybe later
// enum planeModels
///Die Flugklasse, die der Nutzer gebucht hat
enum PassengerClass { economy, premiumEconomy, business, first }

const _passengerClassEnumMap = <PassengerClass, String>{
  PassengerClass.economy: "Economy",
  PassengerClass.premiumEconomy: "Premiumm Economy",
  PassengerClass.business: "Business",
  PassengerClass.first: "First",
};
const _passengerClassMultiplicator = <PassengerClass, double>{
  PassengerClass.economy: 1,
  PassengerClass.first: 2.5,
  PassengerClass.premiumEconomy: 1.275,
  PassengerClass.business: 1.875
};

class Plane extends Vehicle {
  PassengerClass passengerClass;
  Plane({this.passengerClass = PassengerClass.economy}) : super();

  @override
  ///Calculates the meissions for flying depending on disstance and passenger class.
  ///For implementation details, see https://www.myclimate.org/fileadmin/user_upload/myclimate_-_home/01_Information/01_About_myclimate/09_Calculation_principles/Documents/myclimate-flight-calculator-documentation_EN.pdf
  double calculateEmissions(double distance) {

    double dist;
    double shortDist = _calcShort(distance, passengerClass);
    double longDist = _calcLong(distance, passengerClass);
    if (distance < 1500)
      dist = shortDist;
    else if (distance > 2500)
      dist = longDist;
    else
      dist = shortDist + (distance - 1500) * (longDist - shortDist) / (1000);

    return dist;
  }

  double _calcShort(double x, PassengerClass passengerClass) {
    x += 50;
    double classMulitplyer;
    switch (passengerClass) {
      case PassengerClass.economy:
        classMulitplyer = 0.96;
        break;
      case PassengerClass.business:
        classMulitplyer = 1.26;
        break;
      case PassengerClass.first:
        classMulitplyer = 2.4;
        break;
      default:
        classMulitplyer = 1;
    }
    return (3.87871e-5 * pow(x, 2) + 2.9866 * x + 1263.42) /
        (158.44 * 0.77) *
        0.951 *
        classMulitplyer *
        (3.15 * 2 + 0.51);
  }

  double _calcLong(double x, PassengerClass passengerClass) {
    x += 125;
    double classMulitplyer;
    switch (passengerClass) {
      case PassengerClass.economy:
        classMulitplyer = 0.8;
        break;
      case PassengerClass.business:
        classMulitplyer = 1.54;
        break;
      case PassengerClass.first:
        classMulitplyer = 2.4;
        break;
      default:
        classMulitplyer = 1;
    }
    return (0.000134576 * pow(x, 2) + 6.1798 * x + 3446.2) /
        (280.39 * 0.77) *
        0.951 *
        classMulitplyer *
        (3.15 * 2 + 0.51);
  }

  factory Plane.fromJson(Map<String, dynamic> json) {
    return Plane(
        passengerClass: StorageUtils.enumDecode(
            _passengerClassEnumMap, json["passengerClass"]));
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      "vehicleType": _vehicleTypeEnumMap[VehicleType.plane],
      "passengerClass": _passengerClassEnumMap[passengerClass]
    };
  }

  @override
  String toString() {
    return "Flug";
  }
}

///Legt fest, welchen Treibstuff ein [MotorizedVehicle] verwendet.
///Zur Auswahl stehen Diesel, Benzin, Elektro, und Elektro (100% Ökostrom)
enum Fuel { diesel, gasoline, electric, ecoEletric }
const _fuelEnumMap = <Fuel, String>{
  Fuel.diesel: "Diesel",
  Fuel.gasoline: "Gasoline",
  Fuel.electric: "Electric",
  Fuel.ecoEletric: "EcoElectric",
};
const fuelUnitMap = <Fuel, String>{
  Fuel.diesel: "Liter",
  Fuel.gasoline: "Liter",
  Fuel.electric: "kWh",
  Fuel.ecoEletric: "kWh",
};
//TODO: nur vorläufige Werte
const Map<Fuel, double> _fuelMultiplicator = {
  Fuel.diesel: 2.64,
  Fuel.ecoEletric: 0.05,
  Fuel.electric: 0.489,
  Fuel.gasoline: 2.36,
};

class MotorizedVehicle extends Vehicle {
  ///Verbrauch pro 100km in Litern oder kWh
  double fuelConsumption;

  ///Anzahl der beförderten Passagiere
  int passengers;

  ///Legt fest, welchen Treibstuff ein [MotorizedVehicle] verwendet.
  ///Zur Auswahl stehen Diesel, Benzin, Elektro, und Elektro (100% Ökostrom)
  Fuel fuel;
  MotorizedVehicle(
      {@required this.fuelConsumption,
      @required this.fuel,
      this.passengers = 1});
  @override
  double calculateEmissions(double distance) {
    return ((distance / 100) * _fuelMultiplicator[fuel] * fuelConsumption) /
        passengers;
  }

  factory MotorizedVehicle.fromJson(Map<String, dynamic> json) {
    return MotorizedVehicle(
      fuelConsumption: json["consumption"],
      fuel: StorageUtils.enumDecode(_fuelEnumMap, json["fuel"]),
      passengers: json["passengers"],
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      "vehicleType": _vehicleTypeEnumMap[VehicleType.motorizedVehicle],
      "consumption": fuelConsumption,
      "type": _fuelEnumMap[fuel],
      "passengers": passengers,
    };
  }

  @override
  String toString() {
    return "Fahrt";
  }
}

// Verkehrsmittel
// [kgCO2-Äqu./ Pkm]
// ÖPNV (Metro, Straßenbahn, Bus)
// 0,076
// Bahn Nahverkehr (S-Bahn, RE, RB)
// 0,101
// Bahn Fernverkehr (ICE, IC, EC)
// 0,064
class PublicTransport extends Vehicle {
  PublicVehicleType publicVehicleType;
  PublicTransport(this.publicVehicleType);
  @override
  double calculateEmissions(double distance) {
    //TODO: Find out values
    switch (publicVehicleType) {
      case PublicVehicleType.bus:
        return distance * 0.076;
      case PublicVehicleType.cable_car:
        return distance * 0.3;
      case PublicVehicleType.commuter_train:
        return distance * 0.101;
      case PublicVehicleType.ferry:
        return distance * 0.3;
      case PublicVehicleType.funicular:
        return distance * 0.3;
      case PublicVehicleType.gondola_lift:
        return distance * 0.3;
      case PublicVehicleType.heavy_rail:
        return distance * 0.101;
      case PublicVehicleType.high_speed_train:
        return distance * 0.064;
      case PublicVehicleType.intercity_bus:
        return distance * 0.3;
      case PublicVehicleType.long_distance_train:
        return distance * 0.064;
      case PublicVehicleType.metro_rail:
        return distance * 0.101;
      case PublicVehicleType.monorail:
        return distance * 0.3;
      case PublicVehicleType.other:
        return distance * 0.3;
      case PublicVehicleType.rail:
        return distance * 0.064;
      case PublicVehicleType.share_taxi:
        return distance * 0.3;
      case PublicVehicleType.subway:
        return distance * 0.076;
      case PublicVehicleType.tram:
        return distance * 0.076;
      case PublicVehicleType.trolleybus:
        return distance * 0.3;
    }
    return null;
  }

  factory PublicTransport.fromJson(Map<String, dynamic> json) {
    return PublicTransport(StorageUtils.enumDecode(
      publicVehicleTypeEnumMap,
      json["publicVehicleType"],
    ));
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      "vehicleType": _vehicleTypeEnumMap[VehicleType.public],
      "publicVehicleType": publicVehicleTypeEnumMap[publicVehicleType]
    };
  }

  @override
  String toString() {
    switch (publicVehicleType) {
      case PublicVehicleType.bus:
        return "Busfahrt";
      case PublicVehicleType.cable_car:
        return "Seilbahnfart";
      case PublicVehicleType.commuter_train:
        return "Pendlerzugfahrt";
      case PublicVehicleType.ferry:
        return "Fährreise";
      case PublicVehicleType.funicular:
        return "Gondelfahrt";
      case PublicVehicleType.gondola_lift:
        return "Gondelfahrt";
      case PublicVehicleType.heavy_rail:
        return "Regionalbahnfahrt";
      case PublicVehicleType.high_speed_train:
        return "Schnellzugfahrt";
      case PublicVehicleType.intercity_bus:
        return "Busfahrt";
      case PublicVehicleType.long_distance_train:
        return "Zugfahrt";
      case PublicVehicleType.metro_rail:
        return "Metro";
      case PublicVehicleType.monorail:
        return "Monorailfahrt";
      case PublicVehicleType.other:
        return "Fahrt";
      case PublicVehicleType.rail:
        return "Zugfahrt";
      case PublicVehicleType.share_taxi:
        return "Taxifahrt";
      case PublicVehicleType.subway:
        return "U-Bahn-Fahrt";
      case PublicVehicleType.tram:
        return "Tramfahrt";
      case PublicVehicleType.trolleybus:
        return "Busfahrt";
    }
    return null;
  }
}

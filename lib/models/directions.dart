import 'dart:core';
import 'package:eMission/utils/storage.dart';

///Mögliche Statuscodes der Directions API. Siehe Dokumentation von [Directions]
enum StatusCode {
  ok,
  notFound,
  zeroResults,
  maxWaypointsExceeded,
  maxRouteLengthExceeded,
  invalidRequest,
  overDailyLimit,
  overQueryLimit,
  requestDenied,
  unknownError,
  //Existiert nicht in Google directions api, wird nur von mir verwendet
  timeout
}
const Map<StatusCode, String> statusCodeEnumMap = {
  StatusCode.ok: "OK",
  StatusCode.notFound: "NOT_FOUND",
  StatusCode.zeroResults: "ZERO_RESULTS",
  StatusCode.maxWaypointsExceeded: "MAX_WAYPOINTS_EXCEEDED",
  StatusCode.maxRouteLengthExceeded: "MAX_ROUTE_LENGTH_EXCEEDED",
  StatusCode.invalidRequest: "INVALID_REQUEST",
  StatusCode.overDailyLimit: "OVER_DAILY_LIMIT",
  StatusCode.overQueryLimit: "OVER_QUERY_LIMIT",
  StatusCode.requestDenied: "REQUEST_DENIED",
  StatusCode.unknownError: "UNKNOWN_ERROR",
  StatusCode.timeout: "TIMEOUT",
};

///Made with https://javiercbk.github.io/json_to_dart/
///Documentation: https://developers.google.com/maps/documentation/directions/intro#DirectionsResponseElements
class Directions {
  List<GeocodedWaypoints> geocodedWaypoints;
  List<DirectionRoute> routes;
  StatusCode status;
  String errorMessage;

  Directions(
      {this.geocodedWaypoints, this.routes, this.status, this.errorMessage});

  Directions.fromJson(Map<String, dynamic> json) {
    if (json['geocoded_waypoints'] != null) {
      geocodedWaypoints = new List<GeocodedWaypoints>();
      json['geocoded_waypoints'].forEach((v) {
        geocodedWaypoints.add(new GeocodedWaypoints.fromJson(v));
      });
    }
    if (json['routes'] != null) {
      routes = new List<DirectionRoute>();
      json['routes'].forEach((v) {
        routes.add(new DirectionRoute.fromJson(v));
      });
    }
    status = StorageUtils.enumDecode(statusCodeEnumMap, json['status']);
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.geocodedWaypoints != null) {
      data['geocoded_waypoints'] =
          this.geocodedWaypoints.map((v) => v.toJson()).toList();
    }
    if (this.routes != null) {
      data['routes'] = this.routes.map((v) => v.toJson()).toList();
    }
    data['status'] = statusCodeEnumMap[this.status];
    if (errorMessage != null) {
      data['error_message'] = this.errorMessage;
    }
    return data;
  }
}

class GeocodedWaypoints {
  String geocoderStatus;

  String placeId;
  List<String> types;

  GeocodedWaypoints({this.geocoderStatus, this.placeId, this.types});

  GeocodedWaypoints.fromJson(Map<String, dynamic> json) {
    geocoderStatus = json['geocoder_status'];
    placeId = json['place_id'];
    if (json['types'] != null) types = json['types'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['geocoder_status'] = this.geocoderStatus;
    data['place_id'] = this.placeId;
    data['types'] = this.types;
    return data;
  }
}

class DirectionRoute {
  Bounds bounds;
  //TODO: display. Legally required
  String copyrights;
  List<Leg> legs;
  OverviewPolyline overviewPolyline;
  String summary;
  List<String> warnings;
  List<int> waypointOrder;
  Fare fare;

  DirectionRoute(
      {this.bounds,
      this.copyrights,
      this.legs,
      this.overviewPolyline,
      this.summary,
      this.warnings,
      this.waypointOrder,
      this.fare});

  DirectionRoute.fromJson(Map<String, dynamic> json) {
    bounds =
        json['bounds'] != null ? new Bounds.fromJson(json['bounds']) : null;
    fare = json['fare'] != null ? new Fare.fromJson(json['bounds']) : null;
    copyrights = json['copyrights'];
    if (json['legs'] != null) {
      legs = new List<Leg>();
      json['legs'].forEach((v) {
        legs.add(new Leg.fromJson(v));
      });
    }
    overviewPolyline = json['overview_polyline'] != null
        ? new OverviewPolyline.fromJson(json['overview_polyline'])
        : null;
    summary = json['summary'];
    warnings = json['warnings'].cast<String>();
    waypointOrder = json['waypoint_order'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.bounds != null) {
      data['bounds'] = this.bounds.toJson();
    }
    if (this.fare != null) {
      data['fare'] = this.fare.toJson();
    }
    data['copyrights'] = this.copyrights;
    if (this.legs != null) {
      data['legs'] = this.legs.map((v) => v.toJson()).toList();
    }
    if (this.overviewPolyline != null) {
      data['overview_polyline'] = this.overviewPolyline.toJson();
    }
    data['summary'] = this.summary;
    data['warnings'] = this.warnings;
    data['waypoint_order'] = this.waypointOrder;
    return data;
  }
}

class Fare {
  String currency;
  num value;
  String text;
  Fare({this.currency, this.value, this.text});
  Fare.fromJson(Map<String, dynamic> jsonData) {
    currency = jsonData['currency'];
    value = jsonData['value'];
    text = jsonData['text'];
  }
  Map<String, dynamic> toJson() {
    return {
      "currency": currency,
      "value": value,
      "text": text,
    };
  }
}

class Bounds {
  Coordinates northeast;
  Coordinates southwest;

  Bounds({this.northeast, this.southwest});

  Bounds.fromJson(Map<String, dynamic> json) {
    northeast = json['northeast'] != null
        ? new Coordinates.fromJson(json['northeast'])
        : null;
    southwest = json['southwest'] != null
        ? new Coordinates.fromJson(json['southwest'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.northeast != null) {
      data['northeast'] = this.northeast.toJson();
    }
    if (this.southwest != null) {
      data['southwest'] = this.southwest.toJson();
    }
    return data;
  }
}

class Coordinates {
  double lat;
  double lng;

  Coordinates({this.lat, this.lng});

  Coordinates.fromJson(Map<String, dynamic> json) {
    lat = json['lat'];
    lng = json['lng'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    return data;
  }
}

class Leg {
  //Only for mode= transit
  Time arrivalTime;
  //Only for mode= transit
  Time departureTime;
  Distance distance;
  DirectionsDuration duration;
  //Only used by Premium Directions API requests
  DirectionsDuration durationInTraffic;
  String endAddress;
  EndLocation endLocation;
  String startAddress;
  StartLocation startLocation;
  List<DirectionStep> steps;

  Leg(
      {this.arrivalTime,
      this.departureTime,
      this.distance,
      this.duration,
      this.endAddress,
      this.endLocation,
      this.startAddress,
      this.startLocation,
      this.steps,
      this.durationInTraffic});

  Leg.fromJson(Map<String, dynamic> json) {
    arrivalTime = json['arrival_time'] != null
        ? new Time.fromJson(json['arrival_time'])
        : null;
    departureTime = json['departure_time'] != null
        ? new Time.fromJson(json['departure_time'])
        : null;
    distance = json['distance'] != null
        ? new Distance.fromJson(json['distance'])
        : null;
    duration = json['duration'] != null
        ? new DirectionsDuration.fromJson(json['duration'])
        : null;
    durationInTraffic = json['duration_in_traffic'] != null
        ? new DirectionsDuration.fromJson(json['duration_in_traffic'])
        : null;
    endAddress = json['end_address'];
    endLocation = json['end_location'] != null
        ? new EndLocation.fromJson(json['end_location'])
        : null;
    startAddress = json['start_address'];
    startLocation = json['start_location'] != null
        ? new StartLocation.fromJson(json['start_location'])
        : null;
    if (json['steps'] != null) {
      steps = new List<DirectionStep>();
      json['steps'].forEach((v) {
        steps.add(new DirectionStep.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.arrivalTime != null) {
      data['arrival_time'] = this.arrivalTime.toJson();
    }
    if (this.departureTime != null) {
      data['departure_time'] = this.departureTime.toJson();
    }
    if (this.distance != null) {
      data['distance'] = this.distance.toJson();
    }
    if (this.duration != null) {
      data['duration'] = this.duration.toJson();
    }
    if (this.durationInTraffic != null) {
      data['duration_in_traffic'] = this.durationInTraffic.toJson();
    }
    data['end_address'] = this.endAddress;
    if (this.endLocation != null) {
      data['end_location'] = this.endLocation.toJson();
    }
    data['start_address'] = this.startAddress;
    if (this.startLocation != null) {
      data['start_location'] = this.startLocation.toJson();
    }
    if (this.steps != null) {
      data['steps'] = this.steps.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Time {
  String text;
  String timeZone;
  DateTime value;

  Time({this.text, this.timeZone, this.value});

  Time.fromJson(Map<String, dynamic> json) {
    text = json['text'];
    timeZone = json['time_zone'];
    value = DateTime.fromMillisecondsSinceEpoch(json['value'] * 1000);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['text'] = this.text;
    data['time_zone'] = this.timeZone;
    data['value'] = this.value.millisecondsSinceEpoch / 1000;
    return data;
  }
}

class Distance {
  String text;
  int value;

  Distance({this.text, this.value});

  Distance.fromJson(Map<String, dynamic> json) {
    text = json['text'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['text'] = this.text;
    data['value'] = this.value;
    return data;
  }
}

class DirectionsDuration {
  String text;

  ///In the response, this is an integer containing the amount of seconds. For convenience, I use dart's Duration class instead. JSON (de)serialization still uses integers
  Duration value;

  ///This is the duration property of the response. Was renamed to DirectionsDuration to avoid confusion with Dart's duration
  DirectionsDuration({this.text, this.value});

  DirectionsDuration.fromJson(Map<String, dynamic> json) {
    text = json['text'];
    value = Duration(seconds: json['value']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['text'] = this.text;
    data['value'] = this.value.inSeconds;
    return data;
  }
}

class EndLocation {
  double lat;
  double lng;

  EndLocation({this.lat, this.lng});

  EndLocation.fromJson(Map<String, dynamic> json) {
    lat = json['lat'];
    lng = json['lng'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    return data;
  }
}

class StartLocation {
  double lat;
  double lng;

  StartLocation({this.lat, this.lng});

  StartLocation.fromJson(Map<String, dynamic> json) {
    lat = json['lat'];
    lng = json['lng'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    return data;
  }
}

class DirectionStep {
  Distance distance;
  DirectionsDuration customDuration;
  EndLocation endLocation;
  String htmlInstructions;
  Polyline polyline;
  StartLocation startLocation;
  TransitDetails transitDetails;
  String travelMode;
  String maneuver;
  List<DirectionStep> steps;

  DirectionStep(
      {this.distance,
      this.customDuration,
      this.endLocation,
      this.htmlInstructions,
      this.polyline,
      this.startLocation,
      this.transitDetails,
      this.travelMode,
      this.maneuver,
      this.steps});

  DirectionStep.fromJson(Map<String, dynamic> json) {
    if (json['steps'] != null) {
      steps = new List<DirectionStep>();
      json['steps'].forEach((v) {
        steps.add(new DirectionStep.fromJson(v));
      });
    }
    distance = json['distance'] != null
        ? new Distance.fromJson(json['distance'])
        : null;
    customDuration = json['duration'] != null
        ? new DirectionsDuration.fromJson(json['duration'])
        : null;
    endLocation = json['end_location'] != null
        ? new EndLocation.fromJson(json['end_location'])
        : null;
    htmlInstructions = json['html_instructions'];
    polyline = json['polyline'] != null
        ? new Polyline.fromJson(json['polyline'])
        : null;
    startLocation = json['start_location'] != null
        ? new StartLocation.fromJson(json['start_location'])
        : null;
    transitDetails = json['transit_details'] != null
        ? new TransitDetails.fromJson(json['transit_details'])
        : null;
    travelMode = json['travel_mode'];
    maneuver = json['maneuver'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.steps != null) {
      data['steps'] = this.steps.map((v) => v.toJson()).toList();
    }
    if (this.distance != null) {
      data['distance'] = this.distance.toJson();
    }
    if (this.customDuration != null) {
      data['duration'] = this.customDuration.toJson();
    }
    if (this.endLocation != null) {
      data['end_location'] = this.endLocation.toJson();
    }
    data['html_instructions'] = this.htmlInstructions;
    if (this.polyline != null) {
      data['polyline'] = this.polyline.toJson();
    }
    if (this.startLocation != null) {
      data['start_location'] = this.startLocation.toJson();
    }
    if (this.transitDetails != null) {
      data['transit_details'] = this.transitDetails.toJson();
    }
    data['travel_mode'] = this.travelMode;
    data['maneuver'] = this.maneuver;
    return data;
  }
}

class TransitDetails {
  Stop arrivalStop;
  Time arrivalTime;
  Stop departureStop;
  Time departureTime;
  String headsign;
  Line line;
  int numStops;

  ///Same as [value] of [DirectionsDuration]
  Duration headway;

  TransitDetails(
      {this.arrivalStop,
      this.arrivalTime,
      this.departureStop,
      this.departureTime,
      this.headsign,
      this.line,
      this.numStops,
      this.headway});

  TransitDetails.fromJson(Map<String, dynamic> json) {
    headway =
        json['headway'] != null ? new Duration(seconds: json['headway']) : null;
    arrivalStop = json['arrival_stop'] != null
        ? new Stop.fromJson(json['arrival_stop'])
        : null;
    arrivalTime = json['arrival_time'] != null
        ? new Time.fromJson(json['arrival_time'])
        : null;
    departureStop = json['departure_stop'] != null
        ? new Stop.fromJson(json['departure_stop'])
        : null;
    departureTime = json['departure_time'] != null
        ? new Time.fromJson(json['departure_time'])
        : null;
    headsign = json['headsign'];
    line = json['line'] != null ? new Line.fromJson(json['line']) : null;
    numStops = json['num_stops'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.arrivalStop != null) {
      data['arrival_stop'] = this.arrivalStop.toJson();
    }
    if (this.headway != null) {
      data['headway'] = this.headway.inSeconds;
    }
    if (this.arrivalTime != null) {
      data['arrival_time'] = this.arrivalTime.toJson();
    }
    if (this.departureStop != null) {
      data['departure_stop'] = this.departureStop.toJson();
    }
    if (this.departureTime != null) {
      data['departure_time'] = this.departureTime.toJson();
    }
    data['headsign'] = this.headsign;
    if (this.line != null) {
      data['line'] = this.line.toJson();
    }
    data['num_stops'] = this.numStops;
    return data;
  }
}

class Stop {
  Coordinates location;
  String name;

  Stop({this.location, this.name});

  Stop.fromJson(Map<String, dynamic> json) {
    location = json['location'] != null
        ? new Coordinates.fromJson(json['location'])
        : null;
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    data['name'] = this.name;
    return data;
  }
}

class Line {
  List<Agencies> agencies;
  String shortName;
  DirectionsVehicle vehicle;
  String name;
  String icon;

  Line({this.agencies, this.shortName, this.vehicle, this.name, this.icon});

  Line.fromJson(Map<String, dynamic> json) {
    if (json['agencies'] != null) {
      agencies = new List<Agencies>();
      json['agencies'].forEach((v) {
        agencies.add(new Agencies.fromJson(v));
      });
    }
    shortName = json['short_name'];
    icon = json['icon'];
    vehicle = json['vehicle'] != null
        ? new DirectionsVehicle.fromJson(json['vehicle'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.agencies != null) {
      data['agencies'] = this.agencies.map((v) => v.toJson()).toList();
    }
    data['short_name'] = this.shortName;
    data['icon'] = this.icon;
    if (this.vehicle != null) {
      data['vehicle'] = this.vehicle.toJson();
    }
    return data;
  }
}

class Agencies {
  String name;
  String phone;
  String url;

  Agencies({this.name, this.phone, this.url});

  Agencies.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    phone = json['phone'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['url'] = this.url;
    return data;
  }
}

///Art des öffentlichen Verkehrs. Die Werte entsprechen den Rückgabewerten der Directions API (https://developers.google.com/maps/documentation/directions/intro Tabelle Vehicle Type)
enum PublicVehicleType {
  rail,
  metro_rail,
  subway,
  tram,
  monorail,
  //z.B. Regionalbahn
  heavy_rail,
  //z.B. S-Bahn
  commuter_train,
  //z.B. ICE
  high_speed_train,
  //z.B. IC
  long_distance_train,
  //Stadtbusse
  bus,
  intercity_bus,
  trolleybus,
  share_taxi,
  ferry,
  cable_car,
  gondola_lift,
  funicular,
  other
}
const Map<PublicVehicleType, String> publicVehicleTypeEnumMap = {
  PublicVehicleType.rail: "RAIL",
  PublicVehicleType.metro_rail: "METRO_RAIL",
  PublicVehicleType.subway: "SUBWAY",
  PublicVehicleType.tram: "TRAM",
  PublicVehicleType.monorail: "MONORAIL",
  PublicVehicleType.heavy_rail: "HEAVY_RAIL",
  PublicVehicleType.commuter_train: "COMMUTER_TRAIN",
  PublicVehicleType.high_speed_train: "HIGH_SPEED_TRAIN",
  PublicVehicleType.long_distance_train: "LONG_DISTANCE_TRAIN",
  PublicVehicleType.bus: "BUS",
  PublicVehicleType.intercity_bus: "INTERCITY_BUS",
  PublicVehicleType.trolleybus: "TROLLEYBUS",
  PublicVehicleType.share_taxi: "SHARE_TAXI",
  PublicVehicleType.ferry: "FERRY",
  PublicVehicleType.cable_car: "CABLE_CAR",
  PublicVehicleType.gondola_lift: "GONDOLA_LIFT",
  PublicVehicleType.funicular: "FUNICULAR",
  PublicVehicleType.other: "OTHER"
};

class DirectionsVehicle {
  String icon;
  String name;
  PublicVehicleType type;
  String localIcon;

  DirectionsVehicle({this.icon, this.name, this.type, this.localIcon});

  DirectionsVehicle.fromJson(Map<String, dynamic> json) {
    if (json['icon'] != null) icon = json['icon'].replaceAll("//", "");
    if (json['local_icon'] != null)
      localIcon = json['local_icon'].replaceAll("//", "");
    name = json['name'];
    type = StorageUtils.enumDecode(
      publicVehicleTypeEnumMap,
      json["type"],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['icon'] = "//" + this.icon;
    data['name'] = this.name;
    data['type'] = publicVehicleTypeEnumMap[type];
    data['localIcon'] = "//" + this.localIcon;
    return data;
  }
}

class Polyline {
  String points;

  Polyline({this.points});

  Polyline.fromJson(Map<String, dynamic> json) {
    points = json['points'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['points'] = this.points;
    return data;
  }
}

class OverviewPolyline {
  String points;

  OverviewPolyline({this.points});

  OverviewPolyline.fromJson(Map<String, dynamic> json) {
    points = json['points'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['points'] = this.points;
    return data;
  }
}

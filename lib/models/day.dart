import 'package:eMission/models/entry.dart';
import 'package:eMission/models/staticEntry.dart';
import 'package:eMission/utils/dateTime.dart';

///Erzeugt einen Tag. Jeder Eintrag wird einem Tag zugeordnet.
///Wenn man einen bestimmten Eintrag sucht, geschieht dies über den Tag
class Day {
  ///Darf nicht um Mitternacht sein
  final DateTime date;

  ///Gesamte Emissionen des Tages
  double dailyEmissions = 0.0;

  ///Emissionen aller Einträge des Tages (ohne statische Einträge)
  double dailyEntryEmissions = 0.0;
  final Set<Entry> entries = <Entry>{};
  Map<Type, StaticEntry> staticEntries = {
    PublicStaticEntry: PublicStaticEntry.average(),
    ConsumptionStaticEntry: ConsumptionStaticEntry.average(),
    HeatingStaticEntry: HeatingStaticEntry.average(),
    PowerStaticEntry: PowerStaticEntry.average(),
    FoodStaticEntry: FoodStaticEntry.average(),
  };
  Day(this.date);
  bool operator ==(other) =>
      other is Day && DateTimeUtils.isSameDay(date, other.date);
  int get hashCode =>
      date.year.hashCode * date.month.hashCode * date.day.hashCode;
}

import 'dart:math';

import 'package:eMission/utils/airports.dart';

///Enthält den Namen, Ort, den IATA-Code und die Koordinaten eines Flughafens
class Airport {
  final num lat, long;
  final String name, city, iataCode;
  ///Wird genutzt, um nicht-airport Einträge in Airport-Liten angezeigt werden sollen
  Airport.info(String infoString)
      : name = infoString,
        city = null,
        iataCode = null,
        lat = null,
        long = null;
  const Airport(this.name, this.city, this.iataCode, this.lat, this.long);
  factory Airport.fromJsom(Map<String, dynamic> json) {
    return Airport(
      json["a"],
      json["b"],
      json["c"],
      json["d"],
      json["e"],
    );
  }
}

///Tools für alles, was mit Standorten zu tun hat, (außer Anfragen an Directions API)
abstract class LocationUtils {

  ///Sucht einen Flughafen mit dem gegebenen IATA-Code. Der Input muss dreistellig sein.
  ///Falls kein Flughafen gefunden wurde, wird ein entsprechender [Airport.info] returned
  static Airport _getAirport(String iataCode) {
    return airports.singleWhere((Airport airport) {
      return airport.iataCode == iataCode.toUpperCase();
    }, orElse: () {
      return Airport.info("Kein Flughafen mit diesem IATA-Code gefunden");
    });
  }

  ///Gibt eine Liste der Flughäfen an, die [query] entsprechen.
  ///Mit [query] wird wie folgt umgegangen:
  ///Weniger als 3 Buchstaben: Kein Ergebnis wird zurückgegeben, da meistens viel zu viele Flughäfen gefunden werden würden.
  ///3 Buchstaben: Der Flughafen mit diesem IATA-Code wird zurückgegeben
  ///Mehr als 3 Buchstaben: Die Flughäfen, deren Name oder Stadt [query] enthält, werden zurückgegeben
  static List<Airport> getAirports(
    String query,
  ) {
    List<Airport> returnList;
    if (query.length == 3)
      returnList = [
        _getAirport(query),
        Airport.info(
            "Gib mehr Zeichen an, um nach Städten oder Namen zu suchen"),
      ];
    else if (query.length > 3)
      returnList = airports.where((Airport airport) {
        try {
          return (airport.name.toLowerCase().contains(query.toLowerCase()) ||
              airport.city.toLowerCase().contains(query.toLowerCase()));
        } catch (e) {
          return false;
        }
      }).toList();
    else
      returnList = [
        Airport.info("Gib mindestens drei Zeichen an"),
      ];
    return returnList;
  }

  ///Errechnet die Entfernung zwischen zwei Punkten
  static double distanceBetweenCoordinates(
      num lat1, num long1, num lat2, num long2) {
    lat1 = lat1 * pi / 180;
    lat2 = lat2 * pi / 180;
    long1 = long1 * pi / 180;
    long2 = long2 * pi / 180;

    return 6378.88 *
        acos(
            sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(long2 - long1));
  }
}
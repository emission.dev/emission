import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:eMission/models/directions.dart';
import 'package:url_launcher/url_launcher.dart';

//TODO: Powered by Google Logo hinzufügen wenn Directions API benutzt.
///Für Anfragen an Google Directions API
enum TravelMode {
  driving,
  walking,
  bycicling,
  transitBus,
  transitSubway,
  transitTrain,
  transitTram,
  transitRail,
  transit
}
Map<TravelMode, String> _travelModeEnumMap = {
  TravelMode.driving: "driving",
  TravelMode.walking: "walking",
  TravelMode.bycicling: "bicycling",
  TravelMode.transitBus: "transit",
  TravelMode.transitSubway: "transit",
  TravelMode.transitTrain: "transit",
  TravelMode.transitTram: "transit",
  TravelMode.transitRail: "transit",
  TravelMode.transit: "transit"
};

///Funktionen zum Interagieren mit der Directions API von Google
abstract class DirectionsUtils {
  ///Gibt eine URL für Google Directions API aus.
  ///origin und destination können Koordinaten oder Addressen sein
  ///Leerstellen sind dabei mit "+" zu kennzeichnen
  static String _createGoogleDirectionsUrl(
      {String origin,
      String destination,
      TravelMode mode,
      List<String> waypoints = const []}) {
    origin.replaceAll(" ", "+");
    destination.replaceAll(" ", "+");
    return "https://emission.paul7.de/api/maps/query?origin=$origin&dest=$destination&mode=${_travelModeEnumMap[mode]}";
  }

  ///Ruft die Google-Directions-API auf und gibt die Antwort der API als dekodiertes JSON (Map) zurück
  ///Handelt keine errors, sondern gibt sie nur weiter als [StatusCode] in Directions.status
  ///Folgende Fehler können auftreten:
  ///Bei timeout StatusCode.timeout,
  ///bei Directions API Fehlern [StatusCode]
  ///HTTP-Code außer 200 wird als Status StatusCode.requestDenied zurückgegeben
  static Future<Directions> getDirectionoData(String origin, String destination,
      [TravelMode mode = TravelMode.driving]) async {
    final String url = _createGoogleDirectionsUrl(
        origin: origin, destination: destination, mode: mode);
    final DateTime start = DateTime.now();
    print("Sending Request to Directions API with url: " + url);
    http.Response response;
    Directions directions;
    try {
      response = await http.get(url).timeout(Duration(seconds: 5));
    } catch (e) {
      print("Timeout");
      return Directions(status: StatusCode.timeout);
    }
    print(
        "response from Directions API recieved after ${DateTime.now().difference(start)}");
    String body = response.body;
    //escape unescaped characters (screw you php)
    if (response.body.contains('\\"') && response.body.contains('\\n'))
      body = json.decode(response.body);
    print(json.decode(response.body));
    directions = Directions.fromJson(json.decode(body));
    print(response.body);
    if (response.statusCode != 200 && directions.status != StatusCode.ok)
      directions = Directions(status: StatusCode.requestDenied);
    return directions;
  }

  ///Zeigt einen Dialog
  static Future<Directions> displayDirectionGetter(
      BuildContext context, TravelMode travelMode) async {
    final GlobalKey _formKey = GlobalKey<FormState>();
    String origin, destination;
    _showEULA() async {
      const url = 'https://policies.google.com/privacy';
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }

    return await showDialog<Directions>(
        builder: (context) {
          return SimpleDialog(children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Start",
                      hintText: "z.B. Adresse, Stadt, Unternehmen",
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Bitte gebe deinen Startort an';
                      }
                    },
                    onSaved: (String value) {
                      origin = value;
                    },
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "Ziel",
                        hintText: "z.B. Adresse, Stadt, Unternehmen"),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Bitte gebe deinen Zielort an';
                      }
                    },
                    onSaved: (String value) {
                      destination = value;
                    },
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Image(image: AssetImage('assets/google.png')),
                      RaisedButton(
                        child: Text('Datenschutzinfo'),
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text('Infos zur automatischen Suche'),
                                  content: SingleChildScrollView(
                                    child: Text(
                                        'Wir benutzen Google Maps, um Routen für dich zu finden. Dazu ein paar Infos: \n - Solange du nicht auf den "Nach Route suchen"-Button drückst, werden gar keine Daten gesendet. \n - Folgende Daten werden gesendet: Start- und Zielort, sowie das Transportmittel. Da wir die Anfrage über unseren eigenen Server an Google weiterleiten, kann weder deine IP-Adresse noch sonst etwas zurückverfolgt werden.\n - Wir speichern keinerlei Daten der Anfrage auf unserem Server, sondern leiten sie nur an Google weiter. \n - Aus rechtlichen Gründen steht hier unten noch ein Link zu Googles Datenschutzbestimmungen'),
                                  ),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text("Google AGB"),
                                      onPressed: () {
                                        _showEULA();
                                      },
                                    ),
                                    FlatButton(
                                      child: Text("Zurück"),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    )
                                  ],
                                );
                              });
                        },
                      )
                    ],
                  ),
                  RaisedButton(
                    color: Colors.green,
                    child: Text("Nach Route suchen"),
                    onPressed: () {
                      FormState formState = _formKey.currentState;
                      if (formState.validate()) {
                        formState.save();

                        showDialog(
                            builder: (context) {
                              return SimpleDialog(
                                children: <Widget>[
                                  Center(child: Text("Suche Route...")),
                                  Center(child: CircularProgressIndicator())
                                ],
                              );
                            },
                            context: context);
                        getDirectionoData(origin, destination, travelMode)
                            .then((Directions directions) {
                          if (directions.status != StatusCode.ok)
                            return Future.error(directions);
                          //"Suche Route..."-Dialog beenden
                          Navigator.pop(context);
                          //Directions-Popup beenden
                          Navigator.pop(context, directions);
                        }).catchError(
                          (e) {
                            //"Suche Route..."-Dialog beenden
                            Navigator.pop(context);
                            print(
                                "Error caught by catchError in DirectionsUtils: " +
                                    e.status.toString());
                            if (e.errorMessage != null) {
                              print('Fehlermeldung:');
                              print(e.errorMessage);
                            }
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text(getErrorString(e)),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text("Alles klar"),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    )
                                  ],
                                );
                              },
                            );
                          },
                        );
                      }
                    },
                  )
                ],
              ),
            ),
          ]);
        },
        context: context);
  }

  ///Gibt einen String für verschiedene Fehlermeldungen beim Finden einer Route aus.
  static String getErrorString(Directions directions) {
    if (directions.status == StatusCode.timeout)
      return "Bist du dir sicher, dass dein Handy mit dem Internet verbunden bist? Wir sind uns nämlich ziemlich sicher, dass es nicht verbunden ist";
    else if (directions.status == StatusCode.maxRouteLengthExceeded)
      return "Übertreibs nicht! Denk mal an das arme Rechenzentrum, dass diese Weltreise berchnen muss (Klartext: Deine Route war zu lang. Teile sie in mehrere Einzelteile auf. Und schreibe uns, wie du das geschafft hast, ich habe diese Fehlermeldung noch nie gesehen)";
    else if (directions.status == StatusCode.overDailyLimit ||
        directions.status == StatusCode.overQueryLimit)
      return "Leute, ihr habt uns besiegt. Wir haben unser Google-Maps-Anfragelimit erreicht. Probiere es morgen nochmal, wir sind bestimmt schon dabei eine Lösung zu finden. Vielleicht haben wir auch einfach nur Mist gebaut, aber das würden wir niemals zugeben";
    else if (directions.status == StatusCode.zeroResults)
      return "Äh, bist du dir wirklich ganz sicher, dass es eine Route zwischen diesen Orten gibt? Wir sind uns ziemlich sicher dass es keine gibt, denn wir haben's gegoogelt";
    else if (directions.status == StatusCode.notFound)
      return "Willst du uns reinlegen? Die Orte, nach denen du uns suchen lässt, gibt es gar nicht! Vielleicht hast du dich auch nur vertippt?";
    else if (directions.status == StatusCode.requestDenied)
      return "Unsere Suche wurde abgelehnt. Das passiert zum Beispiel, wenn du Start- oder Zielort nicht angegeben hast. Kann es sein dass du was vergessen hast?";
    else
      return "Das hat irgendwie nicht geklappt. Wir haben leider keine Ahnung, woran das liegt. Wenn das häufiger passiert, kontaktiere uns bitte.";
  }
}

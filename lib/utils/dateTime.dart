///Sammlung eigener kleiner Funktionen zum Umgang mit [DateTime]s
abstract class DateTimeUtils {
  ///Erstellt eine einmalige dateID für das gegebene Datum.
  ///Alle Daten unterhalb von day stammen von DateTime.now()
  static DateTime createDateId(DateTime date) {
    DateTime now = DateTime.now();
    return DateTime(date.year, date.month, date.day, now.hour, now.minute,
        now.second, now.millisecond, now.microsecond);
  }

  ///Gibt ein [DateTime] in folgender Syntax aus
  ///
  ///[day].[month].[year]
  static String toDateString(DateTime date) {
    DateTime now = DateTime.now();
    if(date.year == 0) return "Anfang";
    else if (isSameDay(now, date))
      return "Heute";
    else if (isSameDay(now.add(Duration(days: 1)), date))
      return "Morgen";
    else if (isSameDay(now.subtract(Duration(days: 1)), date))
      return "Gestern";
    else
      return "${date.day}.${date.month}.${date.year}";
  }

  ///Gibt ein [DateTime] in folgender Syntax aus
  ///
  ///[weekday], der [day].[month].[year]
  static String toLongDateString(DateTime date) {
    List<String> weekdays = [
      null,
      "Montag",
      "Dienstag",
      "Mittwoch",
      "Donnerstag",
      "Freitag",
      "Samstag",
      "Sonntag"
    ];
    DateTime now = DateTime.now();
    if (isSameDay(now, date))
      return "Heute";
    else if (isSameDay(now.add(Duration(days: 1)), date))
      return "Morgen";
    else if (isSameDay(now.subtract(Duration(days: 1)), date))
      return "Gestern";
    else
      return "${weekdays[date.weekday]}, der ${toDateString(date)}";
  }

  ///Gibt [true] zurück, wenn beide DateTimes am gleichen Tag sind
  static bool isSameDay(DateTime date1, DateTime date2) {
    if (date1.year == date2.year &&
        date1.month == date2.month &&
        date1.day == date2.day)
      return true;
    else
      return false;
  }

  ///Erzeugt eine [Duration] aus einem gegebenen String. Format: [Duration.toString]
  static Duration parseDuration(String string) {
    List<String> splitted = string.split(RegExp("[:.]"));
    return Duration(
      hours: int.parse(splitted[0]),
      minutes: int.parse(splitted[1]),
      seconds: int.parse(splitted[2]),
      microseconds: int.parse(splitted[3]),
    );
  }

///Wandelt ein Dart DateTime-Objekt in einen String im Format: YYYY-MM-DD um
  static String toMySqlDate(DateTime date) {
    return date.year.toString().padLeft(4, '0') +
        "-" +
        date.month.toString().padLeft(2, '0') +
        "-" +
        date.day.toString().padLeft(2, '0');
  }
}

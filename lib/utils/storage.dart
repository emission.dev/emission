import 'dart:convert';
import 'dart:io';
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:path_provider/path_provider.dart';

///Sammlung von Methoden zum Schreiben bzw Lesen von Dateien, en- und decodieren von Daten
abstract class StorageUtils {
  ///Wandelt json-Data in enums um. Notwendige Argumente:
  /// Eine Map im Format {enumWert: Wert} und ein Wert(in der Map vorhanden)
  static T enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
    //null check might not be wanted all the time
    if (source == null) {
      return null;
    }
    return enumValues.entries
        .singleWhere((e) => e.value == source,
            orElse: () => throw ArgumentError(
                '`$source` is not one of the supported values: '
                '${enumValues.values.join(', ')}'))
        .key;
  }

  ///Lädt die Daten aus der angegebenen Datei im Documents-Verzeichnis und gibt sie als String zurück
  static Future<String> loadFromFile(String fileName) async {
    final directory = await getApplicationDocumentsDirectory();
    File database = File("${directory.path}/$fileName");
    if(!await database.exists()) return "";
    return await database.readAsString();
  }

  ///Schreibt [content] in die Datei [fileName] im Documents-Verzeichnis. Vorhandene Daten werden überschrieben.
  static writeToFile(String content, String fileName) async {
    final directory = await getApplicationDocumentsDirectory();
    File database = File('${directory.path}/$fileName');
    //Datei erstellen falls nicht vorhanden
    if (!await database.exists()) await database.create();
    //Zur Datei schreiben
    await database.writeAsString(content);
  }

  ///setzt alle Nutzerdaten zurück
  ///Wenn 
  static resetData() {
    writeToFile("","staticEntries.json");
    writeToFile("","entries.json");
    writeToFile("","shortcuts.json");
    writeToFile("","settings.json");
  }

  static Future<File> createBackup(DataModel model) async {
    model.saveData();
    File file = File("eMission.txt");
    Map<String, dynamic> writeMap = {
      "settings": loadFromFile("settings.json"),
      "entries": loadFromFile("entries.json")
    };
    file.writeAsStringSync(writeMap.toString());
    return file;
  }

  static restoreBackup(File file, DataModel model) async {
    try {
      Map<String, dynamic> backup = json.decode(await file.readAsString());
      writeToFile(backup["settings"], "settings.json");
      writeToFile(backup["entries"], "entries.json");
      model.loadData();
    } catch (e) {
      print("Fehler in restoreBackup" + e);
    }
  }
}

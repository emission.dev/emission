import 'package:eMission/models/entry.dart';
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/utils/units.dart';
import 'package:flutter/material.dart';
import 'dart:math';

///Enthält Tools, die Flutter UI Packages verwenden
class UiUtils {
  ///Fügt einen neuen Eintrag hinzu und gibt eine Snackbar zurück, die dem Benutzer zur Information angezeigt werden kann
  static SnackBar showAddEntrySnackbar(Entry entry) {
    return SnackBar(
      content: Text(
          "Eintrag mit ${UnitUtils.printEmissions(entry.totalEntryEmission)} hinzugefügt"),
    );
  }

  static SnackBar showAddShortcutSnackbar(Entry entry) {
    return SnackBar(
      content: Text(
          "Shortcut mit ${UnitUtils.printEmissions(entry.totalEntryEmission)} hinzugefügt"),
    );
  }

  ///Löscht einen Eintrag und gibt eine Snackbar zurück,
  ///die es dem User ermöglicht, den Eintrag nach versehentlichem Löschen wieder hinzuzufügen.
  static SnackBar deleteEntryWithSnackbar(DateTime dateId, DataModel model) {
    Entry deleted = model.deleteEntry(dateId, model.toBeUploaded);
    return SnackBar(
      content: Text("Eintrag gelöscht"),
      action: SnackBarAction(
        label: "AAARRGGH! NEEEIN!",
        onPressed: () {
          model.addEntry(deleted, true, model.toBeUploaded);
        },
      ),
    );
  }

  ///Je größer das Verhältnis, desto mehr geht die Farbe ins Rote (schlecht), je niedriger, desto mehr ins helle.
  ///Nützlich, um einem Nutzer visuaell anzuzeigen, wie gut sein Verbrauch im Vergleich zum Durchschnitt ist
  static generateColorFromRatio(double ratio) {
    return HSLColor.fromAHSL(1, 120 * pow(0.5, ratio), 1, 0.5).toColor();
  }
}

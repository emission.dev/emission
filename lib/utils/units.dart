///Sammlung von Funktionen zur Umrechnung und Anzeige von Daten in einer Einheit
abstract class UnitUtils {
  ///Wandelt die als double gespeicherte Länge in einen lesbaren String mit Einheit (je nach Größe) um
  ///Einheit: 1 = 1km
  static String printDistance(num distance) {
    if (distance < 1)
      return (distance * 1000).round().toString() + "m";
    else if (distance < 1000) return distance.toStringAsPrecision(3).replaceAll(".", ",") + "km";
    else return distance.round().toString() + "km";
  }

  ///Wandelt die als double gespeicherten Emissionen in einen lesbaren String mit Einheit (je nach Größe) um
  ///Einheit: 1 = 1 kg CO²}-
  static String printEmissions(num emissions) {
    if (emissions == null)
      return "0g CO\u{2082}";
    else if (emissions < 1)
      return (emissions * 1000).round().toString() + "g CO\u{2082}";
    else if (emissions.round() >= 1000)
      return (emissions / 1000).toStringAsPrecision(3).replaceAll(".", ",") + "t CO\u{2082}";
    else
      return emissions.round().toString() + "kg CO\u{2082}";
  }
}

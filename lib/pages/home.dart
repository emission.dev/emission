import 'package:eMission/models/settings.dart';
import 'package:eMission/pages/intro.dart';
import 'package:eMission/widgets/home/history.dart';
import 'package:eMission/widgets/home/more.dart';
import 'package:eMission/widgets/home/staticEntries.dart';
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/widgets/statistics/statistics.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  final DataModel _model;
  HomePage(this._model);
  @override
  HomePageState createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  DateFormat dateFormat;
  DateFormat timeFormat;
  bool showIntro = false;

  @override
  void initState() {
    if (!widget._model.hasLoaded) widget._model.loadData();
    super.initState();
    _checkDisplaySize();
    _checkShowIntro();
  }

  @override
  Widget build(BuildContext context) {
    return showIntro
        ? IntroPage()
        : DefaultTabController(
            length: 4,
            child: Scaffold(
              bottomNavigationBar: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  TabBar(
                    tabs: <Widget>[
                      Tab(
                        text: "Start",
                      ),
                      Tab(
                        text: "Reisen",
                      ),
                      Tab(
                        text: "Alltag",
                      ),
                      Tab(
                        text: "Mehr",
                      ),
                    ],
                    indicator: Theme.of(context).tabBarTheme.indicator,
                  ),

                  ///Verhindert, dass die TabBar unter OS-Elementen verschwindet (glaube ich)
                  Container(
                    color: Theme.of(context).tabBarTheme.unselectedLabelColor,
                    height: MediaQuery.of(context).padding.bottom,
                  ),
                ],
              ),
              key: scaffoldKey,
              body: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/bg.jpg'),
                  ),
                ),
                child: SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: TabBarView(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            StatisticsPage(),
                          ],
                        ),
                        HistoryPage(),
                        StaticEntriesPage(),
                        MoreTab(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.inactive) {
      widget._model.saveData().then((bool value) {
        print("Speichern und Hochladen abgeschlossen");
      });
    }
  }

  void _checkShowIntro() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      ///Warte, bis die Settings geladen wurden
      whenNotNull(
        Stream<Settings>.periodic(
            Duration(milliseconds: 50), (x) => widget._model.settings),
      ).then((Settings value) {
        //Passe die Einstellung an
        setState(() {
          showIntro = value.showIntro;
        });
      });
    });
  }

  ///Zeigt eine Warnmeldung an, falls das Display wahrscheinlich zu klein ist, um eMission richtig darzustelllen
  void _checkDisplaySize() {
    //Überprüfe Displaygröße

    WidgetsBinding.instance.addPostFrameCallback((_) {
      ///Warte, bis die Daten für Höhe und Breite vorhanden sind
      whenNotZero(
        Stream<double>.periodic(Duration(milliseconds: 50),
            (x) => MediaQuery.of(context).size.width),
      ).then((double value) {
        //Zeige Warnung an, falls das Display des Users zu klein ist
        if (MediaQuery.of(context).size.width < 320 ||
            MediaQuery.of(context).size.height < 568)
          showDialog(
              context: context,
              builder: (context) {
                return SimpleDialog(
                  children: <Widget>[
                    Text(
                        //TODO: insert actual email address
                        "Dein Gerät ist kleiner als unsere Mindestgröße (iPhone 5/5s/SE). Dadurch wird es zu Anzeigefehlern kommen. Bitte teile uns mit, welches Gerät du besitzt, dann können wir darüber nachdenken, es auch zu unterstützen (emission@paul7.de). Falls du dein Handy einfach nur im Querformat benutzt, kann das auch passieren."),
                    Text("Deine Displaygröße:"),
                    Text("Höhe: " +
                        MediaQuery.of(context).size.height.toString()),
                    Text("Breite: " +
                        MediaQuery.of(context).size.width.toString())
                  ],
                );
              });
      });
    });
  }

  ///Stream, um herauszufinden, ob ein Wert 0 ist. Notwendig, da MediaQuery.size erst nach einiger Zeit korrekte Werte angibt
  Future<double> whenNotZero(Stream<double> source) async {
    await for (double value in source) {
      if (value > 0) {
        return value;
      }
    }
  }

  ///Stream, um herauszufinden, ob ein Wert null ist. Notwendig, da MediaQuery.size erst nach einiger Zeit korrekte Werte angibt
  Future<T> whenNotNull<T>(Stream<T> source) async {
    await for (T value in source) {
      if (value != null) {
        return value;
      }
    }
  }
}

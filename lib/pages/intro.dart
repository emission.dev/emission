import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/pages/home.dart';
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:scoped_model/scoped_model.dart';

class IntroPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextStyle descStyle =
        Theme.of(context).textTheme.body2.copyWith(color: Colors.white);
    TextStyle titleStyle =
        Theme.of(context).textTheme.headline.copyWith(color: Colors.white);
    return IntroSlider(
      slides: [
        Slide(
          title: "Wichtig!",
          description:
              "Moin erstmal! Danke dass du eMission ausprobieren willst. eMission ist zwar einfach zu benutzen, aber aktuell noch nicht komplett selbsterklärend. Schau immer genau hin (Texte sind zum Lesen da, danke dass du hier nicht einfach auf 'Weiter' geklickt hast).\n\n Wenn du mal nicht weiter weißt, benutze die Info-Buttons, dann sollte alles verständlich sein. Wenn nicht, ist das unser Fehler, nicht deiner, also schreib uns einfach an :)",
          styleDescription: descStyle,
          styleTitle: titleStyle,
          backgroundColor: Colors.green,
        ),
        Slide(
          title: "Aufbau",
          description:
              "Die App besteht aus vier Tabs: \n\n\n - Start: Hier siehst du deine Statistiken (und die deiner Freunde) \n\n - Reisen: Hier kannst du herausfinden, wieviel Emissionen du durch Transport verursachst. Einfach den Tag der Reise antippen, dann das Transportmittel und dann ein paar Details - keine Angst, geht schnell.  \n\n Alltag: Hier sind die Emissionen, die du jeden Tag durch Strom, Heizungs, Essen und Einkaufen verbrauchst. \n\n Mehr: Hier gibt's Tipps, Infos, und naja, halt einfach mehr. Aktuell kannst du hier hauptsächlich einen Account erstellen, um Daten deiner Freunde zu sehen.",
          styleDescription: descStyle,
          styleTitle: titleStyle,
          backgroundColor: Colors.green,
        ),
        //TODO: think of minimal tutorial
      ],
      onDonePress: () {
        DataModel model = ScopedModel.of<DataModel>(context);
        model.settings.showIntro = false;
        model.saveSettings();
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) {
          return HomePage(model);
        }));
      },
    );
  }
}

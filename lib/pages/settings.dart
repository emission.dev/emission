/* import 'dart:io';

import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/utils/storage.dart';
import 'package:eMission/widgets/defaultUiElements.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:share/share.dart';

class SettingsPage extends StatefulWidget {
  @override
  SettingsPageState createState() {
    return new SettingsPageState();
  }
}

class SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage('assets/bg.jpg'))),
      child: SafeArea(
        child: Hero(
          child: DefaultCard(
            child: ScopedModelDescendant<DataModel>(
              builder: (BuildContext context, Widget child, DataModel model) {
                return ListView(
                    children: ListTile.divideTiles(context: context, tiles: [
                  SwitchListTile(
                    title:
                        Text("Backups aller Daten auf Google Drive speichern"),
                    value: model.settings.enableDriveBackup,
                    onChanged: (bool value) {
                      setState(() {
                        model.settings.enableDriveBackup = value;
                        model.saveSettings();
                      });
                    },
                  ),
                  Row(
                    children: <Widget>[
                      Text("Automatisch sichern alle: "),
                      Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width - 240,
                        child: DropdownButton<Duration>(
                          isExpanded: true,
                          onChanged: model.settings.enableDriveBackup
                              ? (Duration value) {
                                  setState(() {
                                    model.settings.driveBackupFrequency = value;
                                    model.saveSettings();
                                  });
                                }
                              : null,
                          value: model.settings.driveBackupFrequency,
                          items: [
                            DropdownMenuItem(
                              child: Text("1 Woche"),
                              value: Duration(days: 7),
                            ),
                            DropdownMenuItem(
                              child: Text("1 Tag"),
                              value: Duration(days: 1),
                            ),
                            DropdownMenuItem(
                              child: Text("nur manuell"),
                              value: Duration(),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  RaisedButton(
                    child: Text("Backup laden"),
                    onPressed: model.settings.enableDriveBackup
                        ? () {
                            FilePicker.getFilePath(
                              type: FileType.CUSTOM,
                              fileExtension: 'txt',
                            ).then((String path) {
                              setState(() {
                                model.settings.backupFileUrl = path;
                              });
                            });
                          }
                        : null,
                  ),
                  RaisedButton(
                    child: Text("Jetzt sichern"),
                    onPressed: model.settings.enableDriveBackup
                        ? () =>
                            StorageUtils.createBackup(model).then((File file) {
                              Share.share(file.readAsStringSync());
                            })
                        : null,
                  ),
                ]).toList());
              },
            ),
          ),
          tag: "settings",
        ),
      ),
    );
  }
}
 */
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/widgets/defaultUiElements.dart';
import 'package:eMission/widgets/friends/account.dart';
import 'package:eMission/widgets/friends/friendsList.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class FriendsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover, image: AssetImage('assets/bg.jpg'))),
        child: SafeArea(
          child: Hero(
            tag: "account",
            child: DefaultCard(
              child: ScopedModelDescendant<DataModel>(
                builder: (BuildContext context, Widget child, DataModel model) {
                  return Column(
                    children: <Widget>[
                      Text(
                        "Dein Account:",
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      AccountsWidget(model),
                      Text(
                        "Deine Freunde:",
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      model.loginData == null
                          ? Expanded(
                              child: Center(
                                child: Text(
                                    "Melde dich an, um Freunde hinzuzufügen"),
                              ),
                            )
                          : Expanded(child: FriendsListWidget(model)),
                    ],
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

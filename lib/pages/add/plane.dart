import 'package:eMission/pages/addEntry.dart';
import 'package:eMission/pages/add/airportSelect.dart';
import 'package:flutter/material.dart';
import 'package:eMission/models/vehicles.dart';
import 'package:eMission/utils/units.dart';
import 'package:eMission/utils/location.dart';
import 'package:eMission/utils/dateTime.dart';
import 'package:eMission/models/entry.dart';

///Erzeugt BottomSheet zum Hinzufügen neuer Einträge
class PlaneAddEntryPage extends AddEntryPage {
  ///Funktion zum Bauen dse fahrzeugunabhängigen Teils

  PlaneAddEntryPage(bool inShortcutMode, DateTime date)
      : super(inShortcutMode, date);
  @override
  AddEntryPageState createState() {
    return _PlaneAddEntryPageState();
  }
}

class _PlaneAddEntryPageState extends AddEntryPageState {
  Map<PassengerClass, Entry> _entriesAllClasses = {};
  PassengerClass _selectedClass = PassengerClass.economy;

  List<Airport> airports;

  @override
  void initState() {
    //Fügt zwei leere Airport-Slots hinzu. Dadurch werden zwei Buttons generiert, mithilfe derer
    //Flughäfen an diese Stelle gesetzt werden können
    airports = [null, null];
    super.initState();
  }

  void _createEntries() {
    //Wenn ein Eintrag null ist, abbrechen. Das passiert, wenn ein Flughafen hinzugefügt, aber nicht ausgewählt wurde.
    //Das ist unschön
    if (airports.contains(null)) return;
    PassengerClass.values.forEach((PassengerClass pc) {
      List<TransportEntry> entries = [];
      for (int i = 1; i < airports.length; i++) {
        if (airports[i] != null)
          entries.add(
            TransportEntry(
                dateId: DateTimeUtils.createDateId(widget.date),
                vehicle: Plane(
                  passengerClass: pc,
                ),
                distance: LocationUtils.distanceBetweenCoordinates(
                    airports[i - 1].lat,
                    airports[i - 1].long,
                    airports[i].lat,
                    airports[i].long),
                start: airports[i - 1].city,
                end: airports[i].city),
          );
      }
      setState(() {
        if (entries.length > 1)
          _entriesAllClasses[pc] = CompoundEntry(entries);
        else if (entries.length == 1)
          _entriesAllClasses[pc] = entries[0];
        else
          _entriesAllClasses[pc] = null;
        entry = _entriesAllClasses[_selectedClass];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return buildAddEntryFrame(
      child: Container(
        margin: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Text("Wähle hier deine Flugklasse aus"),
            _buildClassPicker(),
            Container(
              height: MediaQuery.of(context).size.height -
                  184 -
                  MediaQuery.of(context).viewInsets.bottom,
              child: ListView.builder(
                itemCount: airports.length + 1,
                itemBuilder: ((context, index) {
                  if (index == airports.length - 1)
                    //Button zum Hinzufügen einer Zwischenlandung
                    return FlatButton(
                      onPressed: () {
                        setState(() {
                          airports.insert(index, null);
                          Navigator.push<Airport>(context,
                              MaterialPageRoute(builder: (context) {
                            return AirportSelectorPage(index);
                          })).then((Airport airport) {
                            print("Airport recieved");
                            print(airport.name);
                            setState(() {
                              airports[index] = airport;
                              _createEntries();
                            });
                          });
                        });
                      },
                      child: Text(
                        "+ ZWISCHENLANDUNG HINZUFÜGEN",
                        style: TextStyle(color: Colors.blue),
                      ),
                    );
                  if (index == airports.length) index = airports.length - 1;
                  return airports[index] == null
                      ?
                      //Falls kein Flughafen ausgewählt wurde, Auswahl anzeigen
                      _buildAirportSelector(index, context)
                      : _buildAirportInfo(index);
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ListTile _buildAirportInfo(int index) {
    return ListTile(
      leading: Icon(Icons.airplanemode_active),
      title: Text(airports[index].name),
      subtitle: Text(airports[index].city +
          " - " +
          (index == 0
              ? "Startflughafen"
              : index == airports.length - 1
                  ? "Zielflughafen"
                  : "Zwischenlandung " + (index).toString())),
      trailing: IconButton(
        icon: Icon(Icons.delete),
        onPressed: () {
          setState(() {
            if (index != 0 && index != airports.length - 1)
              airports.removeAt(index);
            else
              airports[index] = null;

            _createEntries();
          });
        },
      ),
    );
  }

  GestureDetector _buildAirportSelector(int index, BuildContext context) {
    return GestureDetector(
      child: Hero(
        tag: index,
        child: ListTile(
          leading: Icon(Icons.airplanemode_inactive),
          title: Text(index == 0
              ? "Startflughafen auswählen"
              : index == airports.length - 1
                  ? "Zielflughafen auswählen"
                  : "Zwischenlandung " + index.toString() + " auswählen"),
          trailing: index != 0 && index != airports.length - 1
              ? IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () {
                    setState(() {
                      airports.removeAt(index);
                    });
                  },
                )
              : SizedBox(),
        ),
      ),
      onTap: () {
        Navigator.push<Airport>(context, MaterialPageRoute(builder: (context) {
          return AirportSelectorPage(index);
        })).then((Airport airport) {
          if (airport != null) {
            print("Airport recieved");
            print(airport.name);
            setState(() {
              airports[index] = airport;
              _createEntries();
            });
          }
        });
      },
    );
  }

  //Erstellt das DropdownMenu, in dem die Passagierklasse ausgewählt werden kann
  DropdownButton<PassengerClass> _buildClassPicker() {
    //Ein einzelner Eintrag des PopupMenus, mit dem die Flugklasse ausgewählt wird.
    DropdownMenuItem<PassengerClass> _buildDropdownMenuItem(
        String title, PassengerClass passengerClass) {
      return DropdownMenuItem(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              title,
              overflow: TextOverflow.fade,
            ),
            //Wenn ein Eintrag definiert wurde, hier die Emissionen anzeigen
            if (_entriesAllClasses[passengerClass] != null)
              Text(
                UnitUtils.printEmissions(
                    _entriesAllClasses[passengerClass].totalEntryEmission),
                overflow: TextOverflow.clip,
              )
          ],
        ),
        value: passengerClass,
      );
    }

    return DropdownButton<PassengerClass>(
      isExpanded: true,
      value: _selectedClass,
      items: [
        _buildDropdownMenuItem("Economy", PassengerClass.economy),
        _buildDropdownMenuItem(
            "Premium-Economy", PassengerClass.premiumEconomy),
        _buildDropdownMenuItem("Business", PassengerClass.business),
        _buildDropdownMenuItem("First Class", PassengerClass.first),
      ],
      onChanged: (PassengerClass value) {
        setState(() {
          _selectedClass = value;
        });
        _createEntries();
      },
    );
  }
}

import 'package:eMission/models/directions.dart';
import 'package:eMission/models/entry.dart';
import 'package:eMission/models/vehicles.dart';
import 'package:eMission/pages/addEntry.dart';
import 'package:eMission/utils/dateTime.dart';
import 'package:eMission/utils/directinos.dart';
import 'package:eMission/utils/units.dart';
import 'package:eMission/widgets/vehicleIcon.dart';
import 'package:flutter/material.dart';

class PublicAddEntryPage extends AddEntryPage {
  PublicAddEntryPage(bool inShortcutMode, DateTime date)
      : super(inShortcutMode, date);
  @override
  AddEntryPageState createState() => _PublicAddEntryPageState();
}

class _PublicAddEntryPageState extends AddEntryPageState {
  List<TransportEntry> _entryData = [];
  List<DirectionStep> _steps = [];
  List<TextEditingController> _textEditingControllers = [];

  _updateDisplayedWidget() {
    List<TransportEntry> nonNullEntryList = _entryData.toList();
    nonNullEntryList.removeWhere((TransportEntry entry) {
      return entry.distance == 0;
    });
    if (nonNullEntryList.length > 1)
      setState(() {
        entry = CompoundEntry(nonNullEntryList);
      });
    else if (nonNullEntryList.length == 1)
      setState(() {
        entry = nonNullEntryList[0];
      });
    else
      setState(() {
        entry = null;
      });
    if (entry != null && description.isNotEmpty)
      entry.description = description;
  }

  Widget _buildStep(int index) {
    if (_steps.length > index)
      return _steps[index] == null
          ? _buildManualStep(index)
          : _buildDirectionsStep(index);
    return _buildManualStep(index);
  }

  ///Wird angezeigt, falls Ergebnisse von Google Maps verwendet werden. Zeigt mehr Details an, die aus der Antwort
  ///von Google erstellt werden. Wird dieser Schritt entfernt, wird er durch einen manuellen mit gleichen Daten
  ///ersetzt, um eventuell nur Details zu ändern
  Widget _buildDirectionsStep(int index) {
    return Card(
      child: ListTile(
        leading: VehicleIcon.fromDirectionsApiData(
            _steps[index].transitDetails.line.vehicle),
        title: Text(
          "Von " +
              _steps[index].transitDetails.arrivalStop.name +
              " nach " +
              _steps[index].transitDetails.arrivalStop.name,
          overflow: TextOverflow.fade,
        ),
        subtitle: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text("mit " + _steps[index].transitDetails.line.shortName),
            Text(
                UnitUtils.printEmissions(_entryData[index].totalEntryEmission)),
          ],
        ),
        trailing: IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            setState(() {
              _steps[index] = null;
            });
          },
        ),
      ),
    );
  }

  Column _buildManualStep(int index) {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("${index + 1}. Schritt"),
              //Distanz-Eingabefeld
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: TextField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    suffixText: "km",
                    labelText: "Distanz",
                  ),
                  controller: _textEditingControllers[index],
                  onChanged: (String value) {
                    setState(() {
                      _entryData[index].distance = double.parse(value);
                      _entryData[index] = TransportEntry(
                          distance: _entryData[index].distance ?? 0,
                          vehicle: PublicTransport(
                              (_entryData[index].vehicle as PublicTransport)
                                  .publicVehicleType),
                          dateId: DateTimeUtils.createDateId(widget.date));
                      _updateDisplayedWidget();
                    });
                  },
                ),
              ),
              SizedBox(
                width: 10,
              ),
              IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  setState(() {
                    print("removed everything at step:" + index.toString());
                    _entryData.removeAt(index);
                    _textEditingControllers.removeAt(index);
                    _updateDisplayedWidget();
                  });
                },
              ),
            ],
          ),
        ),
        Container(
          height: 50,
          child: Form(
            child: Row(
              children: <Widget>[
                //Dropdown
                Container(
                  child: DropdownButton<PublicVehicleType>(
                    isExpanded: true,
                    isDense: false,
                    items: [
                      for (PublicVehicleType type in PublicVehicleType.values)
                        _buildDropdownMenuItem(
                            publicVehicleTypeEnumMap[type], type, index)
                    ],
                    onChanged: (PublicVehicleType type) {
                      setState(() {
                        (_entryData[index].vehicle as PublicTransport)
                            .publicVehicleType = type;
                        _entryData[index] = TransportEntry(
                            distance: _entryData[index].distance ?? 0,
                            vehicle: PublicTransport(type),
                            dateId: DateTimeUtils.createDateId(widget.date));
                        _updateDisplayedWidget();
                      });
                    },
                    value: (_entryData[index].vehicle as PublicTransport)
                            .publicVehicleType ??
                        PublicVehicleType.bus,
                  ),
                  width: MediaQuery.of(context).size.width * 0.9,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  DropdownMenuItem<PublicVehicleType> _buildDropdownMenuItem(
      String title, PublicVehicleType publicVehicleType, int index) {
    return DropdownMenuItem(
      child: ListTile(
        leading: VehicleIcon(publicVehicleType),
        title: Text(
          title,
          textAlign: TextAlign.start,
          overflow: TextOverflow.fade,
        ),
        trailing: Text(
          _entryData[index].distance == null
              ? ""
              : UnitUtils.printEmissions(TransportEntry(
                  dateId: DateTime.now(),
                  distance: _entryData[index].distance,
                  vehicle: PublicTransport(publicVehicleType),
                ).totalEntryEmission),
        ),
      ),
      value: publicVehicleType,
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return buildAddEntryFrame(
      child: Column(
        //TODO: update with dart 2.3, define constraints properly
        children: <Widget>[
          if (_entryData.length == 0) ...[
            RaisedButton(
              child: Text("Reise manuell ausfüllen"),
              onPressed: () {
                _addStep();
              },
            ),
            RaisedButton(
              child: Text('Reise automatisch finden'),
              onPressed: () {
                _getRoute();
              },
            )
          ] else
            if (_entryData.length != 0)
              Container(
                height: MediaQuery.of(context).size.height * 0.9 -
                    200 -
                    MediaQuery.of(context).viewInsets.horizontal,
                child: ListView.builder(
                  itemCount: _entryData.length + 2,
                  itemBuilder: (context, index) {
                    if (index == _entryData.length)
                      return FlatButton.icon(
                        icon: Icon(Icons.add),
                        label: Text("UMSTEIGEN"),
                        onPressed: () {
                          _addStep();
                        },
                      );
                    else if (index == _entryData.length + 1)
                      return Text(
                          "${_entryData.length} Abschnitte erzeugen insgesamt ${entry == null ? UnitUtils.printEmissions(0) : UnitUtils.printEmissions(entry.totalEntryEmission)}");
                    else
                      return _buildStep(index);
                  },
                ),
              ),
        ],
      ),
    );
  }

  ///Fügt einen Navigationsschrit hinzu. Wird entweder durch den Umsteigen-Button oder das Auswählen des manuellen Modus ausgeführt
  void _addStep() {
    setState(() {
      _entryData.add(TransportEntry(
          distance: 0,
          vehicle: PublicTransport(PublicVehicleType.bus),
          dateId: DateTimeUtils.createDateId(widget.date)));
      _textEditingControllers.add(TextEditingController(text: "0"));
    });
  }

  void _getRoute() {
    //Routendaten empfangen
    DirectionsUtils.displayDirectionGetter(context, TravelMode.transit)
        .then((Directions directions) {
      if (directions.status != StatusCode.ok) return Future.error(directions);

      //Fußgängerschritte aus allen Routen entfernen
      directions.routes.forEach((DirectionRoute route) {
        route.legs[0].steps.retainWhere((DirectionStep step) {
          return step.travelMode == "TRANSIT";
        });
      });

      //Wenn nur ein Ergebnis, dieses direkt benutzen
      if (directions.routes.length == 1) {
        _setValues(directions.routes[0]);
        Navigator.pop(context);
      } else {
        //Ansonsten Popup zur Auswahl anzeigen
        print(directions.routes.length.toString() +
            " Routen gefunden. Zeige Auswahlmenü...");
        showDialog(
                builder: (BuildContext context) {
                  return _buildRouteSelectorDialog(directions, context);
                },
                context: context)
            .then((value) {
          if (value != null) _setValues(value);
          Navigator.pop(context);
        });
      }
    });
  }

  SimpleDialog _buildRouteSelectorDialog(
      Directions directions, BuildContext context) {
    return SimpleDialog(title: Text("Wähle eine der Routen aus"), children: [
      for (DirectionRoute route in directions.routes)
        Card(
          child: InkWell(
            onTap: () {
              Navigator.pop(context, route);
            },
            child: Column(children: [
              for (DirectionStep step in route.legs[0].steps)
                ListTile(
                  leading: VehicleIcon.fromDirectionsApiData(
                      step.transitDetails.line.vehicle),
                  title: Text(
                      "Mit ${step.transitDetails.line.vehicle.name} von ${step.transitDetails.departureStop.name} nach ${step.transitDetails.arrivalStop.name}"),
                )
            ]),
          ),
        )
    ]);
  }

  ///Aus der gerade festgelegten Daten die Einträge und UI-Daten generieren ([_distances],[publicVehicleTypes])
  void _setValues(DirectionRoute route) {
    setState(() {
      _steps = route.legs[0].steps;

      _entryData = _steps.map((DirectionStep step) {
        return TransportEntry(
            distance: step.distance.value.toDouble() / 1000.0,
            vehicle: PublicTransport(step.transitDetails.line.vehicle.type),
            dateId: DateTimeUtils.createDateId(widget.date),
            start: step.transitDetails.departureStop.name,
            end: step.transitDetails.arrivalStop.name);
      }).toList();

      _textEditingControllers = _entryData.map((TransportEntry te) {
        return TextEditingController(text: te.distance.toString());
      }).toList();
    });
    _updateDisplayedWidget();
  }
}

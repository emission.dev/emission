import 'package:eMission/utils/location.dart';
import 'package:eMission/widgets/defaultUiElements.dart';
import 'package:flutter/material.dart';

class AirportSelectorPage extends StatefulWidget {
  final int index;
  AirportSelectorPage(this.index);

  @override
  _AirportSelectorPageState createState() => _AirportSelectorPageState();
}

List<Airport> _suggestionBuilder(BuildContext context, String query) {
  List<Airport> airport = LocationUtils.getAirports(query);
  List<Airport> returnList = [];
  airport.forEach((Airport airport) {
    returnList.add(
      airport,
    );
  });
  return returnList;
}

class _AirportSelectorPageState extends State<AirportSelectorPage> {
  String query = "";

  @override
  Widget build(BuildContext context) {
    List<Airport> suggestions = _suggestionBuilder(context, query);
    return Scaffold(
      body: Container(
        color: Colors.green,
        child: SafeArea(
          child: DefaultCard(
            child: Hero(
              tag: widget.index,
              child: Material(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white.withAlpha(215),
                child: Column(children: <Widget>[
                  _buildTextField(),
                  Expanded(
                    child: query == ""
                        ? Center(
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.5,
                              child: const Text(
                                "Gib oben den Namen des Flughafens oder der Stadt ein, und wähle dann hier den richtigen Flughafen aus",
                                overflow: TextOverflow.clip,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )
                        : ListView.builder(
                            itemCount: suggestions.length,
                            itemBuilder: (context, index) {
                              Airport data = suggestions[index];
                              return ListTile(
                                leading: CircleAvatar(
                                  backgroundColor: Colors.green,
                                  child: data.lat == null
                                      ? Icon(Icons.info)
                                      : Icon(Icons.airplanemode_active),
                                ),
                                title: Text(data.iataCode == null
                                    ? data.name
                                    : "${data.name} (${data.iataCode})"),
                                subtitle: Text(data.city ?? ""),
                                onTap: () {
                                  if (data.lat != null)
                                    Navigator.pop(context, data);
                                },
                              );
                            },
                          ),
                  )
                ]),
              ),
            ),
          ),
        ),
      ),
    );
  }

  LayoutBuilder _buildTextField() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Material(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
          child: Container(
            width: constraints.maxWidth,
            decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius:
                    BorderRadius.vertical(top: Radius.circular(20.0))),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Flughafen finden",
                      style: TextStyle(fontSize: 30, color: Colors.white),
                      textAlign: TextAlign.start,
                    ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                        hintText: "Gib hier den Namen des Flughafens ein",
                        border: UnderlineInputBorder()),
                    autofocus: true,
                    onChanged: (String value) {
                      setState(() {
                        query = value;
                      });
                    },
                  )
                ]),
          ),
        );
      },
    );
  }
}

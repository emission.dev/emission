import 'package:eMission/models/directions.dart';
import 'package:eMission/pages/addEntry.dart';
import 'package:eMission/utils/dateTime.dart';
import 'package:eMission/utils/units.dart';
import 'package:flutter/material.dart';

import 'package:eMission/models/entry.dart';
import 'package:eMission/models/vehicles.dart';
import 'package:eMission/utils/directinos.dart';

//TODO: Fix FocusNode
class MotorizedAddEntryPage extends AddEntryPage {
  MotorizedAddEntryPage(bool inShortcutMode, DateTime date)
      : super(inShortcutMode, date);

  @override
  AddEntryPageState createState() => _MotorizedAddEntryPageState();
}

class _MotorizedAddEntryPageState extends AddEntryPageState {
  DirectionRoute _route;
  double _distance, _consumption;
  int _passengers = 1;
  Fuel _fuel = Fuel.diesel;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return buildAddEntryFrame(
      child: ListView(
        children: <Widget>[
          _route == null
              ? TextField(
                  keyboardType: TextInputType.number,
                  decoration:
                      InputDecoration(suffixText: "km", labelText: "Distanz"),
                  textInputAction: TextInputAction.next,
                  onChanged: (String value) {
                    setState(() {
                      _distance = double.tryParse(value);
                      _createEntry();
                    });
                  })
              : Card(
                  child: ListTile(
                      subtitle: Text(_route.legs[0].distance.text),
                      title: Text(
                          "Von ${_route.legs[0].startAddress} nach ${_route.legs[0].endAddress}"),
                      trailing: IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                          setState(() {
                            _route = null;
                            _distance = null;
                          });
                        },
                      )),
                ),
          RaisedButton(
            onPressed: () {
              _getRoute();
            },
            child: Text("Distanz herausfinden"),
          ),
          TextField(
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                  suffixText: fuelUnitMap[_fuel],
                  labelText: "Treibstoffverbrauch pro 100 Kilometer"),
              onChanged: (String value) {
                setState(() {
                  _consumption = double.tryParse(value);
                  _createEntry();
                });
              }),
          Text("Treibstoffart festlegen"),
          _buildFuelRadio("Diesel", Fuel.diesel),
          _buildFuelRadio("Benzin", Fuel.gasoline),
          _buildFuelRadio("Strom (dt. Strommix)", Fuel.electric),
          _buildFuelRadio("Strom (100% Ökostrom)", Fuel.ecoEletric),
          RaisedButton(
            onPressed: () {},
            child: Text("Emissionen herausfinden"),
          ),
          TextField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: "Anzahl Passagiere"),
              textInputAction: TextInputAction.done,
              onChanged: (String value) {
                setState(() {
                  _passengers = int.parse(value);
                  _createEntry();
                });
              }),
          Text(entry == null
              ? ""
              : UnitUtils.printEmissions(entry.totalEntryEmission))
        ],
      ),
    );
  }

  _createEntry() {
    if (_distance != null && _consumption != null) {
      entry = TransportEntry(
          dateId: DateTimeUtils.createDateId(widget.date),
          distance: _distance,
          description: description,
          vehicle: MotorizedVehicle(
              fuel: _fuel,
              fuelConsumption: _consumption,
              passengers: _passengers));
    } else
      entry = null;
  }

  RadioListTile<Fuel> _buildFuelRadio(String title, Fuel fuel) {
    return RadioListTile<Fuel>(
      groupValue: _fuel,
      value: fuel,
      title: Text(title),
      onChanged: (Fuel value) {
        setState(() {
          _fuel = value;
          _createEntry();
        });
      },
    );
  }

  ///Empfängt die Daten aus der DirectionsUtils Klasse.
  ///falls mehrere Routen zur Auswahl stehen, wird ein Auswhaldialog angezeigt
  void _getRoute() {
    //Routendaten empfangen
    DirectionsUtils.displayDirectionGetter(context, TravelMode.driving)
        .then((Directions directions) {
      if (directions == null) return;
      //Wenn nur ein Ergebnis, dieses direkt benutzen
      if (directions.routes.length == 1) {
        _route = directions.routes[0];
      } else {
        //Ansonsten Popup zur Auswahl anzeigen
        print(directions.routes.length.toString() +
            " Routen gefunden. Zeige Auswahlmenü...");

        showDialog(
            builder: (BuildContext context) {
              return SimpleDialog(title: Text("Bitte Route wählen"), children: [
                for (DirectionRoute route in directions.routes)
                  ListTile(
                    title: Text("über ${route.summary}"),
                    onTap: () {
                      setState(() {
                        _route = route;
                        _distance =
                            (route.legs[0].distance.value / 1000).toDouble();
                        Navigator.pop(context);
                      });
                    },
                  )
              ]);
            },
            context: context);
      }
    });
  }
}

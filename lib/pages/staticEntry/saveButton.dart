import 'package:eMission/utils/dateTime.dart';
import 'package:flutter/material.dart';
import 'package:eMission/models/staticEntry.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/utils/units.dart';

class StaticEntrySaveButton extends StatelessWidget {
  const StaticEntrySaveButton({
    Key key,
    @required this.entry,
    this.recommendedSwitchDate,
  }) : super(key: key);

  final StaticEntry entry;
  final DateTime recommendedSwitchDate;

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<DataModel>(
      builder: (context, child, model) {
        return FloatingActionButton.extended(
          label: Text(UnitUtils.printEmissions(entry.emissionsPerDay) +
              " pro Tag festlegen"),
          icon: Icon(Icons.save),
          onPressed: () {
            if (model.staticEntries[entry.runtimeType].isEmpty) {
              model.addStaticEntry(entry, model.toBeUploaded);
              Navigator.pop(context);
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text("Stromverbrauch festlegelt"),
              ));
            } else {
              //wenn schon ein StaticEntry dieses Typs existiert, Dialog zum Wechseldatum festlegen
              DateTime switchDate = recommendedSwitchDate;
              showDialog(
                  context: context,
                  builder: (context) {
                    return SimpleDialog(
                      children: <Widget>[
                        Text(
                            "Du hast hierfür schonmal Daten festgelegt. Ab wann sollen die neuen gelten?"),
                        RaisedButton(
                          color: switchDate == null
                              ? Colors.red
                              : Theme.of(context).buttonColor,
                          child: Text(
                            switchDate == null
                                ? "Datum auswählen"
                                : DateTimeUtils.toDateString(switchDate),
                            style: TextStyle(
                                color: switchDate == null
                                    ? Colors.white
                                    : Theme.of(context)
                                        .buttonTheme
                                        .colorScheme
                                        .primary),
                          ),
                          onPressed: () {
                            showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              lastDate: DateTime(2100),
                              firstDate: DateTime(1970),
                            ).then((DateTime value) {
                              model.addStaticEntry(entry.withStartDate(value),
                                  model.toBeUploaded);
                              Navigator.pop(context);
                              Navigator.pop(context);
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text("Stromemissionen festgelegt"),
                              ));
                            });
                          },
                        ),
                      ],
                    );
                  });
            }
          },
        );
      },
    );
  }
}

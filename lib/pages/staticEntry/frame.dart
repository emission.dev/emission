import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StaticEntryPageFrame extends StatefulWidget {
  @override
  StaticEntryPageFrameState createState() => StaticEntryPageFrameState();
}

class StaticEntryPageFrameState extends State<StaticEntryPageFrame> {
  PageController _pageController = PageController();

  ///Anzahl an Personen im Haushalt. Wird bei Heizungen und Strom verwendet
  int peopleInHousehold = 1;

  ///Erzeugt den PageView, in dem die einzelnen Seiten verwaltet werden.
  Widget buildPageView(List<Widget> pages) {
    return Scaffold(
        body: PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: pages,
    ));
  }

  ///Button, der zum nächsten Schritt führt. Ist erst aktiviert, wenn alle Daten des aktuellen Schrittes ausgefüllt wurden
  FloatingActionButton continueButton() {
    return FloatingActionButton.extended(
      label: Text("Weiter"),
      icon: Icon(Icons.forward),
      onPressed: () {
        _pageController.nextPage(
            duration: Duration(milliseconds: 400), curve: Curves.easeIn);
      },
    );
  }

  ///Button, um eine Seite zurück zu springen
  FlatButton buildBackButton() {
    return FlatButton(
      child: Text("Zurück",
          style:
              Theme.of(context).textTheme.body1.copyWith(color: Colors.white)),
      onPressed: () {
        _pageController.previousPage(
            duration: Duration(milliseconds: 400), curve: Curves.easeOut);
      },
    );
  }

  ///Grundlegendes Geräst jeder Seite
  Widget buildSinglePageFrame(
      {List<Widget> children,
      bool showContinueButton = false,
      Widget customContinueButton,
      Color backgroundColor = Colors.green}) {
    if (customContinueButton == null)
      customContinueButton = continueButton();
    else
      showContinueButton = true;
    return Scaffold(
      body: GestureDetector(
        onHorizontalDragUpdate: (DragUpdateDetails details) {
          //Nach links wischen geht auf die vorherige Seite, aber die nächste kann nicht durch nach rechts wischen erreicht werden
          if (details.delta.dx > 0 && _pageController.offset > 0)
            _pageController.jumpTo(_pageController.offset - details.delta.dx);
        },
        onHorizontalDragEnd: (DragEndDetails details) {
          //Wenn auf der ersten Seite nach links gewischt wird, zur vorherigen Seite zurückkehren
          if (_pageController.offset < 0.001 &&
              details.velocity.pixelsPerSecond.dx > 0) Navigator.pop(context);
        },
        child: Container(
            padding: EdgeInsets.all(10),
            color: backgroundColor,
            width: MediaQuery.of(context).size.width,
            child: SafeArea(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: children,
              ),
            )),
      ),
      floatingActionButton:
          showContinueButton ? customContinueButton : SizedBox(),
    );
  }

  ///Zeigt ein Cupertino - Auswahlfeld zum Festlegen der Personen im Haushalt an.
  buildPeopleCountSelector({Color color = Colors.green}) {
    return Container(
      height: 150,
      child: CupertinoPicker(
        children: <Widget>[
          Text("1 Person",
              style: Theme.of(context)
                  .textTheme
                  .title
                  .copyWith(color: Colors.white)),
          for (var i = 2; i < 10; i++)
            Text("$i Personen",
                style: Theme.of(context)
                    .textTheme
                    .title
                    .copyWith(color: Colors.white)),
        ],
        backgroundColor: color,
        itemExtent: 30,
        onSelectedItemChanged: (int value) {
          setState(() {
            peopleInHousehold = value + 1;
          });
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return null;
  }
}

import 'package:eMission/pages/staticEntry/frame.dart';
import 'package:eMission/utils/dateTime.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:eMission/models/staticEntry.dart';
import 'saveButton.dart';

class PowerStaticEntryPage extends StaticEntryPageFrame {
  @override
  _PowerStaticEntryPageState createState() => _PowerStaticEntryPageState();
}

class _PowerStaticEntryPageState extends StaticEntryPageFrameState {
  ///Verbrauch in kWh
  double consumption;

  ///Anteil des Energieerzeugers an Gesamtenergie.
  double coal = 0.366,
      atomic = 0.117,
      gas = 0.132,
      renewable = 0.323,
      other = 0.062;

  ///Begrenzung des Zeitraums, in dem die Menge Strom verbraucht wurde, die in consumption gerspeichert wird
  DateTime timeFrameStart, timeFrameEnd;

  ///Legt fest, wie der Benutzer den Strommix festlegen will
  PowerMixType powerMixType;

  ///Anzahl an Tagen im Zeitraum
  int dayCount;
  @override
  Widget build(BuildContext context) {
    return buildPageView([
      _buildIntroPage(),
      _buildPowerConsumptionPage(),
      _buildEnergyProviderSelectoinPage(),
    ]);
  }

  ///Auf dieser Seite wird etwas Text angezeigt und die Anzahl an Personen im Haushalt abgefragt
  Widget _buildIntroPage() {
    return buildSinglePageFrame(
      showContinueButton: true,
      children: <Widget>[
        Text("Strom",
            style: Theme.of(context)
                .textTheme
                .display3
                .copyWith(color: Colors.white)),
        Text(
            "Hast du gerade etwas Zeit? Dann such doch mal deine letzte Stromrechnung raus. Damit können wir herausfinden, wieviel Emissionen dein Stromverbrauch ausmacht." +
                "Zuerst aber noch eine einfache Frage:\n",
            textAlign: TextAlign.justify,
            style: Theme.of(context)
                .textTheme
                .subhead
                .copyWith(color: Colors.white)),
        Text(
          "Wieviele Personen leben in deinem Haushalt?",
          style:
              Theme.of(context).textTheme.title.copyWith(color: Colors.white),
          textAlign: TextAlign.center,
        ),
        buildPeopleCountSelector(),
        Text(
            "Daraus wird ausgerechnet, wie hoch dein Anteil am Stromverbrauch deines Haushalts ist",
            textAlign: TextAlign.justify,
            style: Theme.of(context)
                .textTheme
                .subhead
                .copyWith(color: Colors.white)),
      ],
    );
  }

  ///Hier werden folgende Daten der Stromrechnung abgefragt: gesamter Verbrauch, Zeitraum des Verbrauchs, Gültigkeitsdatum des Eitnrags
  Widget _buildPowerConsumptionPage() {
    return buildSinglePageFrame(
      showContinueButton: dayCount != null && consumption != null,
      children: [
        Text(
            "Hier musst du jetzt einige Daten eintragen, die du auf deiner Stromrechnung findest\n",
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .subhead
                .copyWith(color: Colors.white)),
        TextField(
          decoration: InputDecoration(
              suffix: Text("kWh"), hintText: "hier Stromverbrauch eingeben"),
          onChanged: (String value) {
            setState(() {
              consumption = double.tryParse(value);
            });
          },
        ),
        SizedBox(
          height: 20,
        ),
        _buildTimeFrameSelector(),
        SizedBox(
          height: 30,
        ),
        buildBackButton()
      ],
    );
  }

  ///Hier wird abgefragt, welche Energieträger verwendet werden
  Widget _buildEnergyProviderSelectoinPage() {
    if (powerMixType == null)
      setState(() {
        print("powerMixType auf average gesetzts");
        powerMixType = PowerMixType.average;
      });
    return buildSinglePageFrame(children: <Widget>[
      Text("Strommix auswählen",
          textAlign: TextAlign.justify,
          style:
              Theme.of(context).textTheme.title.copyWith(color: Colors.white)),
      _buildPowerMixRadio("Deutscher Strommix", PowerMixType.average),
      _buildPowerMixRadio("100% Erneuerbar", PowerMixType.renewable),
      _buildPowerMixRadio("Selbst festlegen", PowerMixType.custom),
      _buildManualPowerMixControl(),
      buildBackButton()
    ], customContinueButton: _finishButton());
  }

  ///Radio-Buttons zur Auswahl des PowerMixTypes
  RadioListTile<PowerMixType> _buildPowerMixRadio(
      String desc, PowerMixType pmt) {
    return RadioListTile<PowerMixType>(
      title: Text(desc,
          style: Theme.of(context)
              .textTheme
              .subtitle
              .copyWith(color: Colors.white)),
      value: pmt,
      groupValue: powerMixType,
      onChanged: (PowerMixType value) {
        setState(() {
          powerMixType = value;
        });
      },
    );
  }

  ///Zusammenfassung der Widgets zum Festlegen des Zeitraums. Wird nur in _buildPowerConsumptionPage verwendet
  Widget _buildTimeFrameSelector() {
    return Column(
      children: <Widget>[
        Text("Zeitraum",
            textAlign: TextAlign.justify,
            style: Theme.of(context)
                .textTheme
                .title
                .copyWith(color: Colors.white)),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text("vom",
                style: Theme.of(context)
                    .textTheme
                    .subhead
                    .copyWith(color: Colors.white)),
            RaisedButton(
              color: timeFrameStart == null
                  ? Colors.red
                  : Theme.of(context).buttonColor,
              child: Text(
                timeFrameStart == null
                    ? "Anfangsdatum"
                    : DateTimeUtils.toDateString(timeFrameStart),
                style: TextStyle(
                    color: timeFrameStart == null
                        ? Colors.white
                        : Theme.of(context).buttonTheme.colorScheme.primary),
              ),
              onPressed: () {
                showDatePicker(
                  context: context,
                  initialDate: timeFrameEnd ?? DateTime.now(),
                  lastDate: timeFrameEnd ?? DateTime.now(),
                  firstDate: DateTime(1970),
                ).then((DateTime value) {
                  setState(() {
                    timeFrameStart = value;
                    if (timeFrameEnd != null)
                      dayCount =
                          timeFrameEnd.difference(timeFrameStart).inDays + 1;
                    else
                      dayCount = null;
                  });
                });
              },
            ),
            Text("bis",
                style: Theme.of(context)
                    .textTheme
                    .subhead
                    .copyWith(color: Colors.white)),
            //Button zum Festlegen des Enddatums. Ist noch kein Datum ausgewählt, ist dieser Rot
            RaisedButton(
              color: timeFrameEnd == null
                  ? Colors.red
                  : Theme.of(context).buttonColor,
              child: Text(
                timeFrameEnd == null
                    ? "Enddatum"
                    : DateTimeUtils.toDateString(timeFrameEnd),
                style: TextStyle(
                    color: timeFrameEnd == null
                        ? Colors.white
                        : Theme.of(context).buttonTheme.colorScheme.primary),
              ),
              onPressed: () {
                showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  lastDate: DateTime.now(),
                  firstDate: timeFrameStart ?? DateTime(1970),
                ).then((DateTime value) {
                  setState(() {
                    timeFrameEnd = value;
                    if (timeFrameStart != null)
                      dayCount =
                          timeFrameEnd.difference(timeFrameStart).inDays + 1;
                    else
                      dayCount = null;
                  });
                });
              },
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Text(dayCount != null ? "(" + dayCount.toString() + " Tage)" : "",
            style: Theme.of(context)
                .textTheme
                .body1
                .copyWith(color: Colors.white)),
      ],
    );
  }

  ///Button, über den der fertige Eintrag hinzugefügt wird. Sofern ein anderer Eintrag existiert, wird nach dem
  ///Wechseldatum gefragt
  _finishButton() {
    //Validiere Eingaben und erstelle Eintrag
    if (consumption == null || dayCount == null) return null;
    PowerMix powerMix;
    if (powerMixType == PowerMixType.average)
      powerMix = PowerMix.average();
    else if (powerMixType == PowerMixType.renewable)
      powerMix = PowerMix.renewable();
    else if (other >= 0)
      powerMix =
          PowerMix(coal: coal, atomic: atomic, gas: gas, renewable: renewable);
    if (powerMix == null) return null;
    PowerStaticEntry entry = PowerStaticEntry(
      consumptionPerDay: (consumption / (dayCount * peopleInHousehold)),
      powerMix: powerMix,
      start: DateTime(0),
    );
    //Zeige Dialog und füge Eintrag hinzu
    return new StaticEntrySaveButton(
      entry: entry,
      recommendedSwitchDate: timeFrameEnd,
    );
  }

  ///Erzeugt vier Schieberegler zum individuellen Festlegen des Strommixes
  _buildManualPowerMixControl() {
    if (powerMixType == PowerMixType.average ||
        powerMixType == PowerMixType.renewable) return SizedBox();
    return Column(
      children: <Widget>[
        ListTile(
          leading: Text("Atomkraft ",
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: Colors.white)),
          title: Slider(
            min: 0,
            max: 1,
            activeColor: Colors.white,
            divisions: 100,
            value: atomic,
            onChanged: (double value) {
              setState(() {
                atomic = value;
                other = 1 - atomic - coal - renewable - gas;
              });
            },
          ),
          trailing: Text(atomic.toStringAsFixed(2),
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: Colors.white)),
        ),
        ListTile(
          leading: Text("Erneuerbar",
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: Colors.white)),
          title: Slider(
            min: 0,
            max: 1,
            activeColor: Colors.white,
            divisions: 100,
            value: renewable,
            onChanged: (double value) {
              setState(() {
                renewable = value;
                other = 1 - atomic - coal - renewable - gas;
              });
            },
          ),
          trailing: Text(renewable.toStringAsFixed(2),
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: Colors.white)),
        ),
        ListTile(
          leading: Text("Erdgas       ",
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: Colors.white)),
          title: Slider(
            min: 0,
            max: 1,
            activeColor: Colors.white,
            divisions: 100,
            value: gas,
            onChanged: (double value) {
              setState(() {
                gas = value;
                other = 1 - atomic - coal - renewable - gas;
              });
            },
          ),
          trailing: Text(gas.toStringAsFixed(2),
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: Colors.white)),
        ),
        ListTile(
          leading: Text("Kohle        ",
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: Colors.white)),
          title: Slider(
            min: 0,
            max: 1,
            activeColor: Colors.white,
            divisions: 100,
            value: coal,
            onChanged: (double value) {
              setState(() {
                coal = value;
                other = 1 - atomic - coal - renewable - gas;
              });
            },
          ),
          trailing: Text(coal.toStringAsFixed(2),
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: Colors.white)),
        ),
        Text("Sonstige: " + other.toStringAsFixed(2),
            style: Theme.of(context)
                .textTheme
                .body1
                .copyWith(color: Colors.white)),
      ],
    );
  }
}

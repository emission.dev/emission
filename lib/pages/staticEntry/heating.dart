import 'package:eMission/models/staticEntry.dart';
import 'package:eMission/pages/staticEntry/frame.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'saveButton.dart';

class HeatingStaticEntryPage extends StaticEntryPageFrame {
  @override
  _HeatingStaticEntryPageState createState() => _HeatingStaticEntryPageState();
}

class _HeatingStaticEntryPageState extends StaticEntryPageFrameState {
  HeatingStaticEntry result;
  HeatingType _ht = HeatingType.oil;
  HeatingUnit _hu = HeatingUnit.kWh;
  double amount;
  @override
  Widget build(BuildContext context) {
    return buildPageView([_buildIntroPage(), _buildDetailsSelector()]);
  }

  Widget _buildIntroPage() {
    return buildSinglePageFrame(
        backgroundColor: Colors.orange,
        children: [
          Text("Heizung",
              style: Theme.of(context)
                  .textTheme
                  .display3
                  .copyWith(color: Colors.white)),
          Text(
              "Hast du gerade etwas Zeit? Dann such doch mal deine letzte Heizkostenabrechnung raus. Damit können wir herausfinden, wieviel Emissionen deine Heizung ausmacht." +
                  "Zuerst aber noch eine einfache Frage:\n",
              textAlign: TextAlign.justify,
              style: Theme.of(context)
                  .textTheme
                  .subhead
                  .copyWith(color: Colors.white)),
          Text(
            "Wieviele Personen leben in deinem Haushalt?",
            style:
                Theme.of(context).textTheme.title.copyWith(color: Colors.white),
            textAlign: TextAlign.center,
          ),
          buildPeopleCountSelector(color: Colors.orange),
          Text(
              "Daraus wird ausgerechnet, wie hoch dein Anteil an der Heizungsnutzung deines Haushalts ist",
              textAlign: TextAlign.justify,
              style: Theme.of(context)
                  .textTheme
                  .subhead
                  .copyWith(color: Colors.white)),
        ],
        showContinueButton: true);
  }

  Widget _buildDetailsSelector() {
    List<HeatingUnit> units =
        unitConversion[_ht] == null ? [] : unitConversion[_ht].keys.toList();
    return buildSinglePageFrame(
        backgroundColor: Colors.orange,
        customContinueButton: result != null
            ? StaticEntrySaveButton(
                entry: result,
              )
            : null,
        children: [
          Text(
            "Welche Art Heizung benutzt du?",
            style:
                Theme.of(context).textTheme.title.copyWith(color: Colors.white),
            textAlign: TextAlign.center,
          ),
          _buildHeatingTypeSelector(),
          Text(
            "Und wieviel Heizstoff wurde verbraucht?",
            style:
                Theme.of(context).textTheme.title.copyWith(color: Colors.white),
            textAlign: TextAlign.center,
          ),
          _ht == HeatingType.power
              ? Container(
                  height: 200,
                  child: Center(child: Text(
                    'Stromverbrauch kannst du an anderer Stelle eingeben. Wir sind hier also  schon fertig!',
                    style: Theme.of(context)
                        .textTheme
                        .title
                        .copyWith(color: Colors.white),
                    textAlign: TextAlign.center,
                  ),)
                )
              : Row(
                  children: <Widget>[
                    Container(
                      child: TextField(
                        onChanged: (value) {
                          setState(() {
                            amount = double.tryParse(value);
                            _buildEntry();
                          });
                        },
                      ),
                      width: MediaQuery.of(context).size.width * 0.6,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.3,
                      height: 200,
                      child: CupertinoPicker(
                        children: <Widget>[
                          for (HeatingUnit u in units)
                            Text(heatingUnitEnumMap[u],
                                style: Theme.of(context)
                                    .textTheme
                                    .title
                                    .copyWith(color: Colors.white)),
                          Text(heatingUnitEnumMap[HeatingUnit.kWh],
                              style: Theme.of(context)
                                  .textTheme
                                  .title
                                  .copyWith(color: Colors.white)),
                        ],
                        backgroundColor: Colors.orange,
                        itemExtent: 30,
                        onSelectedItemChanged: (int value) {
                          setState(() {
                            _hu = value == units.length
                                ? HeatingUnit.kWh
                                : units[value];
                            _buildEntry();
                          });
                        },
                      ),
                    ),
                  ],
                )
        ]);
  }

  Container _buildHeatingTypeSelector() {
    return Container(
      height: 150,
      child: CupertinoPicker(
        children: <Widget>[
          for (HeatingType type in HeatingType.values)
            Text(heatingTypeEnumMap[type],
                style: Theme.of(context)
                    .textTheme
                    .title
                    .copyWith(color: Colors.white)),
        ],
        backgroundColor: Colors.orange,
        itemExtent: 30,
        onSelectedItemChanged: (int value) {
          setState(() {
            _ht = HeatingType.values[value];
            _buildEntry();
          });
        },
      ),
    );
  }

  void _buildEntry() {
    if (_ht == HeatingType.power)
      result = HeatingStaticEntry.power(DateTime(0));
    else if (_hu != null &&
        _ht != null &&
        amount != null &&
        (_hu == HeatingUnit.kWh)) {
      result = HeatingStaticEntry(
          heatingType: _ht,
          unitAmount: amount / peopleInHousehold,
          unit: _hu,
          start: DateTime(0));
    } else
      result = null;
  }
}

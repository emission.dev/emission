import 'package:eMission/pages/staticEntry/frame.dart';
import 'package:flutter/material.dart';

class ConsumptionStaticEntryPage extends StaticEntryPageFrame {
  @override
  _ConsumptionStaticEntryPageState createState() =>
      _ConsumptionStaticEntryPageState();
}

class _ConsumptionStaticEntryPageState extends StaticEntryPageFrameState {
  @override
  Widget build(BuildContext context) {
    return buildSinglePageFrame(
        backgroundColor: Colors.lightBlue,
        children: [
          Center(
            child: Text(
                'Zum Konsum haben wir leider noch keine verlässliche Datenquelle gefunden, sorry. Umter Konsum fallen übrigens etwas Klamotten, Handys, Autos, und was du sonst noch so außer Essen kaufst.',
                style: Theme.of(context)
                    .textTheme
                    .subtitle
                    .copyWith(color: Colors.white)),
          )
        ],
        customContinueButton: FloatingActionButton.extended(
          label: Text('Zurück'),
          onPressed: () {
            Navigator.pop(context);
          },
        ));
  }
}

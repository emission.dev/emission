import 'package:eMission/pages/staticEntry/frame.dart';
import 'package:flutter/material.dart';

class FoodStaticEntryPage extends StaticEntryPageFrame {
  @override
  _FoodStaticEntryPageState createState() => _FoodStaticEntryPageState();
}

class _FoodStaticEntryPageState extends StaticEntryPageFrameState {
  @override
  Widget build(BuildContext context) {
    return buildSinglePageFrame(
        backgroundColor: Colors.red,
        children: [
          SingleChildScrollView(
            child: Center(
              child: Text(
                  'Zur Ernährung haben wir leider noch keine verlässliche Datenquelle gefunden, sorry. Einiges wissen wir aber mit Sícherheit: \n\n - Tierische Produkte erzeugen grundsätzlich am meisten Emissionen,so weit keine Überraschung. Wenn es dir - so wie uns auch - aber schwerfällt, ganz darauf zu verzichten, kannst du trotzdem Einiges verbessern. Wusstest du, dass Rindfleisch vier mal so viele Emissionen erzeugt wie Geflügel oder Schweinefleisch? \n\n - Meistens ist es auch gesund, umweltfreundlich zu leben. Schon mal versucht, auf Kuhilch zu verzichten? Soja- und Hafermilch enthalten zum Beispiel auch deutlich weniger Zucker.',
                  style: Theme.of(context)
                      .textTheme
                      .subtitle
                      .copyWith(color: Colors.white)),
            ),
          )
        ],
        customContinueButton: FloatingActionButton.extended(
          label: Text('Zurück'),
          onPressed: () {
            Navigator.pop(context);
          },
        ));
  }
}

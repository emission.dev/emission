import 'package:eMission/models/entry.dart';
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/models/shortcut.dart';
import 'package:eMission/models/vehicles.dart';
import 'package:eMission/utils/ui.dart';
import 'package:eMission/pages/add/motorized.dart';
import 'package:eMission/pages/add/plane.dart';
import 'package:eMission/pages/add/public.dart';
import 'package:eMission/widgets/defaultUiElements.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class AddEntryPage extends StatefulWidget {
  final bool _shortcutMode;
  final DateTime date;

  AddEntryPage(this._shortcutMode, this.date, {Key key}) : super(key: key);
  @override
  AddEntryPageState createState() => AddEntryPageState();

  ///Zeigt die seite mit dem enstsprechenden Widget an
  factory AddEntryPage.selectVehicle(
      bool shortcutMode, DateTime date, VehicleType vt) {
    switch (vt) {
      case VehicleType.plane:
        return PlaneAddEntryPage(shortcutMode, date);
      case VehicleType.motorizedVehicle:
        return MotorizedAddEntryPage(shortcutMode, date);
      case VehicleType.public:
        return PublicAddEntryPage(shortcutMode, date);
    }
    return null;
  }
}

class AddEntryPageState extends State<AddEntryPage> {
  Entry entry;
  String description;

  @override
  Widget build(BuildContext context) {
    print(
        "Fehler!! AddEntryPage soll erweitert werden, nicht direkt aufrufen!!");
    return null;
  }

  ///Baut den fahrzeugunabhängigen Teil der AddEntry-Page.
  ///Parameter:
  /// - Widget child: fahrzeugabhängiger Teil der Seite
  /// - [FloatingActionButton] floatingActionButton: Wenn ein eigener FAB definiert wurde, diesen hier einfügen
  ///   ansonsten ist hier der FAB zum Speichern des Eintrags
  Widget buildAddEntryFrame({Widget child, Widget floatingActionButton}) {
    if (floatingActionButton == null) floatingActionButton = buildSaveFAB();
    return Scaffold(
        body: Container(
          color: Colors.green,
          child: SafeArea(
            child: DefaultCard(
              child: Column(
                children: <Widget>[
                  LayoutBuilder(
                    builder:
                        (BuildContext context, BoxConstraints constraints) {
                      return Container(
                        width: constraints.maxWidth,
                        decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            borderRadius: BorderRadius.vertical(
                                top: Radius.circular(20.0))),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                widget._shortcutMode
                                    ? "Shortcut hinzufügen"
                                    : "Eintrag hinzufügen",
                                style: TextStyle(
                                    fontSize: 30, color: Colors.white),
                                textAlign: TextAlign.start,
                              ),
                            ),
                            TextField(
                              decoration: InputDecoration(
                                  hintText: entry == null
                                      ? "Beschreibung (optional)"
                                      : entry.toString()),
                              onChanged: (String value) {
                                setState(() {
                                  description = value;
                                });
                              },
                            )
                          ],
                        ),
                      );
                    },
                  ),
                  Expanded(
                    child: child,
                  ),
                ],
              ),
            ),
          ),
        ),
        floatingActionButton: buildSaveFAB());
  }

  Widget buildSaveFAB() {
    ///Warnung, die angezeigt wird, falls keine eigene Beschreibung für Shortcuts eingegeben wurde.
    ///Grund: Generische Titel sind ungenügend für alltägliche Wege.
    Widget buildNoDescriptionWarning(BuildContext context) {
      return AlertDialog(
        title: Text(
            'Wir empfehlen dir sehr, für einen Shortcut eine Beschreibung festzulegen, die die Aktion in einem Wort beschreibt (z.B. "Arbeit" für deinen Arbeitsweg oder "Supermarkt" für den Weg dorthin)'),
        actions: <Widget>[
          FlatButton(
            child: Text("Mir egal"),
            onPressed: () {
              Navigator.pop(context, true);
            },
          ),
          FlatButton(
            child: Text("Okay"),
            onPressed: () => Navigator.pop(context, false),
          )
        ],
      );
    }

    return ScopedModelDescendant<DataModel>(
        builder: (context, unusedChild, model) {
      return FloatingActionButton.extended(
          icon: Icon(Icons.add),
          label: Text(widget._shortcutMode
              ? "Shortcut speichern"
              : "Eintrag speichern"),
          onPressed: () {
            if (description != null) if (description.isNotEmpty)
              entry.description = description;
            try {
              if (widget._shortcutMode && description.isEmpty)
                showDialog(
                        context: context,
                        builder: (BuildContext context) =>
                            buildNoDescriptionWarning(context))
                    .then((dynamic value) {
                  if (value) {
                    model.addShortcut(Shortcut(entry));
                    Navigator.pop(context);
                  }
                });
              else {
                widget._shortcutMode
                    ? model.addShortcut(Shortcut(entry))
                    : model.addEntry(entry, true, model.toBeUploaded);
                Navigator.pop(
                    context,
                    widget._shortcutMode
                        ? UiUtils.showAddShortcutSnackbar(entry)
                        : UiUtils.showAddEntrySnackbar(entry));
              }
            } catch (e) {
              print("Fehler +$e");
              showDialog(
                  context: context,
                  builder: (context) {
                    String text;
                    if (entry == null)
                      text =
                          "Du musst erst was eingeben, bevor du einen Eintrag speichern kannst :)";
                    else
                      text =
                          "Irgendwie kann ich deinen Eintrag nicht speichern. " +
                              e.toString();
                    return AlertDialog(
                      title: Text("Fehler beim Speichern"),
                      content: Text(text),
                      actions: <Widget>[
                        FlatButton(
                          onPressed: () => Navigator.pop(context),
                          child: Text("Okay"),
                        )
                      ],
                    );
                  });
            }
          });
    });
  }
}

import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/models/staticEntry.dart';
import 'package:eMission/utils/dateTime.dart';
import 'package:eMission/utils/units.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

///Zeigt ein Balkendiagramm, dass die erzeugten Emissionen für einen festgelegten Zeitraum anzeigt und vergleicht.
///Verglichen wird mit:
///- Dem deutschen Durchschnitt
///- Dem Benutzer im vorherigen Zeitraum (z.B. Gestern, wenn Heute als Zeitraum festgelegt wurde)
class DetailsBarChart extends StatelessWidget {
  final double screenWidth;
  final int dayCount;
  final DataModel _model;
  DetailsBarChart(this.screenWidth, this.dayCount, this._model);
  @override
  Widget build(BuildContext context) {
    List<charts.Series<ListData, String>> listData =
        _generateDetailsListData(dayCount);
    return charts.BarChart(
      listData,
      vertical: false,
      animate: true,
      barRendererDecorator: charts.BarLabelDecorator<String>(
          insideLabelStyleSpec: charts.TextStyleSpec()),
      // Hide domain axis.
      domainAxis:
          new charts.OrdinalAxisSpec(renderSpec: new charts.NoneRenderSpec()),
    );
  }

///Generiert die Daten, und gibt sie in einem für [charts_flutter] nutzbaren Format zurück
  List<charts.Series<ListData, String>> _generateDetailsListData(int dayCount) {
    List<ListData> children = [];
    if (dayCount == 0) {
      children.add(ListData(
          _getEmissions(_model.displayDate, 0),
          "Du ${DateTimeUtils.toDateString(_model.displayDate).toLowerCase()}",
          true));
      children.add(ListData(
          _getEmissions(_model.displayDate.subtract(Duration(days: 1)), 0),
          "Du ${DateTimeUtils.toDateString(_model.displayDate.subtract(Duration(days: dayCount + 1))).toLowerCase()}",
          false));
    }
    if (dayCount > 0) {
      children.add(ListData(
          _getEmissions(_model.displayDate, dayCount),
          "Du vom ${DateTimeUtils.toDateString(_model.displayDate.subtract(Duration(days: dayCount)))}" +
              " bis " +
              "${DateTimeUtils.toDateString(_model.displayDate).toLowerCase()}",
          true));
      children.add(
        ListData(
            _getEmissions(
                _model.displayDate.subtract(Duration(days: dayCount + 1)),
                dayCount),
            "Du vom ${DateTimeUtils.toDateString(_model.displayDate.subtract(Duration(days: dayCount * 2 + 1)))}" +
                " bis " +
                "${DateTimeUtils.toDateString(_model.displayDate.subtract(Duration(days: dayCount + 1))).toLowerCase()}",
            false),
      );
    }
    children.add(ListData(
        (9000 * (dayCount + 1) / 365), "deutscher Durchschnitt", false));
    children.sort((ListData l1, ListData l2) {
      return (l1.emissions - l2.emissions).round();
    });
    List<charts.Series<ListData, String>> series = [
      charts.Series(
        data: children,
        id: "ChartData",
        domainFn: (ListData listData, _) => listData.name,
        measureFn: (ListData listData, _) => listData.emissions,
        labelAccessorFn: (ListData listData, _) =>
            listData.name + ": " + UnitUtils.printEmissions(listData.emissions),
        colorFn: (ListData listData, _) => listData.isHighlited
            ? charts.Color(
                a: Colors.green.alpha,
                r: Colors.green.red,
                g: Colors.green.green,
                b: Colors.green.blue)
            : charts.Color(
                a: Colors.lime.alpha,
                r: Colors.lime.red,
                g: Colors.lime.green,
                b: Colors.lime.blue),
      )
    ];

    return series;
  }

///Gibt die Gesamtemissionen für den gewählten Zeitraum zurück (Einträge und statische Einträge)
  double _getEmissions(DateTime endDate, int dayCount) {
    double emissions = 0;
    for (var i = 0; i <= dayCount; i++) {
      emissions +=
          _model.getDailyEntryEmissions(endDate.subtract(Duration(days: i)));
      _model
          .getStaticEntriesForDay(endDate.subtract(Duration(days: i)))
          .forEach((Type type, StaticEntry staticEntry) {
        emissions += staticEntry.emissionsPerDay;
      });
    }
    return emissions;
  }
}

///Enzhält Daten für einen Balken des Diagramms
class ListData {
  final String name;
  final double emissions;
  ///Ist für den angeforderten Datumsbereich, der farblich von den Vergleichswerten hervorgehoben wird
  final bool isHighlited;
  ListData(this.emissions, this.name, this.isHighlited);
}

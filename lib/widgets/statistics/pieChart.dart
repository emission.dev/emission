import 'package:auto_size_text/auto_size_text.dart';
import 'package:eMission/models/entry.dart';
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/models/staticEntry.dart';
import 'package:eMission/utils/units.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class PieChart extends StatelessWidget {
  final double screenWidth;
  final int dayCount;
  final DataModel _model;
  PieChart(this.screenWidth, this.dayCount, this._model);
  @override
  Widget build(BuildContext context) {
    List<charts.Series<ChartData, String>> chartData = _generateChartData(
        _model.displayDate.subtract(Duration(days: 1)), dayCount);
    return Row(
      children: <Widget>[
        _showLegend(chartData),
        _buildPieChart(context, chartData)
      ],
    );
  }

  Stack _buildPieChart(
      BuildContext context, List<charts.Series<ChartData, String>> chartData) {
    return Stack(
      alignment: Alignment(0, 0),
      children: <Widget>[
        Container(
          height: screenWidth * 0.45,
          width: screenWidth * 0.45,
          child: charts.PieChart(
            chartData,
            defaultRenderer: charts.ArcRendererConfig(
              arcWidth: (screenWidth * 0.06).round(),
            ),
          ),
        ),
        Container(
          width: screenWidth * 0.14,
          height: screenWidth * 0.16,
          child: Center(
            child: AutoSizeText(
              UnitUtils.printEmissions(_getEmissions(chartData)),
              maxLines: 2,
              overflow: TextOverflow.clip,
              minFontSize: 10,
              style: TextStyle(fontSize: 100),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
  }

  Widget _showLegend(List<charts.Series<ChartData, String>> chartData) {
    List<Widget> children = [];
    return Container(
      height: screenWidth * 0.45,
      width: screenWidth * 0.45,
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          chartData.forEach((charts.Series<ChartData, String> series) {
            series.data.forEach((ChartData data) {
              Color color = Color.fromARGB(
                  data.color.a, data.color.r, data.color.g, data.color.b);
              children.add(Padding(
                padding: EdgeInsets.all(constraints.maxHeight * 0.025),
                child: Row(
                  children: <Widget>[
                    Container(
                      height: constraints.maxWidth * 0.07,
                      width: constraints.maxWidth * 0.07,
                      decoration: BoxDecoration(color: color),
                    ),
                    Text(
                      " " + staticEntryString[data.emissionType] + ": ",
                      style: TextStyle(fontSize: constraints.maxWidth * 0.07),
                    ),
                    Text(UnitUtils.printEmissions(data.emissions),
                        style:
                            TextStyle(fontSize: constraints.maxWidth * 0.07)),
                  ],
                ),
              ));
            });
          });
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: children,
          );
        },
      ),
    );
  }

  ///Erstellt die Daten für das Diagramm. Wenn für mehrere Tage Daten erstellt werden sollen,
  ///muss ein Enddatum und die Anzahl der Tage als Integer angegeben werden
  List<charts.Series<ChartData, String>> _generateChartData(
      DateTime endDate, int dayCount) {
    Map<Type, double> entryEmissions = {};
    //Füge für die gewählte Tageszahl Werte hinzu
    for (var i = 0; i <= dayCount; i++) {
      if (entryEmissions[Entry] == null)
        entryEmissions[Entry] =
            _model.getDailyEntryEmissions(endDate.subtract(Duration(days: i)));
      else
        entryEmissions[Entry] +=
            _model.getDailyEntryEmissions(endDate.subtract(Duration(days: i)));
      _model
          .getStaticEntriesForDay(endDate.subtract(Duration(days: i)))
          .forEach((Type type, StaticEntry staticEntry) {
        if (entryEmissions[type] == null)
          entryEmissions[type] = staticEntry.emissionsPerDay;
        else
          entryEmissions[type] += staticEntry.emissionsPerDay;
      });
    }
    List<ChartData> generatedData = [];
    entryEmissions.forEach((Type type, double emissions) {
      generatedData.add(ChartData(type, emissions, _colorForType[type]));
    });
    List<charts.Series<ChartData, String>> series = [
      charts.Series(
        data: generatedData,
        id: "ChartData",
        domainFn: (ChartData chartData, _) =>
            _stringForType[chartData.emissionType],
        measureFn: (ChartData chartData, _) => chartData.emissions,
        colorFn: (ChartData chartData, _) => chartData.color,
      )
    ];
    return series;
  }

  ///Summiert die Emissionen der gegebenen Datenreihe
  double _getEmissions(List<charts.Series<ChartData, String>> chartData) {
    double totalChartEmissions = 0;
    chartData[0].data.forEach((ChartData eachChartData) {
      totalChartEmissions += eachChartData.emissions;
    });
    return totalChartEmissions;
  }
}

class ChartData {
  final double emissions;
  final Type emissionType;
  final charts.Color color;
  ChartData(this.emissionType, this.emissions, Color color)
      : this.color = charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}

const Map<Type, Color> _colorForType = {
  Entry: Colors.blue,
  ConsumptionStaticEntry: Colors.white,
  FoodStaticEntry: Colors.amber,
  HeatingStaticEntry: Colors.orange,
  PowerStaticEntry: Colors.yellow,
  PublicStaticEntry: Colors.cyan
};
const Map<Type, String> _stringForType = {
  Entry: "Reisen",
  ConsumptionStaticEntry: "Konsum",
  FoodStaticEntry: "Nahrung",
  HeatingStaticEntry: "Heizen",
  PowerStaticEntry: "Strom",
  PublicStaticEntry: "staatlich"
};

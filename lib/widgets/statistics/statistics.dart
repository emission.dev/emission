import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/pages/friends.dart';
import 'package:eMission/widgets/statistics/graph.dart';
import 'package:eMission/widgets/statistics/pieChart.dart';
import 'package:flutter/material.dart';
import 'package:eMission/widgets/defaultUiElements.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:scoped_model/scoped_model.dart';

class StatisticsPage extends StatefulWidget {
  final Key key;
  StatisticsPage({this.key}) : super(key: key);
  @override
  StatisticsPageState createState() {
    return new StatisticsPageState();
  }
}

class StatisticsPageState extends State<StatisticsPage>
    with SingleTickerProviderStateMixin {
  double screenWidth;
  int _duration = 0;
  int _lastIndex;
  TabController _tabController;
  List<charts.Series<ChartData, Type>> chartData = [];
  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    _tabController.addListener(() => _onTabChanged());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    return ScopedModelDescendant<DataModel>(
      builder: (context, child, model) {
        return Container(
          child: DefaultCard(
            child: Column(
              children: <Widget>[
                Text(
                    'Statistiken',
                    style: Theme.of(context).textTheme.title),
                TabBar(
                  tabs: <Widget>[
                    Tab(text: "1 Tag"),
                    Tab(text: "1 Woche"),
                    Tab(text: "30 Tage")
                  ],
                  controller: _tabController,
                ),
                Container(
                  height: 215,
                  child: model.hasLoaded
                      ? TabBarView(
                          controller: _tabController,
                          physics: NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            Graph(215, screenWidth, 1, model),
                            Graph(215, screenWidth, 7, model),
                            Graph(215, screenWidth, 30, model),
                          ],
                        )
                      : Center(
                          child: CircularProgressIndicator(),
                        ),
                ),
                if (model.loginData == null)
                  FlatButton(
                    child: Text(
                        'Melde dich hier an, um den Verbrauch deiner Freunde zu sehen'),
                    onPressed: () => Navigator.push(context,
                        MaterialPageRoute(builder: (context) => FriendsPage())),
                  ),
                Text(
                    _tabController.index == 0
                        ? 'Detailansicht für gestern'
                        : _tabController.index == 1
                            ? 'Detailansicht für letzte Woche'
                            : 'Detailansicht für letzten Monat',
                    style: Theme.of(context).textTheme.subhead),
                PieChart(screenWidth, _duration, model),
              ],
            ),
          ),
        );
      },
    );
  }

  _onTabChanged() {
    if (_tabController.index != _lastIndex)
      setState(() {
        _lastIndex = _tabController.index;
        switch (_tabController.index) {
          case 1:
            _duration = 6;
            break;
          case 2:
            _duration = 29;
            break;
          default:
            _duration = 0;
        }
      });
  }
}

import 'package:eMission/models/friendData.dart';
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/utils/units.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

///Zeigt ein Liniendiagramm, dass die erzeugten Emissionen für einen festgelegten Zeitraum anzeigt und vergleicht.
///Verglichen wird mit:
///- Dem deutschen Durchschnitt
///- Dem Benutzer im vorherigen Zeitraum (z.B. Gestern, wenn Heute als Zeitraum festgelegt wurde)
///- Freunde
class Graph extends StatelessWidget {
  final double screenWidth, height;

  ///Wieviele Tage pro Datenpunkt zusammengefasst werden sollen
  final int daysPerColumn;
  final DataModel _model;
  Graph(
    this.height,
    this.screenWidth,
    this.daysPerColumn,
    this._model,
  );
  @override
  Widget build(BuildContext context) {
    charts.DateTimeTickFormatterSpec _tickFormatterSpec;
    if (daysPerColumn == 1)
      _tickFormatterSpec = charts.AutoDateTimeTickFormatterSpec(
          day: charts.TimeFormatterSpec(format: "d", transitionFormat: "d.M"));
    List<charts.Series<ListData, DateTime>> listData =
        _generateGraphData(daysPerColumn);
    return Container(
      height: height,
      child: charts.TimeSeriesChart(
        listData,
        animate: true,
        //Legende mit (de)aktivierbaren Freundeseinträgen
        behaviors: [
          charts.SeriesLegend(),
          charts.RangeAnnotation([
            charts.LineAnnotationSegment(
                31 * daysPerColumn, charts.RangeAnnotationAxisType.measure,
                endLabel: "deutscher Durchschnitt"),
            charts.LineAnnotationSegment(
                5.5 * daysPerColumn, charts.RangeAnnotationAxisType.measure,
                color: charts.Color.fromHex(code: "#71f442"),
                endLabel: "nachhaltiger Ausstoß")
          ])
        ],
        //Eigene y-Achsenbeschriftung
        primaryMeasureAxis: charts.NumericAxisSpec(
            tickFormatterSpec: charts.BasicNumericTickFormatterSpec(
                (num number) => UnitUtils.printEmissions(number))),
        //Eigene x-Achsenbeschriftung
        domainAxis:
            charts.DateTimeAxisSpec(tickFormatterSpec: _tickFormatterSpec),
      ),
    );
  }

  ///Generiert alle Datensätze, und gibt sie in einem für [charts_flutter] nutzbaren Format zurück
  ///Datensätze werden erstellt für: Freunde, deutscher Durchschnitt, Benutzer
  List<charts.Series<ListData, DateTime>> _generateGraphData(
      int daysPerColumn) {
    ///Wieviele Daten gleichzeitig angezeigt werden sollen
    int entryCount = (screenWidth * (14 / 400)).round();
    List<charts.Series<ListData, DateTime>> series = [
      //User
      _getGraphData(_model.displayDate, daysPerColumn, entryCount,
          _model.getTotalDailyEmissions, "Du")
      //Durchschnitt
    ];
    //Erstelle Daten für jeden Freund
    _model.friendData.forEach((FriendData data) {
      series.add(_getGraphData(_model.displayDate, daysPerColumn, entryCount,
          data.getEmissions, data.username));
    });
    return series;
  }

  ///Erstellt einen Datensatz für den Graphen im angegeben Zeitraum mithilfe der gegebenen Funktion
  ///Diese muss folgende Syntax haben: 1 positionales Argument (Datum des Tages, für den die Funktion Daten ausgibt)
  charts.Series<ListData, DateTime> _getGraphData(DateTime endDate,
      int daysPerEntry, int entryCount, Function dataFetcher, String name) {
    List<ListData> listData = [];
    for (int i = 1; i < entryCount; i++) {
      DateTime startDate = endDate.subtract(Duration(days: i * daysPerColumn));
      double emissions = 0;
      for (int j = 1; j <= daysPerEntry; j++) {
        emissions += dataFetcher(startDate.add(Duration(days: j)));
      }
      listData.add(ListData(startDate.add(Duration(days: daysPerColumn)),
          emissions == 0 ? null : emissions));
    }
    return charts.Series(
      data: listData.toList(),
      id: name,
      domainFn: (ListData listData, _) => listData.date,
      measureFn: (ListData listData, _) => listData.emissions,
    );
  }
}

///Enzhält Daten für einen Balken des Diagramms
class ListData {
  final DateTime date;
  double emissions = 0;
  ListData(this.date, this.emissions);
  bool operator ==(other) =>
      this.date == other.date && this.emissions == other.emissions;
  int get hashCode => emissions.hashCode * date.hashCode;
}

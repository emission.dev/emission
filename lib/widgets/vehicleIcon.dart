import 'package:flutter/material.dart';

import 'package:eMission/models/directions.dart';
import 'package:eMission/models/vehicles.dart';

///Zeigt das für das jeweilige Vehikel ein passendes Bild an
///Kann entweder einen [VehicleType] oder ein [DirectionsVehicle]
class VehicleIcon extends StatelessWidget {
  final DirectionsVehicle _directionsVehicle;
  final PublicVehicleType _vehicleType;
  final double size;
  VehicleIcon.fromDirectionsApiData(this._directionsVehicle, [this.size])
      : this._vehicleType = getVehicleFromDirections(_directionsVehicle.type),
        assert(_directionsVehicle != null);
  VehicleIcon(this._vehicleType, [this.size]) : this._directionsVehicle = null;
  static const Map<PublicVehicleType, IconData> publicVehicleTypeIconsMap = {
    PublicVehicleType.bus: Icons.directions_bus,
    PublicVehicleType.cable_car: Icons.block,
    PublicVehicleType.commuter_train: Icons.directions_railway,
    PublicVehicleType.ferry: Icons.directions_boat,
    PublicVehicleType.funicular: Icons.block,
    PublicVehicleType.gondola_lift: Icons.block,
    PublicVehicleType.heavy_rail: Icons.directions_railway,
    PublicVehicleType.high_speed_train: Icons.directions_railway,
    PublicVehicleType.intercity_bus: Icons.directions_bus,
    PublicVehicleType.long_distance_train: Icons.directions_railway,
    PublicVehicleType.metro_rail: Icons.subway,
    PublicVehicleType.monorail: Icons.directions_railway,
    PublicVehicleType.other: Icons.block,
    PublicVehicleType.rail: Icons.directions_railway,
    PublicVehicleType.share_taxi: Icons.local_taxi,
    PublicVehicleType.subway: Icons.subway,
    PublicVehicleType.tram: Icons.directions_subway,
    PublicVehicleType.trolleybus: Icons.directions_bus,
  };

  @override
  Widget build(BuildContext context) {
    try {
      return Image.network(
        "https://" + _directionsVehicle.localIcon ??
            "https://" + _directionsVehicle.icon,
        width: size ?? IconTheme.of(context).size,
        height: size ?? IconTheme.of(context).size,
      );
    } catch (e) {
      return Container(
          width: size ?? IconTheme.of(context).size,
          height: size ?? IconTheme.of(context).size,
          child: Icon(publicVehicleTypeIconsMap[_vehicleType]));
    }
  }

  static PublicVehicleType getVehicleFromDirections(
      PublicVehicleType directionsVehicle) {
    return directionsVehicle;
  }
}

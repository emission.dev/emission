import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import 'package:eMission/widgets/home/history.dart';
import 'package:eMission/widgets/home/staticEntries.dart';
import 'package:eMission/widgets/statistics/statistics.dart';

class MainSwiper extends StatefulWidget {
  @override
  MainSwiperState createState() {
    return new MainSwiperState();
  }
}

class MainSwiperState extends State<MainSwiper> {
  final GlobalKey key = GlobalKey();
  List<Widget> widgets;
  RenderBox renderBox;
  @override
  void initState() {
    widgets = [StaticEntriesPage(), StatisticsPage(key: key), HistoryPage()];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Swiper(
      itemCount: 3,
      index: 1,
      loop: false,
      itemBuilder: (BuildContext context, int index) {
        return widgets[index];
      },
    );
  }
}

import 'package:eMission/models/entry.dart';
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/models/shortcut.dart';
import 'package:eMission/utils/ui.dart';
import 'package:flutter/material.dart';
import 'package:eMission/widgets/defaultUiElements.dart';

import 'package:eMission/models/vehicles.dart';
import 'package:scoped_model/scoped_model.dart';

class AddEntryCard extends StatefulWidget {
  final Function _navigateToSubpage;
  AddEntryCard(this._navigateToSubpage);
  @override
  AddEntryCardState createState() {
    return new AddEntryCardState();
  }
}

class AddEntryCardState extends State<AddEntryCard> {
  ///false, falls ein Eintrag hinzugefügt werden soll, true wenn ein Shortcut hinzugefügt werden soll
  bool _shortcutMode = false;
  List<Widget> _chips = [];

  ///Legt den Fahrzeugtyp fest
  Widget _buildVehicleButton(IconData icon, String string, BuildContext context,
      VehicleType selectedWidget) {
    return IconButton(
      onPressed: () {
        widget._navigateToSubpage(selectedWidget, _shortcutMode);
      },
      icon: Icon(icon),
    );
  }

  ///Wird angezeigt, wenn noch kein Fahrzeugtyp ausgewählt wurde. Enthält Buttons zur Fahrzeugauswahl
  Widget _buildVehicleChooser() {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        _buildVehicleButton(
            Icons.airplanemode_active, "Flug", context, VehicleType.plane),
        _buildVehicleButton(
            Icons.directions_car, "Privatfahrzeug", context, VehicleType.motorizedVehicle),
        _buildVehicleButton(
            Icons.directions_transit, "Öffis", context, VehicleType.public),
      ],
    );
  }

  ///Zeigt das ausgewählte Widget und eventuell Buttons zur Navigation
  Widget _buildMainEntryAddWidget(DataModel model) {
    return Column(
      children: <Widget>[
        //Wenn kein Shortcut hinzugefügt werden soll, wird der Datumspicker angezeigt
        _shortcutMode
            ? Text(
                "Shortcut hinzufügen:",
                style: Theme.of(context).textTheme.subhead,
              )
            : SizedBox(),
        Expanded(
          child: _buildVehicleChooser(),
        ),
      ],
    );
  }

  ///Hier werden Chips angezeigt, um einen Eintrag über einen Shortcut hinzuzufügen
  _buildShortcutsRow(DataModel model) {
    List<Shortcut> shortcuts = model.shortcuts;
    if (!_shortcutMode) {
      _chips = [];
      shortcuts.forEach((Shortcut shortcut) {
        _chips.addAll([
          SizedBox(
            width: 5,
          ),
          ActionChip(
            backgroundColor: Theme.of(context).accentColor,
            onPressed: () {
              Entry entry = shortcut.createEntry(model.displayDate);
              model.addEntry(entry, true, model.toBeUploaded);
              Scaffold.of(context).showSnackBar(UiUtils.showAddEntrySnackbar(entry));
            },
            label: Text(shortcut.toString()),
          ),
        ]);
      });
    } else {
      _chips = [];
      shortcuts.forEach((Shortcut shortcut) {
        _chips.addAll([
          SizedBox(
            width: 5,
          ),
          InputChip(
            backgroundColor: Theme.of(context).accentColor,
            onPressed: () {},
            label: Text(shortcut.toString()),
            onDeleted: () {
              model.deleteShortcut(shortcut.entry.dateId);
            },
          ),
        ]);
      });
    }
    _chips.addAll([
      SizedBox(
        width: 5,
      ),
      ChoiceChip(
        avatar: Icon(Icons.edit),
        label: Text("Bearbeiten"),
        backgroundColor: Theme.of(context).accentColor,
        onSelected: (bool value) {
          setState(() {
            _shortcutMode = value;
          });
        },
        selected: _shortcutMode,
      )
    ]);

    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemBuilder: (BuildContext context, int index) {
        return _chips[index];
      },
      itemCount: _chips.length,
    );
  }


  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<DataModel>(
      builder: (BuildContext context, Widget child, DataModel model) {
        return DefaultCard(
          child: Column(
            children: <Widget>[
              Container(
                child: _buildMainEntryAddWidget(model),
                height: 50,
              ),
              Container(
                child: _buildShortcutsRow(model),
                height: 45,
              )
            ],
          ),
        );
      },
    );
  }
}

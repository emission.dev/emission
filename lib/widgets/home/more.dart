import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/pages/friends.dart';
import 'package:eMission/pages/intro.dart';
import 'package:eMission/widgets/defaultUiElements.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class MoreTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<DataModel>(builder: (context, child, model) {
      return ListView(
        children: <Widget>[
          /*DefaultCard(
            child: Container(
              height: 50,
              child: Center(
                child: Text("Warum?"),
              ),
            ),
          ),
          DefaultCard(
            child: Container(
              height: 50,
              child: Center(
                child: Text("Mehr erfahren"),
              ),
            ),
          ),*/
          InkWell(
            child: DefaultCard(
              child: Container(
                height: 50,
                child: Center(
                  child: Text("Zeige noch mal das Tutorial"),
                ),
              ),
            ),
            onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Scaffold(
                          body: IntroPage(),
                        ))),
          ),
          /*  InkWell(
            child: Hero(
                tag: "settings",
                child: DefaultCard(
                  child: Container(
                    height: 50,
                    child: Center(child: Text("Einstellungen")),
                  ),
                )),
            onTap: () =>
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return SettingsPage();
                })),
          ),*/
          InkWell(
            child: Hero(
                tag: "account",
                child: DefaultCard(
                  child: Container(
                    height: 50,
                    child: Center(child: Text("Account & Freunde")),
                  ),
                )),
            onTap: () =>
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return FriendsPage();
                })),
          ),
          DefaultCard(
            child: Container(
              height: 60,
              child: Center(
                child: Text(
                  "Hier werden irgendwann mehr Infos, Tipps und Einstellungen sein.",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          /*RaisedButton(
            child: Text("Load Data"),
            onPressed: () => model.loadData(),
          ),
          RaisedButton(
            child: Text("Create Dummy Data"),
            onPressed: () => Examples.createDummyData(model),
          ),
          RaisedButton(
            child: Text("Reset Data"),
            onPressed: () => StorageUtils.resetData(),
          ),
          RaisedButton(
            child: Text("Save Data"),
            onPressed: () => model.saveData(),
          ),*/
        Text('32bit',style: Theme.of(context).textTheme.body2,)],
      );
    });
  }
}

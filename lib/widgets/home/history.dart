import 'package:eMission/models/vehicles.dart';
import 'package:eMission/pages/addEntry.dart';
import 'package:eMission/utils/dateTime.dart';
import 'package:eMission/utils/ui.dart';
import 'package:eMission/utils/units.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/widgets/defaultUiElements.dart';

class HistoryPage extends StatefulWidget {
  @override
  HistoryPageState createState() {
    return new HistoryPageState();
  }
}

class HistoryPageState extends State<HistoryPage> {
  ScrollController _controller;
  DataModel _model;
  Widget _buildEntries(var entry) {
    return Slidable(
      delegate: SlidableScrollDelegate(),
      child: ListTile(
        title: Text(entry.toString()),
        subtitle: Text("${UnitUtils.printEmissions(entry.totalEntryEmission)}"),
      ),
      secondaryActions: <Widget>[
        IconSlideAction(
          icon: Icons.delete,
          color: Colors.red,
          onTap: () {
            Scaffold.of(context).showSnackBar(
                UiUtils.deleteEntryWithSnackbar(entry.dateId, _model));
          },
        ),
        IconSlideAction(icon: Icons.edit)
      ],
    );
  }

  @override
  void initState() {
    _controller = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<DataModel>(
      builder: (BuildContext context, Widget child, DataModel model) {
        _model = model;
        return DefaultCard(
          child: Column(
            children: <Widget>[
              Expanded(
                child: ListView.builder(
                    controller: _controller,
                    reverse: true,
                    itemBuilder: (BuildContext context, int index) {
                      DateTime dateId =
                          DateTime.now().subtract(Duration(days: index));
                      List<dynamic> entries = model.getEntriesFromDay(dateId);
                      return _buildExpansionTile(dateId, entries, model, index);
                    }),
              ),
              /*    LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  return InkWell(
                    onTap: () {
                      _controller.jumpTo(2000);
                    },
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20.0),
                        bottomRight: Radius.circular(20.0)),
                    child: Container(
                      width: constraints.maxWidth,
                      padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(20.0),
                              bottomRight: Radius.circular(20.0)),
                          color: Theme.of(context).accentColor),
                      child: Text(
                        "JUMP TO",
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  );
                },
              ) */
            ],
          ),
        );
      },
    );
  }

//Zeigt Reisen für den jeweiligen Tag an, und erlaubt es, neue Reisen hinzuzufügen
  ExpansionTile _buildExpansionTile(
      DateTime dateId, List entries, DataModel model, int index) {
    return ExpansionTile(
      title: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              DateTimeUtils.toLongDateString(dateId) + ": ",
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
          ),
          Text(
            (entries.length == 1 ? "1 Eintrag" : "${entries.length} Einträge") +
                "\n${UnitUtils.printEmissions(model.getTotalDailyEmissions(dateId))}" +
                "\n${UnitUtils.printEmissions(model.getDailyEntryEmissions(dateId))}",
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
      initiallyExpanded: index == 0 ? true : false,
      children: [
        ///Zeige Einträge für diesen Tag
        for (var entry in entries) _buildEntries(entry),
        Text("Reise für diesen Tag hinzufügen:"),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _buildVehicleButton(
                Icons.airplanemode_active, dateId, VehicleType.plane, "Flug"),
            _buildVehicleButton(Icons.directions_car, dateId,
                VehicleType.motorizedVehicle, "privat"),
            _buildVehicleButton(
                Icons.directions_transit, dateId, VehicleType.public, "Öffis"),
            IconButton(
              icon: Icon(Icons.info),
              onPressed: () => showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                        title: Text('Was heißt "Öffis" und "privat"?'),
                        content: SingleChildScrollView(
                          child: Text(
                              'Öffis sind noch recht simpel: Alles, wofür du ein Ticket brauchst, außer Flugzeuge. \n\n Privat ist der ganze Rest, also Autos, Taxis, Motorräder - aber auch (Elektro-)Fahrräder, und auch Strecken zu Fuß kannst du hier eintragen. Fernbusse können hier übrigens auch verwendet werden falls die Strecke bei den Öffis nicht gefunden werden konnte, sie sind ja praktisch nur große Autos mit vielen Fahrgästen. (BTW, Ist dir schon aufgefallen, wie umweltfreundlich Fernbusse trotz Diesel sind?)'),
                        ),
                        actions: <Widget>[
                          FlatButton(
                            onPressed: () => Navigator.pop(context),
                            child: Text('Zurück'),
                          )
                        ],
                      )),
            )
          ],
        )
      ],
    );
  }

  Widget _buildVehicleButton(
      IconData icon, DateTime date, VehicleType vt, String desc) {
    return FlatButton.icon(
      label: MediaQuery.of(context).size.width < 350 ? SizedBox() : Text(desc),
      color: Colors.lightGreen.withAlpha(120),
      onPressed: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return AddEntryPage.selectVehicle(false, date, vt);
        }));
      },
      icon: Icon(icon),
    );
  }
}

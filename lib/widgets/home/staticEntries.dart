import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:eMission/models/staticEntry.dart';
import 'package:eMission/utils/dateTime.dart';
import 'package:eMission/utils/ui.dart';
import 'package:eMission/utils/units.dart';
import 'package:eMission/widgets/defaultUiElements.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import "package:eMission/pages/staticEntry/consumption.dart";
import "package:eMission/pages/staticEntry/food.dart";
import "package:eMission/pages/staticEntry/power.dart";
import "package:eMission/pages/staticEntry/heating.dart";

class StaticEntriesPage extends StatefulWidget {
  @override
  StaticEntriesPageState createState() {
    return new StaticEntriesPageState();
  }
}

class StaticEntriesPageState extends State<StaticEntriesPage> {
  DateTime startDate;
  double emissionsPerDay;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ScopedModelDescendant<DataModel>(
        builder: (BuildContext context, Widget child, DataModel model) {
          return Column(children: [
            for (StaticEntry se
                in model.getStaticEntriesForDay(DateTime.now()).values)
              _buildCardForStaticEntryTyype(se, context),

            ///Zeigt Infos zum Einfluss der Atmung an
            DefaultCard(
              child: ListTile(
                leading: Text(UnitUtils.printEmissions(0) + "\n pro Tag"),
                title: Text(
                  "Atmung",
                  style: TextStyle(fontSize: 25),
                ),
                trailing: IconButton(
                  icon: Icon(Icons.info),
                  onPressed: () {
                    showDialog(
                        builder: (context) {
                          return AlertDialog(
                            title: const Text(
                                "null Emissionen - wie kann das sein?"),
                            content: SingleChildScrollView(
                              child: const Text(
                                  "Mit jedem Atemzug setzen wir CO\u{2082} frei. Und zwar eine ganze Menge: Beim Sport atmest du bis zu 50 Liter Luft pro Minute! Umgerechnet heißt das dann: eine Stunde Sport erzeugt etwa 5kg CO\u{2082}." +
                                      "\n\nAlso beweg dich so wenig wie möglich, rede mit niemanden, und halte so oft es geht die Luft an!" +
                                      "\n\n\nDu glaubt uns nicht? Sehr gut! Ist nämlich quatsch. Denn den Kohlenstoff, den du freisetzt, hast du über deine Nahrung aufgenommen, und dieser ist in Pflanzen durch Photosynthese aus Luft gefiltert worden. Photosynthese und Atmung gleichen sich perfekt aus - CO\u{2082} kann ja auch nicht einfach aus dem Nichts enstehen." +
                                      "\n\nIm Gegensatz dazu wird beim Fördern von fossilen Brennstoffen neuer Kohlenstoff an die Erdoberfläche geschafft. Deswegen erhöht sich beim Verbrennen von Benzin die Menge an CO\u{2082} in der Atmosphäre dauerhaft, aber nicht durch deine nächste Gym-Session."),
                            ),
                            actions: <Widget>[
                              FlatButton(
                                child: RichText(
                                  text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                        text: "langweilig",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            decoration:
                                                TextDecoration.lineThrough)),
                                    TextSpan(
                                        text: " Ach, interessant!",
                                        style: TextStyle(color: Colors.blue))
                                  ]),
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              )
                            ],
                          );
                        },
                        context: context);
                  },
                ),
              ),
            )
          ]);
        },
      ),
    );
  }

  DefaultCard _buildCardForStaticEntryTyype(
      StaticEntry se, BuildContext context) {
    return DefaultCard(
      child: ListTile(
        leading: Text(
          UnitUtils.printEmissions(se.emissionsPerDay) + "\n pro Tag",
          style: TextStyle(
              //Farbe abhängig vom Verbrauch verglichen mit dem Durchschnitt
              color: UiUtils.generateColorFromRatio(se.emissionsPerDay /
                  StaticEntry.average(se.runtimeType.toString())
                      .emissionsPerDay)),
        ),
        title: Text(
          staticEntryString[se.runtimeType],
          style: TextStyle(fontSize: 25),
        ),
        trailing: se.runtimeType == PublicStaticEntry
            //TODO: auch hierfür eigene seite, vielleicht für länderauswahl (deutschsprachig)?
            //Info anzeigen, dass die öffentlichen nicht bearbeitet werden können
            ? IconButton(
                icon: Icon(Icons.info),
                onPressed: () {
                  showDialog(
                      builder: (context) {
                        return AlertDialog(
                          title: const Text("Öffentliche Emissionen"),
                          content: SingleChildScrollView(
                            child: const Text(
                                "Öffentliche Emissionen sind die Emissionen, die in staatlichen Organen entstehen (Verwaltung, Müllabfuhr, und so weiter). Daher kannst du sie nicht bearbeiten, sie sind eine feste Größe. " +
                                    "\n\nWir sind uns noch unsicher, wie wir mit öffentlichen Emissionen umgehen sollen und freuen uns über Feedback dazu. Entfernen wollen wir sie aber nicht, immerhin tragen sie auch zu deinen Emissionen bei"),
                          ),
                          actions: <Widget>[
                            FlatButton(
                              child: const Text("Okay"),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            )
                          ],
                        );
                      },
                      context: context);
                },
              )
            :
            //Button zum Bearbeiten der Einträge
            IconButton(
                icon: Icon(Icons.edit),
                onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          switch (se.runtimeType) {
                            case PowerStaticEntry:
                              return PowerStaticEntryPage();
                            case HeatingStaticEntry:
                              return HeatingStaticEntryPage();
                            case ConsumptionStaticEntry:
                              return ConsumptionStaticEntryPage();
                            case FoodStaticEntry:
                              return FoodStaticEntryPage();
                            default:
                              print("Es wurde versucht einen StaticEntry vom Typ" +
                                  se.runtimeType.toString() +
                                  "zu bearbeiten, dieser ist jedoch unbekannt");
                          }
                        },
                      ),
                    ),
              ),
        subtitle: se.runtimeType == PublicStaticEntry
            ? null
            : Text(se.isDefault
                ? "Durchschnittwert - bitte bearbeiten"
                : "Aktiv seit: " + DateTimeUtils.toDateString(se.start)),
      ),
    );
  }
}

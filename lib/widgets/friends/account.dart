import 'dart:convert';

import 'package:eMission/models/friendData.dart';
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:flutter/material.dart';

class AccountsWidget extends StatefulWidget {
  final DataModel _model;

  const AccountsWidget(this._model, {Key key}) : super(key: key);
  @override
  _AccountsWidgetState createState() => _AccountsWidgetState();
}

class _AccountsWidgetState extends State<AccountsWidget> {
  bool saveLogin = true;
  bool obscureText = true;
  bool isLoggingIn = false;
  TextEditingController _usernameTextEditingController,
      _passwordTextEditingController;

  @override
  void initState() {
    super.initState();
    _passwordTextEditingController = TextEditingController();
    _usernameTextEditingController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    //Finde heraus, ob der Nutzer angemeldet ist
    return Column(
        children: ListTile.divideTiles(context: context, tiles: [
      //Zeige das entsprechende Widget
      widget._model.loginData != null
          ? _buildLoggedInWidget()
          : _buildLoginWidget(context)
    ]).toList());
  }

  ///Widget zum Anmelden eines Benutzers. Enthält Textfelder für Username und Passwort, sowie Login- und Registrieren-Buttons
  Column _buildLoginWidget(BuildContext context) {
    return Column(
      children: isLoggingIn
          ? <Widget>[Text("Anmelden"), LinearProgressIndicator()]
          : <Widget>[
              Text("Dein Account"),
              TextField(
                decoration: InputDecoration(labelText: "Benutzername"),
                controller: _usernameTextEditingController,
              ),
              TextField(
                obscureText: obscureText,
                controller: _passwordTextEditingController,
                decoration: InputDecoration(labelText: "Passwort"),
              ),
              SwitchListTile(
                value: !obscureText,
                title: Text("Passwort anzeigen"),
                onChanged: (bool value) {
                  setState(() {
                    obscureText = !value;
                  });
                },
              ),
              SwitchListTile(
                value: saveLogin,
                title: Text("Anmeldedaten speichern."),
                onChanged: (bool value) {
                  setState(() {
                    saveLogin = value;
                  });
                },
              ),
              _buildPrivacyInfoButton(),
              ButtonBar(alignment: MainAxisAlignment.center,
                children: <Widget>[
                  _buildLoginButton(context, false),
                  _buildLoginButton(context, true)
                ],
              )
            ],
    );
  }

  RaisedButton _buildPrivacyInfoButton() {
    return RaisedButton(
      child: Text('Datenschutzinfo'),
      onPressed: () {
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Text('Infos zum Freunde-System'),
                content: SingleChildScrollView(
                  child: Text(
                      'Um deine Emissionen an deine Freunde weiterzuleiten, müssen wir sie auf unserem Server verarbeiten und speichern. Dazu ein paar Infos: \n - Solange du nicht auf Anmelden oder Registrieren drückst, wird gar nichts gesendet. \n - Wir speichern folgende Daten: deinen Benutzernamen, dein Passwort (gehasht und gesaltet, das heißt wir kennen dein Passwort nicht, und auch Angreifer können es herausfinden), sowie wieviel Emissionen du an welchem Tag erzeugt hast. \n\n Wir sammeln absichtlich keine persönlichen Daten, denn wir sind Hobbyprogrammierer und müssen damit rechnen, dass Daten von uns geklaut werden können (was auch gro0en Firmen erschreckend oft passiert). Daher gibt es aktuell aber leider keine Möglichkeit, ein vergessenes Passwort wiederherzustellen.'),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Zurück"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              );
            });
      },
    );
  }

  ///Registriert einen neuen Benutzer für die eingegebenen Daten und meldet diesen an
  RaisedButton _buildLoginButton(BuildContext context, bool registerMode) {
    Function loginFunction =
        registerMode ? widget._model.createUser : widget._model.login;
    return RaisedButton(
      child: Text(registerMode ? "Registrieren" : "Anmelden"),
      onPressed: () {
        setState(() {
          isLoggingIn = true;
        });
        LoginData loginData = LoginData.hashPassword(
            _usernameTextEditingController.text,
            _passwordTextEditingController.text,
            saveLogin);
        loginFunction(loginData).then((value) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(registerMode
                ? "Erfolgreich registriert und angemeldet"
                : "Erfolgreich angemeldet"),
          ));
          setState(() {
            isLoggingIn = false;
          });
        }).catchError((error) {
          setState(() {
            isLoggingIn = false;
          });
          if (error.runtimeType.toString() == "Response")
            error = json.decode(error.body)['message'];
          showDialog(
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text(registerMode
                      ? "Fehler beim Registrieren!"
                      : "Fehler beim Anmelden!"),
                  content: Text(error.toString()),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("Okay, dann halt nochmal"),
                      onPressed: () => Navigator.pop(context),
                    )
                  ],
                );
              },
              context: context);
        });
      },
    );
  }

  Widget _buildLoggedInWidget() {
    return ListTile(
      title: Text("Angemeldet als " + widget._model.loginData.username),
      trailing: RaisedButton(
        child: Text("Abmelden"),
        onPressed: () {
          setState(() {
            widget._model.loginData = null;
          });
        },
      ),
    );
  }
}

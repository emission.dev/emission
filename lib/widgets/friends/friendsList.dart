import 'dart:convert';

import 'package:eMission/models/friendData.dart';
import 'package:eMission/models/scoped-models/dataModel.dart';
import 'package:share/share.dart';
import 'package:flutter/material.dart';

class FriendsListWidget extends StatefulWidget {
  final DataModel _model;

  const FriendsListWidget(this._model, {Key key}) : super(key: key);
  @override
  _FriendsListWidgetState createState() => _FriendsListWidgetState();
}

class _FriendsListWidgetState extends State<FriendsListWidget> {
  bool isSearching = false;
  String username, errorText;
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        for (FriendData friendData in widget._model.friendData)
          ListTile(
            title: Text(friendData.username),
            trailing: IconButton(
              icon: Icon(Icons.remove_circle),
              onPressed: () {
                setState(() {
                  widget._model.friendData.remove(username);
                });
              },
            ),
          ),
        _buildNewFriendTile(),
        RaisedButton(
          child: Text('Lade Freunde ein'),
          onPressed: () => Share.share(
                'https://play.google.com/store/apps/details?id=de.emission',
              ),
        )
      ],
    );
  }

  Widget _buildNewFriendTile() {
    return isSearching
        ? ListTile(
            subtitle: LinearProgressIndicator(),
            title: Text("Suche nach $username..."),
          )
        : ListTile(
            title: TextField(
              onChanged: (String value) {
                setState(() {
                  username = value;
                });
              },
              decoration: InputDecoration(
                  labelText: "Benutzername", errorText: errorText),
            ),
            trailing: IconButton(
              icon: Icon(Icons.person_add),
              onPressed: () {
                setState(() {
                  isSearching = true;
                });
                widget._model.updateUserData(username).then((value) {
                  setState(() {
                    isSearching = false;
                    errorText = null;
                  });
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("$username hinzugefügt"),
                  ));
                }).catchError((error) {
                  if (error.runtimeType.toString() == "Response")
                    error = json.decode(error.body)['message'];
                  setState(() {
                    isSearching = false;
                    errorText = error.toString();
                  });
                });
              },
            ),
          );
  }
}
